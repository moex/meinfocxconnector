//
//  GIStompFrame.h
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIStompBody.h"
#import "GIError.h"
#import "GIQueue.h"

#define GI_FRAME_DESTINATION_STRING @"destination"
#define GI_FRAME_SELECTOR_STRING @"selector"


/**
 Declaration protocol should be used by concrete STOMP class declaration such as connect frame, disconnect, messages and etc...
*/
@protocol GIStompDeclarationProtocol <NSObject>
@optional
/**
 *  Destination name dfines in inherient classes.
 *  
 *  @return string name.
 */
+ (NSString*) destination;
/**
 *  Market place name.
 *  
 *  @return name.
 */
- (NSString*) marketPlace;
@end

/**
 When we receive Frame dispatcher can pass frame to model instance whih can be mamanged in View.
*/
@protocol GIStompDispatcherProtocol <NSObject>

/**
 *  The MAIN STOMP protocol handler which you will use in your app. Handle when server replies data which server componetnets publish server to bus.
 *  Of cause if client has permition to receive these data.
 *  @param frame frame.
 */
- (void) didFrameReceive:(id)frame;

@optional
/**
 * Restored deffered subscription
 */
- (void) didSubscriptionRestore:(id)frame;

/**
 * Frame will receive.
 */
- (void) willFrameSend:(id)frame;

/**
 *  Handle when STOMP frame receiveing interrupted.
 *  
 *  @param frame frame object.
 *  @param error error description.
 */
- (void) didFrameInterrupt:(id)frame withError:(GIError*)error;

/**
 *  Handle when STOMP frame does not have access to receive.
 *  
 *  @param frame frame object.
 *  @param error error description.
 */
- (void) didAccessDeny:(id)frame withError:(GIError*)error;

/**
 *  Handle when STOMP frame error recieved. 
 *  
 *  @param frame frame object.
 *  @param error error description.
 */
- (void) didFrameError:(id)frame withError:(GIError*)error;
@end


/**
 *  The root STOMP frame object. Define the common STOMP attributes like the NSObject class defines root of Cocoa hierarchy.
 */
@interface GICommonStompFrame : NSObject <NSCopying>
/**
 *  STOMP frame name.
 */
@property(readonly) NSString     *name;

/**
 *  STOMP frame headers.
 */
@property(readonly) NSDictionary *headers;

/**
 *  STOMP frame body.
 */
@property GIStompBody  *body;

/**
 *  Create STOMP frame from raw data.
 *  
 *  @param aData raw data.
 *  
 *  @return STOMP frame.
 */
- (id) initWithData: (NSData*)aData;

/**
 *  Recreate STOMP frame from raw data.
 *  
 *  @param aData raw data.
 */
- (void) updateFromData: (NSData*)aData;

/**
 *  Get STOMP Frame header, it usually uses when we receive new message.
 *  
 *  @param key header name.
 *  
 *  @return value.
 */
- (id)   getHeader:(NSString*)key;

/**
 *  Set STOMP Frame header, it usually uses to send Frame.
 *  
 *  @param key   header name.
 *  @param value header value.
 */
- (void) setHeader:(NSString*)key withValue:(id)value;

/**
 *  Create instance with name of stomp frame (CONNECT, MESSAGE, REPLAY, etc).
 *  
 *  @param aName name.
 *  
 *  @return STOMP frame.
 */
- (id) initWithName:(NSString *)aName;

/**
 *  Get STOMP frame raw data.
 *  
 *  @return raw data.
 */
- (NSData*) stompFrame;

/**
 *  Compare headers.
 *  
 *  @param object STOMP frame object.
 *  
 *  @return YES if it equals.
 */
- (BOOL) isEqualWithHeaders:(id)object;
@end


/**
 *  STOMP receiver protocol. Receiver delegate performes message delegate in conrete object process level.
 */
@protocol GIStompReceiverProtocol <NSObject>

/**
 *  Handle when message received.
 *  
 *  @param frame STOMP feame.
 */
- (void) messageReceived: (GICommonStompFrame*)frame;

@end

/**
 * Responses Queue. Server can respond a lot of messages at the moment. Client should accumulate all of these.
 */
@interface GIStompFrameQueue : GIQueue
@end


/**
 *  User level ROOT STOMP object. Every STOMP of the class can be regestered in central STOMP dispatcher and handle on concrete app level.
 */
@interface GIStomp : NSObject <GIStompDeclarationProtocol, GIStompReceiverProtocol>

/**
 *  Some user defined data. It can used to echange references to any object between local logical part of application which involved in differet stage of STOMP processing.
 */
@property(nonatomic) id userData;

/**
 *  Reference to connection structure.
 */
@property(readonly) GIStompStructure *structure;

/**
 *  Request definition.
 */
@property GICommonStompFrame    *request;

/**
 *  Last server response.
 */
@property(readonly) GICommonStompFrame    *lastResponse;

/**
 *  We can have many responses for one request. Iterate if it needs.
 */
@property(readonly) GIStompFrameQueue     *responseQueue;

/**
 *  STOMP frame is already dispatched.
 */
@property(readonly) BOOL isDispatched;


/**
 *  Set max queue depth if we want to keep last size responses only.
 *  
 *  @param size queue size.
 */
- (void) setResponseQueueMaxSize: (NSUInteger)size;

/**
 *  Create stomp request and response containers.
 *  
 *  @param rqname request name.
 *  @param rpname response name.
 *  
 *  @return STOMP frame instance.
 */
- (id) initWithRequestName: (NSString*)rqname andResponseName:(NSString*)rpname;


@end
