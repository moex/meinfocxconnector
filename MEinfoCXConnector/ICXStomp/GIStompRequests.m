//
//  GIStompRequests.m
//  GIConnector
//
//  Created by denn on 8/7/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompRequests.h"
#import "GIConnectionStructure.h"


NSNumber *getGIStompNextUniqID(id object){
    static NSInteger _gistomp_request_id = 0;
    @synchronized(object){
        return [NSNumber numberWithInteger:_gistomp_request_id++];
    }
}

@class GIStompMessage;
//
// CONNECT
//
@implementation GIStompConnect
//
//
// in new version
+ (id) withLogin:(NSString *)login andPasscode:(NSString *)passcode{
    return [self withLogin:login withPasscode:passcode andDomain:@"passport"];
}

+ (id) withLogin:(NSString *)login withPasscode:(NSString *)passcode andDomain:(NSString *)domain{
    return [self withLogin:login withPasscode:passcode andDomain:domain withLang:GIC_current_lang()];
};

+ (id) withLogin:(NSString *)login withPasscode:(NSString *)passcode andDomain:(NSString *)domain withLang:(NSString*)language{
    GIStompConnect *connect = [[GIStompConnect alloc] initWithRequestName:@"CONNECT" andResponseName:@"CONNECTED" ];
    
    [connect.request setHeader:@"login" withValue: login];
    [connect.request setHeader:@"passcode" withValue:passcode];
    [connect.request setHeader:@"domain" withValue:domain];
    [connect.request setHeader:@"language" withValue:language];
    
    return connect;
};

@end

//
// DISCONNECT
//
@implementation GIStompDisconnect
+ (id) make{
    return [[GIStompDisconnect alloc] initWithRequestName:@"DISCONNECT" andResponseName:@""];
}
@end

//
// STOMP REQUEST
//
@interface GIRequestStompFrame ()
@property NSNumber *requestID;
@end

@implementation GIRequestStompFrame
@synthesize requestID=_requestID;
@synthesize receipt=_receipt;

- (id) init{
    return  [ self initWithRequestName:@"REQUEST" andResponseName:@"REPLY"];
}

- (void) updateRequestID{
    _receipt = _requestID = getGIStompNextUniqID(self);
}

- (void) selectFor:(NSString *)selector{
    _receipt = _requestID = getGIStompNextUniqID(self);
    
    [self.request setHeader:GI_FRAME_DESTINATION_STRING withValue:[[self class] destination]];
    
    if (selector)
        [self.request setHeader:GI_FRAME_SELECTOR_STRING withValue:selector];
    
    [self.request setHeader:@"id" withValue:[NSString stringWithFormat:@"%@", self.requestID]];
    [self.request setHeader:@"receipt" withValue:[NSString stringWithFormat:@"%@", self.receipt]];    
}
@end


//
// TICKER LIST
//

@interface GIStompTicker()
@property NSString *ticker, *title;
@end

@implementation GIStompTicker
@synthesize ticker,title;
@end

@implementation GIStompTickers
{
    NSString *marketPlace_;
}

+ (NSString*) destination{
    return @"list";
}

- (NSString*) marketPlace{
    return marketPlace_;
}

+ (id) select:(NSString *)tickerMask onMarketPlace:(NSString *)market{
    
    NSString  *pattern;
    
    if(tickerMask!=nil)
        pattern = [NSString stringWithFormat:@" and pattern=\"%@\"", tickerMask];
    
    NSString  *selector;
    
    if (market || pattern) {
        selector= [NSString stringWithFormat:@"marketplace=\"%@\"%@", market, pattern?pattern:@""];
    }
    
    GIStompTickers *tickers = [[GIStompTickers alloc] init];
    
    tickers->marketPlace_ = market;
    
    [tickers selectFor:selector];
    
    return tickers;
}


- (GIStompTicker*) tickerByRow: (NSUInteger) row{
    NSDictionary *_d =[self.lastResponse.body rowByIndex:row];
    GIStompTicker *_t = [[GIStompTicker alloc] init];
    _t.title = _d[@"CAPTION"];
    _t.ticker = _d[@"TICKER"];
    return _t;
}

- (NSUInteger) count{
    return self.lastResponse.body.countOfRows;
}
@end

//
// Filtered ticker list
//
@implementation GIStompSearchTickers

+ (NSString*) destination{
    return @"SEARCH.ticker";
}

+ (id) select:(NSString*)ticker{
    
    NSString  *pattern;
    
    if(ticker!=nil)
        pattern = [NSString stringWithFormat:@"ticker=\"%@\"", ticker];
    
    GIStompSearchTickers *tickers = [[GIStompSearchTickers alloc] init];
    
    [tickers selectFor:pattern];
    
    return tickers;
}

+ (id) selectByMask:(NSString*)tickerMask{
    
    NSString  *pattern;
    
    if(tickerMask!=nil)
        pattern = [NSString stringWithFormat:@"pattern=\"%@\"", tickerMask];
    
    GIStompSearchTickers *tickers = [[GIStompSearchTickers alloc] init];
    
    [tickers selectFor:pattern];
    
    return tickers;
}


+ (id) selectPreset:(NSString*)presetName{

    GIStompSearchTickers *tickers = [[GIStompSearchTickers alloc] init];
    
    [tickers selectFor:[NSString stringWithFormat:@"preset=\"%@\"", presetName]];
    
    return tickers;
}

@end
