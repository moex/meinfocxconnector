//
//  GIStompSubscriptions.h
//  GIConnector
//
//  Created by denn on 8/7/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompFrame.h"


@class GIStompMessage;

/**
 *  Basic ICX protocol STOMP support. This is the UNSUBSCRIPTION STOMP frame. We must use it at the end of subscription proccess.
 */
@interface GIStompUnsubscriber : GIStomp <GIStompDeclarationProtocol>

/**
 *  Make subscription message.
 *  
 *  @param message base STOMP frame.
 *  
 *  @return STOMP frame.
 */
+ (id) makeForMessage:(GIStompMessage*)message;
+ (id) makeForStomp:(GICommonStompFrame *)frame;
@end


/**
 *  Basic ICX protocol STOMP support. This is the SUBSCRIPTION STOMP frame. We must use to create STOMP message which request subscription for some data.
 */
@interface GIStompMessage : GIStomp <GIStompDeclarationProtocol>

/**
 *  SUBSCRIPTION destination.
 */
@property(readonly) NSString *destination;

/**
 *  SUBSCRIPTION unique ID.
 */
@property(readonly) NSNumber *subscriptionID;

/**
 *  SUBSCRIPTION name space (market place).
 */
@property(readonly) NSString *marketPlace;

/**
 *  Accepted unique id.
 */
@property(readonly) NSNumber *receipt;

/**
 *  Who will handle responses.
 */
@property id<GIStompDispatcherProtocol> delegate;

// subscribe on messages

/**
 *  Select by standart cretaeria.
 *  
 *  @param selector selector
 *  @param ticker   ticker name.
 */
- (void) selectFor:(NSString *)selector andTicker: (NSString*)ticker;

/**
 *  Select for market place by standart criteria.
 *  
 *  @param marketplace market place.
 */
- (void) selectForMarketPlace:(NSString *)marketplace;

/**
 *  Create instance.
 *  
 *  @return STOMP SUBSCRIPTION frame.
 */
- (id) init;

@property(readonly) dispatch_queue_t stomponReadQueue;

/**
 *  Ticker name.
 */
@property(readonly, nonatomic) NSString *ticker;

/**
 * Create new subscription ID;
 */
- (void) updateSubscriptionID;
@end


//
// SELECTOR
//

/**
 *  Basic ICX protocol STOMP support. Base selector.
 */
@interface GIStompSelector: GIStompMessage

/**
 *  Select by ticker name.
 *  
 *  @param ticker ticker name.
 *  
 *  @return STOMP frame selector.
 */
+ (id) select:(NSString *)ticker;
@end

//
// SYSLOG
//
/**
 *  Basic ICX protocol STOMP support. System log queue.
 */
@interface GIStompSyslog : GIStompMessage

/**
 *  Initiate system log subscription frame.
 *  
 *  @return STOMP frame message.
 */
+ (id) select;
@end


/**
 *  Basic ICX protocol STOMP support. Orderbook SUBSCRIPTION.
 */
@interface GIStompOrderbook : GIStompSelector

/**
 *  Get BUY/SELL position in orderbook.
 */
@property NSInteger splitPosition;

/**
 *  Set maximum depth/
 *  
 *  @param depth depth.
 */
+ (void) setDepth:(NSInteger)depth;

/**
 *  Get maximum depth.
 *  
 *  @return number.
 */
- (NSInteger) depth;
@end


/**
 *  Basic ICX protocol STOMP support. Lasttrade SUBSCRIPTION.
 */
@interface GIStompLastTrades : GIStompSelector
@end

/**
 *  Basic ICX protocol STOMP support. Securities SUBSCRIPTION.
 */
@interface GIStompSecurity : GIStompSelector

/**
 *  Get security by ticker.
 *  
 *  @param ticker ticker name.
 *  
 *  @return rows by tickers.
 */
- (NSDictionary*) getRowByTicker:(NSString*)ticker;
@end


//
// CANDLES
//
/**
 *  Basic ICX protocol STOMP support. Candles SUBSCRIPTION.
 */
@interface GIStompCandles: GIStompSelector

/**
 *  Select ticker candles by ticker name and ticker interval.
 *  
 *  @param ticker   ticker name.
 *  @param interval candle interval.
 *  
 *  @return STOMP frame message.
 */
+(id) select:(NSString *)ticker andInterval:(NSString*)interval;
@end

