//
//  GIStompSubscriptions.m
//  GIConnector
//
//  Created by denn on 8/7/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompSubscriptions.h"

extern NSNumber *getGIStompNextUniqID(id object);

//
// UNSUBSCRIBE
//
@implementation GIStompUnsubscriber

+ (id) makeForMessage:(GIStompMessage *)message{
    GIStompUnsubscriber *req = [[GIStompUnsubscriber alloc] initWithRequestName:@"UNSUBSCRIBE" andResponseName:@""] ;
    
    if ([message destination])
        [req.request setHeader:GI_FRAME_DESTINATION_STRING withValue: [message destination] ];
    
    if (message.subscriptionID)
        [req.request setHeader:@"id" withValue: message.subscriptionID];
    
    return req;
}

+ (id) makeForStomp:(GICommonStompFrame *)frame{
    GIStompUnsubscriber *req = [[GIStompUnsubscriber alloc] initWithRequestName:@"UNSUBSCRIBE" andResponseName:@""] ;
    
    [req.request setHeader:GI_FRAME_DESTINATION_STRING withValue: frame.headers[GI_FRAME_DESTINATION_STRING] ];
    [req.request setHeader:@"id" withValue: frame.headers[@"subscription"]];
    
    return req;
}
@end

//
// SUBSCRIBE
//
@interface GIStompMessage()
@property NSNumber *subscriptionID;
@property NSString *marketPlace;
@property dispatch_queue_t stomponReadQueue;
@property (nonatomic,strong) NSTimer *deferredTimer;
@property (atomic,assign) NSInteger references;
@end

@implementation GIStompMessage

@synthesize destination=_destination, receipt=_receipt, marketPlace=_marketPlace, deferredTimer=deferredTimer, references=_references;

- (void) setSession:(NSString *)sessionID{
    if (sessionID) {
        [self.request setHeader:@"session" withValue:sessionID];
    }
}

- (id) init{
    self = [super initWithRequestName:@"SUBSCRIBE" andResponseName:@"MESSAGE"];
    if (self) {
        _references = 0;
    }
    return self;
}

- (NSString*) destination{
    return self.request.headers[GI_FRAME_DESTINATION_STRING];
}

- (void) setMarketPlace:(NSString *)marketPlace{
    _marketPlace = marketPlace;
}

- (NSString*) marketPlace{
    return _marketPlace;
}

- (void) updateSubscriptionID{
    _subscriptionID = _receipt = getGIStompNextUniqID(self);
}

- (void) selectForMarketPlace:(NSString *)marketplace{
    _receipt = _subscriptionID = getGIStompNextUniqID(self);
    
    [self.request setHeader:GI_FRAME_DESTINATION_STRING withValue: [NSString stringWithFormat:@"%@.%@", marketplace,[[self class] destination]]];
    
    @try {
        self.marketPlace = marketplace;
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
        self.marketPlace = @"any";
    }
    
    [self.request setHeader:@"id" withValue:self.subscriptionID];
    [self.request setHeader:@"receipt" withValue:[NSString stringWithFormat:@"%@", self.receipt]];
    
    self.stomponReadQueue = dispatch_queue_create("com.infocx.message.queue", DISPATCH_QUEUE_SERIAL);
}

- (void) selectFor:(NSString *)selector andTicker:(NSString *)ticker{
    
    _ticker = ticker;
    
    NSArray *comp = [ticker componentsSeparatedByString:@"."];
    
    _receipt = _subscriptionID = getGIStompNextUniqID(self);
    
    [self.request setHeader:GI_FRAME_DESTINATION_STRING withValue: [NSString stringWithFormat:@"%@.%@", comp[0],[[self class] destination]]];
    
    if(selector){
        [self.request setHeader:GI_FRAME_SELECTOR_STRING withValue:selector];
    }
    
    @try {
        self.marketPlace = comp[0];
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
        self.marketPlace = @"any";
    }
    
    [self.request setHeader:@"id" withValue:self.subscriptionID];
    [self.request setHeader:@"receipt" withValue:[NSString stringWithFormat:@"%@", self.receipt]];
    
    self.stomponReadQueue = dispatch_queue_create("com.infocx.message.queue", DISPATCH_QUEUE_SERIAL);
}


@end

//
// SYSLOG
//
@implementation GIStompSyslog
+ (NSString*) destination{
    return @"syslog";
}
+ (id)select{
    GIStompSyslog *s=[[GIStompSyslog alloc] init];
    [s selectFor:nil andTicker:nil];
    return s;
}
- (void) setSession:(NSString *)sessionID{
    // nothing to do
}
@end

//
// Selectors
//
@implementation GIStompSelector
+ (id) select:(NSString *)ticker{
    id m=[[[self class] alloc] init];
    [m selectFor:[NSString stringWithFormat:@"TICKER=\"%@\"", ticker] andTicker:ticker];
    return m;
}
@end

//
// ORDERBOOK
//
static NSInteger __gi_stomp_orderbook_depth = 0;
@implementation GIStompOrderbook

- (id) init{
    self = [super init];
    if (self) {
        [self setResponseQueueMaxSize:1];
    }
    return self;
}

+ (NSString*)destination{    return @"orderbooks"; }

+ (void) setDepth:(NSInteger)depth{
    __gi_stomp_orderbook_depth = depth;
}

- (NSInteger) depth{ return __gi_stomp_orderbook_depth;}

- (void) messageReceived:(GICommonStompFrame *)iresponse{
    
    if (!iresponse)
        return;
    
    self.splitPosition = 0;
    
    if (__gi_stomp_orderbook_depth==0 || [iresponse.body countOfRows]<=__gi_stomp_orderbook_depth*2) {
        [super messageReceived: iresponse];
        self.splitPosition = [iresponse.body countOfRows]/2;
        return;
    }
    
    NSUInteger bids=0, asks=0, bids_ask_index=0;
    for (NSInteger i=0; i<[iresponse.body countOfRows]; i++) {
        BOOL isBid = [[iresponse.body objectForIndex:i andKey:@"BUYSELL"] isEqualToString:@"B"];
        if (isBid) {
            bids++;
            bids_ask_index++;
        }
        else
            asks++;
    }
    
    if (bids>__gi_stomp_orderbook_depth) {
        if (asks<__gi_stomp_orderbook_depth ) {
            bids = 2*__gi_stomp_orderbook_depth-asks;
        }else
            bids=__gi_stomp_orderbook_depth;
    }
    if (asks>__gi_stomp_orderbook_depth) {
        if (bids<__gi_stomp_orderbook_depth) {
            asks = 2*__gi_stomp_orderbook_depth-bids;
        }
        else
            asks=__gi_stomp_orderbook_depth;
    }
    
    self.splitPosition = bids_ask_index;
    NSArray *new_bid_asks = [iresponse.body.data subarrayWithRange:NSMakeRange(bids_ask_index-bids,bids+asks)];
    
    [iresponse.body setData: new_bid_asks];
    [super messageReceived:iresponse];
}

@end

//
// LAST TRADES
//
@implementation GIStompLastTrades
+ (NSString*) destination{ return @"lasttrades"; }
@end


//
// SECURITY
//
@implementation GIStompSecurity
+ (NSString*) destination{    return @"securities"; }

- (NSDictionary*) getRowByTicker:(NSString *)ticker{
    if (!self.lastResponse) return nil;
    return [self.lastResponse.body rowByKeyValue:ticker forColumn:@"TICKER"];
}
@end

//
// Candles
//
@implementation GIStompCandles
+ (NSString*) destination{    return @"candles"; }

+(id) select:(NSString *)ticker andInterval:(NSString*)interval{
    id m=[[[self class] alloc] init];
    [m selectFor:[NSString stringWithFormat:@"ticker=\"%@\" and interval=%@", ticker, interval] andTicker:ticker];
    return m;
}

@end


