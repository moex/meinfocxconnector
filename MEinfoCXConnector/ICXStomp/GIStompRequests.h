//
//  GIStompRequests.h
//  GIConnector
//
//  Created by denn on 8/7/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompFrame.h"

/**
 *  Basic ICX protocol STOMP support. This is the connection STOMP frame. We must use it at the start of session.
 */
@interface GIStompConnect : GIStomp <GIStompDeclarationProtocol>

/**
 *  Create LOGICAL connection with login parapeters.
 *  
 *  @param login    user id.
 *  @param passcode user password.
 *  
 *  @return reference to STOMP frame connection .
 */
+ (id) withLogin:(NSString *)login andPasscode:(NSString *)passcode;
+ (id) withLogin:(NSString *)login withPasscode:(NSString *)passcode andDomain:(NSString*)domain;
+ (id) withLogin:(NSString *)login withPasscode:(NSString *)passcode andDomain:(NSString *)domain withLang:(NSString*)language;
@end

/**
 *  Basic ICX protocol STOMP support. This is the diconnection STOMP frame. We must use it at the end of session.
 */
@interface GIStompDisconnect : GIStomp <GIStompDeclarationProtocol>
/**
 *  Just create instance.
 *  
 *  @return frame.
 */
+ (id) make;
@end

//
// STOMP REQUEST
//
/**
 *  Basic ICX protocol STOMP support. This is the request STOMP frame. We must use when request any once responsed data.
 */
@interface GIRequestStompFrame : GIStomp <GIStompDeclarationProtocol>

/**
 *  Unique request id.
 */
@property(readonly) NSNumber *requestID;

/**
 *  Accepted unique rquest id reply.
 *  
 */
@property(readonly) NSNumber *receipt;

/**
 *  Set selector for the request. Selector looks like WHERE SQL codition to set filter for the request.
 *  
 *  @param selector selector sentence.
 */
- (void) selectFor:(NSString *)selector;

/**
 *  Create object.
 *  
 *  @return instance.
 */
- (id)init;

/**
 *  Delegate dispatcher receiveing.
 */
@property id<GIStompDispatcherProtocol> delegate;

- (void) updateRequestID;
@end

//
// REQUEST TICKER LIST
//

/**
 *  ICX ticker definition from trading engine.
 */
@interface GIStompTicker : NSObject
/**
 *  Ticker name.
 */
@property(readonly) NSString *ticker;
/**
 *  Ticker title.
 */
@property(readonly) NSString *title;
@end

/**
 *  ICX ticker list request.
 */
@interface GIStompTickers : GIRequestStompFrame

/**
 *  Create selected tickers by ticker part and market name.
 *  
 *  @param tickerMask looks like "sber", etc
 *  @param market     marketplace returns in GIStream structure.
 *  
 *  @return Tickers request frame. 
 */
+ (id) select:(NSString*)tickerMask onMarketPlace:(NSString*)market;

/**
 *  Get by row.
 *  
 *  @param row row index.
 *  
 *  @return Tiker/
 */
- (GIStompTicker*) tickerByRow: (NSUInteger) row;

/**
 *  How many tickers we found.
 *  
 *  @return count.
 */
- (NSUInteger) count;
@end

/**
 *  STOMP FRAME prepared to search ticker by some creteria.
 */
@interface GIStompSearchTickers : GIRequestStompFrame
/**
 *  Select by ticker mask.
 *  
 *  @param tickerMask looks like "sber"
 *  
 *  @return STOMP Request frame.
 */
+ (id) selectByMask:(NSString*)tickerMask;
/**
 *  Select by ticker ID.
 *
 *  @param tickerMask looks like MOEX.TQBR.SBER
 *
 *  @return STOMP Request frame.
 */
+ (id) select:(NSString*)ticker;
/**
 *  Select by preset name.
 *  
 *  @param presetName preset name.
 *  
 *  @return STOMP Request frame.
 */
+ (id) selectPreset:(NSString*)presetName;
@end


