//
//  GIStompStructure.m
//  GIConnector
//
//  Created by denn on 6/13/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIConnectionStructure.h"

NSString* GIC_current_lang(){
    static NSString *_lang = nil;
    
    if (!_lang) {
        NSString *languageCode = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([languageCode hasPrefix:@"ru"])
            _lang = @"ru";
        else
            _lang = @"en";
    }

    return _lang;
}

id _get_caption(NSDictionary *dict){
    id d = dict[@"caption"][GIC_current_lang()];
    if (d) 
        return d;
    
    NSDictionary *c=dict[@"caption"];
    
    for (NSString *key in c) {
        d = c[key];
        break;
    }
    
    return d;
    
}


id _get_hint(NSDictionary *dict){
    id d = dict[@"description"];
    
    if (d[GIC_current_lang()])
        return d[GIC_current_lang()];
    
    for (NSString *key in d) {
        d = d[key];
    }
    
    if (!d) {
        return _get_caption(dict);
    }
    return d;
}

@implementation GIEnumValue
- (id) initEnumValue:(NSDictionary *)dict{
    self = [super init];
    
    if (self) {                
        _value = dict[@"id"];
        _caption = _get_caption(dict);
        _hint = _get_hint(dict);
        if (!_hint || [_hint length]==0) {
            _hint = _caption;
        }
    }
    
    return  self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@=%@/%@", _value, _caption, _hint];
}

@end

@implementation GIEnum

- (id) initEnum: (NSString*)aName withCaption:(NSString*)aCaption withHint:(NSString*)aHint withValues:(NSDictionary*)aValues{
    self = [super init];
    
    if (self) {
        _name = aName;
        _caption = aCaption;
        _hint = aHint;
        _values = [[GIOrderedDictionary alloc] initWithCapacity:0];
        for (NSDictionary *d in aValues) {
            [_values setObject:[[GIEnumValue alloc] initEnumValue:d] forKey:d[@"id"]];
        }
    }
    
    return self;
}

- (NSString *) description{
    NSMutableString *_v = [[NSMutableString alloc] init];
    for (NSString *n in _values) {
        [_v appendFormat:@"%@: %@, ",n,_values[n]];
    }
    return [NSString stringWithFormat:@"ENUM: %@[%@:%@]: {%@}", _name, [_caption description], [_hint description], [_v description]];
}

@end

@interface __GIOutputPart()
@property GIEnum *enumType;
@property NSString *enumName;
@end

@implementation __GIOutputPart
- (id) initWithJson: (NSDictionary*) json{
    self = [super init];
    
    if (self) {
        
        _name = json[@"name"];
        _caption = _get_caption(json);
        _length = (int)[json[@"length"] integerValue];
        
        NSString *stype = json[@"type"];
        
        if ([stype hasPrefix:@"int"])
            _type = GI_STOMP_FIELD_INT;
        else if ([stype hasPrefix:@"float"])
            _type = GI_STOMP_FIELD_FLOAT;
        else if ([stype hasPrefix:@"fixed"])
            _type = GI_STOMP_FIELD_FIXED;
        else if ([stype hasPrefix:@"enum"]) {
            _type = GI_STOMP_FIELD_ENUM;
            _enumName = json[@"enum"];
        }
        else if ([stype hasPrefix:@"string"])
            _type = GI_STOMP_FIELD_STRING;
        else if ([stype hasPrefix:@"time"])
            _type = GI_STOMP_FIELD_TIME;
        else if ([stype hasPrefix:@"datetime"])
            _type = GI_STOMP_FIELD_DATETIME;
        else if ([stype hasPrefix:@"date"])
            _type = GI_STOMP_FIELD_DATE;
        else if ([stype hasPrefix:@"bool"])
            _type = GI_STOMP_FIELD_BOOL;
        else{
            _type = GI_STOMP_FIELD_UNKNOWN;
            NSLog(@"GIStompStructure:GIField: unknown type: %@", stype);
        }
    }
    
    return self;
}
@end

//
// FIELD (OUTPUT)
//
@interface GIField()
@end

@implementation GIField

- (id) initWithJson: (NSDictionary*) json{
    self = [super initWithJson:json];
    
    if (self) {

        _isSystem = _isHidden = _hasPercent = _isStatic = NO;
        _trendBy = nil;
        

        _hint = _get_hint(json);
        if (json[@"isHidden"]) {
            _isHidden = [json[@"isHidden"] boolValue];
        }
        
        if (json[@"isSystem"]) {
            _isSystem = [json[@"isSystem"] boolValue];

        }

        if (json[@"hasPercent"]) {
            _hasPercent = [json[@"hasPercent"] boolValue];
            
        }

        if (json[@"isStatic"]) {
            _isStatic = [json[@"isStatic"] boolValue];
            
        }

        if (json[@"trendBy"]) {
            _trendBy = json[@"trendBy"];
            
        }
    }
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"FIELD[name=%@/caption=%@/hint=%@, length=%i, type=%i enum=%@, isSystem=%i, isHidden=%i, hasPercent=%i, isStatic=%i]", self.name, self.caption, self.hint, self.length, self.type, self.enumType, self.isSystem, self.isHidden, self.hasPercent, self.isStatic];
}

@end

@interface GIParam()
@end

@implementation GIParam

- (id) initWithJson:(NSDictionary *)json{
    self = [super initWithJson:json];
    
    if (self) {
        _isOptional = [json[@"optional"] integerValue];
    }
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"SELECTOR[name=%@/caption=%@, length=%i, type=%i, enum=%@, isOptional=%i]", self.name, self.caption, self.length, self.type, [self.enumType description], _isOptional];
}

@end

//
// STREAM
//
@implementation GIStream

- (id) initWithJson: (NSDictionary*)json withEnums: (NSDictionary *)enums andType: (GIStreamType) type{
    self = [super init];
    
    if (self) {
        
        _type = type;
        
        _name = json[@"name"];
        _caption = _get_caption(json);
        
        NSString *_access = json[@"access"];
        
        if ([_access isEqualToString:@"general"]) {
            _accessType = GI_ACCESS_GENERAL;
        }
        else if ([_access isEqualToString:@"private"]){
            _accessType = GI_ACCESS_PRIVATE;
        }
        else{
            _accessType = GI_ACCESS_UNKNOWN;
            NSLog(@"GIStompStructure:GIStream: unknown type: %@", _access);
        }
        
        _fields = [[GIOrderedDictionary alloc] initWithCapacity:0];
        
        for (NSDictionary *f in json[@"output"]) {
            GIField  *field = [[GIField alloc] initWithJson:f];
            if (field.type == GI_STOMP_FIELD_ENUM) {
                field.enumType = enums[field.enumName];
            }
            [_fields setObject: field forKey:f[@"name"]];
        }
        
        _params = [[GIOrderedDictionary alloc] initWithCapacity:0];
        for (NSDictionary *f in json[@"params"]) {
            GIParam  *param = [[GIParam alloc] initWithJson:f];
            if (param.type == GI_STOMP_FIELD_ENUM) {
                param.enumType = enums[param.enumName];
            }
            [_params setObject: param forKey:f[@"name"]];
        }
    }
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"STREAM[id=%@/caption=%@, accesstype=%i, type=%i],\n    FIELDS = %@,\n    PARAMS = %@", _name, _caption, _accessType, _type, [_fields descriptionWithLocale:[NSLocale currentLocale] indent:13], [_params descriptionWithLocale:[NSLocale currentLocale] indent:13]];
}

@end

@interface GIMarketPlaceInfo()
@property(nonatomic) NSString *name, *engine, *market;
@end

@implementation GIMarketPlaceInfo
@end

@implementation GIMarketPlace

- (id) initMarketPLace:(NSString*)mp withJson:(NSDictionary*)json{
    
    self = [super init];
    if (self) {
        
        _info = [[GIMarketPlaceInfo alloc] init];
        
        _info.name = mp;        
        NSDictionary *_comment = json[@"comment"];
        
        if (_comment) {
            _comment = _comment[@"issrelation"];
            if (_comment) {
                _info.engine = _comment[@"engine"];
                _info.market = _comment[@"market"];
            }
        }        
        
//        if (_info.engine==nil) {
//            _info.engine = @"*";
//        }
//        
//        if (_info.market==nil) {
//            _info.market = @"*";
//        }
        
        NSArray *enums = json[@"enums"];
        
        NSMutableDictionary *_enums_ = nil;
        if (enums) {
            _enums_ = [NSMutableDictionary dictionaryWithCapacity:0];
            for (NSDictionary *enumd in enums) {
                NSString *_cname = enumd[@"name"];
                NSString *_caption = _get_caption(enumd);
                NSString *_hint = _get_hint(enumd);
                [_enums_ setObject:[[GIEnum alloc] initEnum:_cname withCaption:_caption withHint:_hint withValues:enumd[@"values"]] forKey:_cname];
            }
        }
        
        NSArray *streams = json[@"streams"];
        
        if (streams) {
            _streams = [[GIOrderedDictionary alloc] initWithCapacity:0];
            for (NSDictionary *stream in streams) {
                [_streams setObject:[[GIStream alloc] initWithJson:stream withEnums:_enums_ andType:GI_STREAM_SUBSCRIPTION] forKey:stream[@"name"]];
            }
        }

        streams = json[@"queries"];
                
        if (streams) {
            
            if (!_streams)
                _streams = [[GIOrderedDictionary alloc] initWithCapacity:0];
            
            for (NSDictionary *stream in streams) {
                [_streams setObject:[[GIStream alloc] initWithJson:stream withEnums:_enums_ andType:GI_STREAM_REQUEST] forKey:stream[@"name"]];
            }            
        }
        
        //
        // !!! REQUEST AND ACTION IS NOT SUPPORTED YET !!!
        //
    }
    return self;
}


- (NSString*) description{
    return [NSString stringWithFormat:@"MARKETPLACE[name=%@/engine=%@/market=%@],\n    STREAMS = %@", _info.name, _info.engine, _info.market, [_streams descriptionWithLocale:[NSLocale currentLocale] indent:4]];
}

@end

@implementation GIConnectionStructure

- (id) initWithJson:(NSDictionary*)json{
    self = [super init];
    
    if (self) {        
        _marketplaces = [[GIOrderedDictionary alloc] initWithCapacity:0];
        for (NSString *mp in json) {
            [_marketplaces setObject:[[GIMarketPlace alloc] initMarketPLace:mp withJson:json[mp]]  forKey:mp];
        }
        
    }            
    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"STRUCTURE = %@", [_marketplaces descriptionWithLocale:[NSLocale currentLocale] indent:2]];
}
@end


@implementation GIStompStructure

- (id) init{
    self = [super init];
    return self;
}

- (void) updateMarketPlaceInfo: (GIMarketPlaceInfo*) anInfo{
    _marketplaceInfo = anInfo;
}

- (void) updateStream: (GIStream*) aStream{
    _stream = aStream;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@: %@", [_marketplaceInfo description], [_stream description]];
}

@end
