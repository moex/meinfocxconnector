//
//  GIStompStructure.h
//  GIConnector
//
//  Created by denn on 6/13/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIOrderedDictionary.h"

NSString* GIC_current_lang(void);

/**
 Predfined structure types.
 In fact, all types recognize automaticaly acording to json spec.
 Exceptions are TSMR enums such as TBuySell, TSecType,... 
 */
typedef enum{
    GI_STOMP_FIELD_ENUM = 1000,
    GI_STOMP_FIELD_STRING,
    GI_STOMP_FIELD_BOOL,
    GI_STOMP_FIELD_INT,
    GI_STOMP_FIELD_TIME,
    GI_STOMP_FIELD_DATETIME,
    GI_STOMP_FIELD_DATE,
    
    //
    // Fixed type has 2 numbers defined in a Array [number,precision]
    // 
    GI_STOMP_FIELD_FIXED,
    GI_STOMP_FIELD_FLOAT,

    //
    // Some unknown type arrived
    //
    GI_STOMP_FIELD_UNKNOWN
} GIStompType;

//
// ACCESS types
//
typedef enum{
    //
    // Public 
    //
    GI_ACCESS_GENERAL = 10000,
    
    //
    // User specific or private
    //
    GI_ACCESS_PRIVATE,
    
    //
    // Some unknown type arrived
    //
    GI_ACCESS_UNKNOWN
} GIStreamAccessType;


//
// Stream types
//

typedef enum {    
    //
    // Messaging subscription
    // 
    GI_STREAM_SUBSCRIPTION = 100,

    //
    // Request means one replay for one query
    //
    GI_STREAM_REQUEST ,
    
#warning THIS version does not support request and action recognition
    //
    // Action, transaction, etc...
    //
    GI_STREAM_ACTION
    
} GIStreamType;

/**
 *  ICX Enum value.
 */
@interface GIEnumValue : NSObject
/**
 *  Value.
 */
@property(readonly,nonatomic) NSString *value;

/**
 *  Value caption.
 */
@property(readonly,nonatomic) NSString *caption;

/**
 *  Value hint.
 */
@property(readonly,nonatomic) NSString *hint;

/**
 *  Parse and create enum from a dictionary. Usually the dict received from JSON object.
 *  
 *  @param dict json object.
 *  
 *  @return enum explanation.
 */
- (id) initEnumValue: (NSDictionary*)dict;
@end


/**
 *  Enum.
 */
@interface GIEnum : NSObject

/**
 *  Enum name.
 */
@property(readonly,nonatomic) NSString *name;

/**
 *  Enum caption.
 */
@property(readonly,nonatomic) NSString *caption;

/**
 *  Enum hint.
 */
@property(readonly,nonatomic) NSString *hint;

/**
 *  Enum values.
 */
@property(readonly,nonatomic) GIOrderedDictionary *values;

/**
 *  Parse and create named Enum from JSON dictionary.
 *  
 *  @param aName    enum name.
 *  @param aCaption enum desc.
 *  @param aHint    enum hint.
 *  @param aValues  enum json dict.
 *  
 *  @return parsed enum.
 */
- (id) initEnum: (NSString*)aName withCaption:(NSString*)aCaption withHint:(NSString*)aHint withValues:(NSDictionary*)aValues;
@end

//
// Common part for stream output parts.
//

/**
 *  Common STOMP stream description.
 */
@interface __GIOutputPart : NSObject

/**
 *  Name.
 */
@property(readonly,nonatomic) NSString *name;

/**
 * Caption.
 */
@property(readonly,nonatomic) NSString *caption;

/**
 * Field type.
 */
@property(readonly,nonatomic) GIStompType type;

/** If field type is enum.
*/
@property(readonly, nonatomic) GIEnum *enumType;

/**
 Data capacity.
*/
@property(readonly,nonatomic) int length;

/** Create it from received connection packet.
*/
- (id) initWithJson: (NSDictionary*) json;

@end

/**
 *  Fields obtained as description of stream, query or action.
 */
@interface GIField : __GIOutputPart

/**
 *  Field hint
 */
@property(readonly,nonatomic) NSString *hint;

/**
 * Field can be described as trend by other field. Trend refere to name another Field.
 */
@property(readonly,nonatomic) NSString *trendBy; 

/**
 * Field attributes
 */
@property(nonatomic) BOOL isSystem, isHidden, hasPercent, isStatic;

/**
 *  Create it from received connection packet.
 *  
 *  @param json JSON dictionary with field description.
 *  
 *  @return parsed Field.
 */
- (id) initWithJson: (NSDictionary*) json;
@end

/**
 * Params can be added to select specific response.
 * They use with "selector" stomp header.
 */
@interface GIParam : __GIOutputPart
/**
 *  Parameters can be optional.
 */
@property(readonly,nonatomic) BOOL isOptional;

/**
 *  Parse params from input jSON.
 *  
 *  @param json JSON description.
 *  
 *  @return parsed param.
 */
- (id) initWithJson: (NSDictionary*) json;
@end

/** 
 Stream or set of subscriptions structure.
 Server has flexible configuration structure. Different objects can be applied theirs structures to server bus.
 This is description publich to Stream structure which contains fields, params to select stream, types. Stream is named and has some aditional description such as caption which can be localized for the current app language.
 */
@interface GIStream : NSObject

/**
 *  Stream name.
 */
@property(readonly,nonatomic) NSString *name;

/**
 *  Stream caption. Localized.
 */
@property(readonly,nonatomic) NSString *caption;

/**
 *  Stream fields.
 *  @see GIField.
 */
@property(readonly,nonatomic) GIOrderedDictionary *fields;

/**
 *  Stream params.
 *  @see GIParam.
 */
@property(readonly,nonatomic) GIOrderedDictionary *params;

/**
 *  Stream access type. It can be general or private.
 */
@property(readonly,nonatomic) GIStreamAccessType accessType;

/**
 *  Stream type. It can be SUBSCRIPTION, REQUEST or ACTION (TRANSACTION, active request).
 *  REQUEST deffers from SUBSCRIPTION that replies only one time after recive message with action to server.
 *  ACTION deffers from REQUEST that can change server state.
 */
@property(readonly,nonatomic) GIStreamType type;

/**
 *  Parse Stream structure from JSON description.
 *  
 *  @param json  json description.
 *  @param enums enums.
 *  @param type  stream type.
 *  
 *  @return parsed stream structure.
 */
- (id) initWithJson: (NSDictionary*)json withEnums: (NSDictionary *)enums andType: (GIStreamType) type;

@end

/**
 *  Information about name space of server objects.
 * Every application which can publish object to server bus should define what namespace its objects belongs.
 */
@interface GIMarketPlaceInfo : NSObject

/**
 *  Name.
 */
@property(readonly,nonatomic) NSString *name;

/**
 *  Every namespace comprise two definition index. Engine is which systen publish objects.
 */
@property(readonly,nonatomic) NSString *engine;

/**
 *  Every namespace comprise two definition index. Market defines what part of engine systen publish objects.
 */
@property(readonly,nonatomic) NSString *market;
@end

/**
 *  Namespace structure.
 */
@interface GIMarketPlace : NSObject
/**
 *  Information about the marketplace.
 */
@property(readonly,nonatomic) GIMarketPlaceInfo   *info;
/**
 *  Streams comprise the marketplace.
 */
@property(readonly,nonatomic) GIOrderedDictionary *streams;

/**
 *  Parse from JSON and create marketplace structure definition.
 *  
 *  @param mp   market place name.
 *  @param json JSON description.
 *  
 *  @return parsed market place.
 */
- (id) initMarketPLace:(NSString*)mp withJson:(NSDictionary*)json;
@end

/**
 *  Global definition of connection structure received for one connection abd one user.
 */
@interface GIConnectionStructure : NSObject
/**
 *  Market places comprise connection for the user. Collection of GIMarketPlace.
 *  @see GIMarketPlace.
 */
@property(readonly, nonatomic) GIOrderedDictionary *marketplaces;

/**
 *  Parse and create connection structure.
 *  
 *  @param json JSON description.
 *  
 *  @return parsed structure.
 */
- (id) initWithJson:(NSDictionary*)json;
@end

/**
 * References to concrete stomp frame structure. Any STOMP object can refer to global connection structure.
 */
@interface GIStompStructure : NSObject

/**
 * One stomp frame can be associated with a one stream and one market place.
 */
@property(readonly,nonatomic) GIMarketPlaceInfo   *marketplaceInfo;

/**
 *  Stream associated with the object.
 */
@property(readonly,nonatomic) GIStream *stream;

/**
 *  Update the object market place info.
 *  
 *  @param anInfo market place info.
 */
- (void) updateMarketPlaceInfo: (GIMarketPlaceInfo*) anInfo;

/**
 *  Update  the object stream definition.
 *  
 *  @param aStream stream definition.
 */
- (void) updateStream: (GIStream*) aStream;
@end


