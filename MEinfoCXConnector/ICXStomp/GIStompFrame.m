//
//  GIStompFrame.m
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompFrame.h"

//
// COMMON
//
@interface GICommonStompFrame ()
@property(nonatomic,strong) NSString *name;
@end

@implementation GICommonStompFrame
{
    NSMutableDictionary  *_headers;
    NSMutableArray       *_header_keys; // to order
    NSMutableData        *_data;
}

@synthesize name;

- (BOOL) isEqualWithHeaders:(id)object{
    GICommonStompFrame *_stomp = object;
    
//    if (_stomp.headers.count!=self.headers.count) {
//        return NO;
//    }
//    
//    for (NSString *key in _stomp.headers) {
//        //NSLog(@" ##### KEY: %@: %@  <==>  %@", key, _stomp.headers[key], self.headers[key]);
//        if (!self.headers[key] || ![self.headers[key] isEqual:_stomp.headers[key]]) {
//            return NO;
//        }
//    }
    
    NSString *destination = [_stomp.headers objectForKey:GI_FRAME_DESTINATION_STRING];
    NSString *selector = [_stomp.headers objectForKey:GI_FRAME_SELECTOR_STRING];
    
    if (destination && [destination isEqualToString:[self.headers objectForKey:GI_FRAME_DESTINATION_STRING]]) {
        
        if (selector && [selector isEqualToString:[self.headers objectForKey:GI_FRAME_SELECTOR_STRING]]){
            //NSLog(@" ##### IS EQUAL: %@  <==>  %@", _stomp.headers, self.headers);
            return YES;
        }
        else
            return NO;
        
        return YES;
    }
    
    return NO;
}

- (id) copyWithZone:(NSZone *)zone{
    GICommonStompFrame *_new_obj = [[[self class] allocWithZone:zone] init];
    
    if (_new_obj) {
        _new_obj->name = [self.name copy];
        _new_obj->_headers = [self.headers mutableCopy];
        _new_obj->_data = [self->_data mutableCopy];
        _new_obj->_body = [self->_body copy];
        _new_obj->_header_keys = [self->_header_keys copy];
    }
    
    return _new_obj;
}

- (NSDictionary*) headers{
    return _headers;
}

- (id)   getHeader:(NSString*)key{
    return [_headers objectForKey:key];
}

- (void) setHeader:(NSString *)key withValue:(id)value{
    @try {
        _data = nil;
        if (![_headers valueForKey:key]){
            [_header_keys addObject:key];
        }
        [_headers setObject:value forKey:key];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
    }
}

- (void) updateFromData:(NSData *)aData{
    
    _headers     = [[NSMutableDictionary alloc] init];
    _header_keys = [[NSMutableArray alloc] init];
    _data = nil;
        
    if (aData) {            
        @try {
            self.name = nil;
            
            int  _header_delimeter_index    = 0;
            int  _header_delimeter_direct_index    = 0;
            BOOL _header_delimeter_is_found = NO;
            int  _header_value_len = 0;

            NSMutableString *_line = [NSMutableString stringWithCapacity:[aData length]]  ;
            NSUInteger dataLength = [aData length];
            char _to_prev_stomp_marker='\0', _after_stomp_maker='\0' ;

            for (int i=0; i<dataLength; i++) {
                
                //
                // find STOMP marker
                //
                char _stomp_marker = ((char*)[aData bytes])[i];

                if (i>=1)
                    _to_prev_stomp_marker = ((char*)[aData bytes])[i-1];
                
                if (i<dataLength-1)
                    _after_stomp_maker = ((char*)[aData bytes])[i+1];
                
                
                //
                // keep : for header
                //
                if(!_header_delimeter_is_found) _header_delimeter_index++;
                _header_value_len ++;
                if(_stomp_marker == ':')        {
                    _header_value_len = 0;
                    _header_delimeter_direct_index = i;
                    _header_delimeter_is_found = YES;
                }
                
                if (_stomp_marker == '\n' && _to_prev_stomp_marker!='\n'){
                    
                    //
                    // name
                    //
                    if (!self.name) {
                        self.name = [[NSString stringWithString: _line] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        [_line setString:@""];
                        _header_delimeter_index = 0;
                    }
                    else{
                        if (_header_delimeter_index>0 && _header_delimeter_is_found) {
                            
                            //
                            // headers
                            //
                            NSString *_h = [[_line substringToIndex:_header_delimeter_index-1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            NSData *_vv = [aData subdataWithRange:NSMakeRange(_header_delimeter_direct_index+1, _header_value_len-1)];
                            NSString *_v = [[NSString alloc] initWithBytes:[_vv bytes] length: [_vv length] encoding: NSUTF8StringEncoding];
                            
                            [_headers setValue:_v forKey:_h];
                            
                            [_line setString:@""];
                            
                            _header_delimeter_index = 0;
                            _header_delimeter_is_found = NO;
                        }
                    }
                }
                else //
                    //if(_stomp_marker == '{' && _to_prev_stomp_marker!='\'' && _to_prev_stomp_marker!='\\'){
                    if (_stomp_marker == '\n' && _to_prev_stomp_marker == '\n') {
                    //
                    // Breake to parse data block "on demand" if we'll need
                    //
                    
                    unsigned long _last_index = dataLength-1;
                    
                    // trim the end of data
                    for(unsigned long l = _last_index+1; l>i; l--){
                        char _stomp_marker = ((char*)[aData bytes])[l];
                        //if(_stomp_marker=='}' && _after_stomp_maker!='\\' && _after_stomp_maker!='\''){
                        if (_stomp_marker == '\0') {
                            _last_index = l-i;
                            break;
                        }
                    }
                    
                    if (i<_last_index) {
                        //
                        // create new body
                        //
                        self.body = [[GIStompBody alloc] initFromData: [aData subdataWithRange:NSMakeRange(i, _last_index)] ];
                    }
                    else
                        self.body = nil;
                    break;
                }
                else if (_stomp_marker=='\0'){
                    break;
                }
                else
                    //
                    // keep line
                    //
                    [_line appendFormat:@"%c" ,_stomp_marker];
            }
        }
        @catch (NSException *exception) {
            NSString *_buffer = [[NSString alloc] initWithBytes:[aData bytes] length: [aData length] encoding: NSStringEncodingConversionAllowLossy];   

            self.name = @"ERROR";
            _headers = [NSMutableDictionary 
                        dictionaryWithDictionary:@{
                                                   @"message": @"could not parse STOMP message",
                                                   @"error-code": @"error.stomp.parser-error"
                                                   }];
            
            NSLog(@"STOMP Parser Exception %@ %s:%i buffer=<%@>", exception, __FILE__, __LINE__, _buffer);
        }
    }
}

- (id) initWithName:(NSString *)aName{
    
    if (!(self=[super init]))
        return nil;
    
    self.name = [aName uppercaseString];
    
    _headers     = [[NSMutableDictionary alloc] init];
    _header_keys = [[NSMutableArray alloc] init];
    _data = nil;
    
    return self;
}

- (id) initWithData:(NSData *)aData{
    
    if (!(self=[super init]))
        return nil;
    
    [self updateFromData:aData];
    
    return self;
}

- (NSData*) stompFrame{
    
    if (!_data){
        @try {
            _data = [[NSMutableData alloc] initWithData:[self.name dataUsingEncoding:NSUTF8StringEncoding]];
            
            //[_data appendBytes:[self.name cStringUsingEncoding:NSUTF8StringEncoding] length:[self.name length]];
            [_data appendBytes:"\n" length:1];
            
            for (id key in [_header_keys objectEnumerator]) {
                [_data appendData:[[NSString stringWithFormat:@"%@: %@", key, _headers[key]] dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
                [_data appendBytes:"\n" length:1];
            }
            
            [_data appendData: [self.body dataWithJSONObject]];
            
            [_data appendBytes:"\n\n\0" length:3];
        }
        @catch (NSException *exception) {
            NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        }
    }
    return _data;
}

- (NSString*) description{
    NSMutableString *headers = [[NSMutableString alloc] init];
    [headers appendString:@"{\n"];
    for (NSString *k in self.headers) {
        [headers appendFormat:@"  %@: %@\n", k, self.headers[k]];
    }
    [headers appendString:@"}"];
    return [NSString stringWithFormat:@"%@\n%@,\n%@", self.name, headers, self.body];
}

@end



//
// Responses Queue
//
@implementation GIStompFrameQueue
@end

//
// ROOT OF STOMP
//

@interface GIStomp()
@property(strong, atomic)    GICommonStompFrame *lastResponse;
@property(strong, nonatomic) GIStompFrameQueue  *responseQueue;
@property(assign, nonatomic) BOOL isDispatched;
@end

@implementation GIStomp

@synthesize  responseQueue=_responseQueue, lastResponse=_lastResponse, isDispatched=_isDispatched;

- (void) hardSetStructure:(GIConnectionStructure *)structure{
    if ([self respondsToSelector:@selector(marketPlace)]) {
        NSString *mp = [self marketPlace];
        if (mp) {
            if ([[self class] respondsToSelector:@selector(destination)]) {
                GIMarketPlace *mps = structure.marketplaces[mp];
                
                if (mps) 
                    [self.structure performSelector:@selector(updateMarketPlaceInfo:) withObject:mps.info];                    
                
                NSString *stream = [[self class] destination];
                if (stream)
                    [self.structure performSelector:@selector(updateStream:) withObject:mps.streams[stream]];
            }
        }
    }
}

- (id) initWithRequestName:(NSString *)rqname andResponseName:(NSString *)rpname{
    if (!(self=[super init])) {
        return nil;
    }
    
    _lastResponse = nil;
    _request  = [[GICommonStompFrame alloc] initWithName:rqname];
    _responseQueue = [[GIStompFrameQueue alloc] init];
    _structure = [[GIStompStructure alloc] init];    
    return self;
}

- (void) messageReceived:(GICommonStompFrame *)frame{
    _lastResponse = frame;
    [_responseQueue push:frame];
}

- (GICommonStompFrame*) lastResponse{
    return _lastResponse;
}

- (void) setLastResponse:(GICommonStompFrame *)lastResponse{
    [_responseQueue push:lastResponse];
}

- (void) setResponseQueueMaxSize:(NSUInteger)size{
    [_responseQueue setMaxSize:size];
}

- (NSString*) description{
    return [NSString stringWithFormat:@"Request[dispatched:%i]: %@\nResponse: %@\n", self.isDispatched, self.request, self.lastResponse];
}

@end
