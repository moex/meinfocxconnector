//
//  GIStompDispatcher.m
//  GIConnector
//
//  Created by denn on 06/06/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompDispatcher.h"
#import "GIAuth.h"
#import "GICSPTunnel.h"
#import "GITunnelProtocol.h"
#import "GIWSPTunnel.h"
#import "GICSPError.h"
#import "GIOrderedDictionary.h"
#import "GIBundle.h"
#import "GIStompFrame.h"

@interface GIStomp()
@property(strong, atomic) GICommonStompFrame *lastResponse;
@property(nonatomic) GIStompFrameQueue  *responseQueue;
@property(nonatomic) BOOL isDispatched;
@end

@interface GIStompMessage()
@property (nonatomic,strong) NSTimer *deferredTimer;
@property (nonatomic,assign) NSUInteger references;
@end

@interface GIDispatchedStomp : NSObject

//- (id) initWithTunnel: (GICSPTunnel*) tunnel;
- (id) initWithTunnel: (id<GITunnelProtocol>) tunnel;

- (NSArray*) frames;
- (GIOrderedDictionary*) subscriptions;
- (GIOrderedDictionary*) requests;

- (void) addFrame: (GIStomp*) stomp;
- (void) sendFrames;
- (void) sendRequests;

- (GIRequestStompFrame*) getRequest: (id) requestID;

- (GIStompMessage*) getSubscription: (id) subscriptionID;
- (void) removeSubscription: (GIStompMessage*) subscription immediately:(BOOL)immediately;
- (void) justRemoveSubscription: (GIStompMessage*) subscription;

- (void) sendStomp: (GIStomp*) stomp;
- (void) sendLastMessage:(GIStompMessage*)message;
- (void) sendLastRequest;

@property(nonatomic,strong)     GIConnectionStructure *structure;
@property(nonatomic,assign)     NSInteger   logLevel;
@property(nonatomic,assign)     GIConfig    *config;
@property(nonatomic,assign)     NSTimeInterval deferredTime;
@end

@implementation GIDispatchedStomp
{
    id<GITunnelProtocol> tunnel;
    GIOrderedDictionary *requests_;
    GIOrderedDictionary *subscriptions_;
    NSMutableArray *frames_;
}

- (id) initWithTunnel:(id<GITunnelProtocol>)aTunnel{
    self = [super init];
    
    if (self) {
        tunnel = aTunnel;
        frames_ = [NSMutableArray arrayWithCapacity:0];
        requests_ = [GIOrderedDictionary dictionaryWithCapacity:0];
        subscriptions_ = [GIOrderedDictionary dictionaryWithCapacity:0];
    }
    return self;
}

- (void) setTunnel: (id<GITunnelProtocol>)aTunnel{
    tunnel = aTunnel;
}

- (void) addRequest: (GIRequestStompFrame*) request{
    if ([requests_ objectForKey:request.requestID]) {
        return;
    }
    request.isDispatched=NO;
    [requests_ setObject:request forKey:request.requestID];
}

- (void) sendLastRequest{
    id key = [requests_ keyAtIndex:requests_.count-1];
    [self sendStomp:requests_[key]];
}


- (GIRequestStompFrame*) getRequest: (id) requestID{
    return [requests_ objectForKey:requestID];
}

- (GIStompMessage*) addSubscription: (GIStompMessage*) subscription{
    
    if (!subscription) {
        return nil;
    }
    
    GIStompMessage *message = [subscriptions_ objectForKey:subscription.subscriptionID];
    if (message) {
        message.references++;        
    }
    
    void(^invalidateTimer)(GIStompMessage *mes) = ^(GIStompMessage *mes) {
        if (mes.deferredTimer) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (_logLevel>=GI_STOMP_LOG_DEBUG) {
                    NSLog(@"###... GIStompDispatcher:addSubscription: Message has deferred timer. Message: %@: %@: %@. Invalidate timer. refs=%lu", mes.subscriptionID, [mes destination], mes.request.headers[@"selector"], (unsigned long)mes.references);
                }
                [mes.deferredTimer invalidate];
                mes.deferredTimer = nil;
            });
        }
    };
    
    if (message) {
        invalidateTimer(message);
        if (message.references>1) {
            message.references=1;
            return nil;
        }
        return message;
    }
    
    for (id st in subscriptions_) {
        message = subscriptions_[st];
        if ([message.request isEqualWithHeaders:subscription.request]) {
            invalidateTimer(message);
            if (message.references>1) {
                message.references=1;
                return nil;
            }
            return message;
        }
    }
    
    subscription.isDispatched = NO;
    subscription.references=1;
    [subscriptions_ setObject:subscription forKey:subscription.subscriptionID];
    
    return subscription;
}

- (void) sendLastMessage:(GIStompMessage*) message{
    if (!message.isDispatched) {
        [self sendStomp:message];
    }
    else if (message.delegate && [message.delegate respondsToSelector:@selector(didSubscriptionRestore:)]) {
        [message.delegate didSubscriptionRestore:message];
    }
    else{
        if (_logLevel>=GI_STOMP_LOG_DEBUG) {
            NSLog(@"###... GIStompDispatcher:Subscription restoring.... %@: %@ %@ delegate=%@", message.subscriptionID, [message destination], message.request.headers[@"selector"], message.delegate);
        }
    }
}

- (void) removeRequest: (id) requestID{
    GIRequestStompFrame *request = [self getRequest:requestID];
    if (!request)
        return;
    request.isDispatched=NO;
    [requests_ removeObjectForKey:requestID];
}

- (void) justRemoveSubscription: (GIStompMessage*) subscription{
    GIStompMessage *message = [self getSubscription:subscription.subscriptionID];
    if (!message)
        // already unsubscribe
        return;
    message.isDispatched=NO;
    [subscriptions_ removeObjectForKey:subscription.subscriptionID];
}

- (void) deferredUnsubscribeTimer:(NSTimer*) timer{
    GIStompMessage *message = (GIStompMessage*)timer.userInfo;
    
    if (_logLevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"###... GIStompDispatcher:deferredUnsubscribeTimer: Message start deferred timer. Message: %@: %@: %@ refs=%li", message.subscriptionID, [message destination], message.request.headers[@"selector"], (unsigned long)message.references);
    }
    
    [self unsubscribe:message];
}

- (void) unsubscribe:(GIStompMessage*)message{
    
    GIStompUnsubscriber *unsubscriber = [GIStompUnsubscriber makeForMessage:message];
    [subscriptions_ removeObjectForKey:message.subscriptionID];
    message.isDispatched=NO;

    if (_logLevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"###... GIStompDispatcher:unsubscribe: Message has deferred timer. Message: %@: %@: %@.", message.subscriptionID, [message destination], message.request.headers[@"selector"]);
    }

    if (tunnel)
        [tunnel send: [unsubscriber.request stompFrame]];
}

- (void) removeSubscription: (GIStompMessage*) subscription immediately:(BOOL)immediately{
    @try {
        GIStompMessage *message = [self getSubscription:subscription.subscriptionID];
        
        if (message==0){
            // already unsubscribe
            if (immediately) {
                for (id st in subscriptions_) {
                    message = subscriptions_[st];
                    if ([message.request isEqualWithHeaders:subscription.request]) {
                        if (message.references<=1)
                            [self unsubscribe:message];
                    }
                }
            }
            return;
        }
        
        if (message.references==0){
            if (message.deferredTimer) {
                if (_logLevel>=GI_STOMP_LOG_DEBUG) {
                    NSLog(@"###... GIStompDispatcher:removeSubscription: Message has deferred timer. Message: %@: %@: %@. Invalidate timer.", message.subscriptionID, [message destination], message.request.headers[@"selector"]);
                }
                [message.deferredTimer invalidate];
            }
            [self unsubscribe:message];
            return;
        }
        
        message.references--;
        
        if (immediately) {
            [self unsubscribe:message];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (message.deferredTimer) {
                    if (_logLevel>=GI_STOMP_LOG_DEBUG) {
                        NSLog(@"###... GIStompDispatcher:removeSubscription: Message has deferred timer. Message: %@: %@: %@. Invalidate timer.", message.subscriptionID, [message destination], message.request.headers[@"selector"]);
                    }
                    [message.deferredTimer invalidate];
                }
                message.deferredTimer = [NSTimer scheduledTimerWithTimeInterval: self.deferredTime target:self selector:@selector(deferredUnsubscribeTimer:) userInfo:message repeats:NO];
            });
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
}

- (GIStompMessage*) getSubscription: (id) subscriptionID{
    return [subscriptions_ objectForKey:subscriptionID];
}

- (void) addFrame:(GIStomp *)stomp{
    for (GIStomp *st in frames_) {
        GICommonStompFrame *request = st.request;
        if ([stomp.request isEqualWithHeaders:request]) {
            return;
        }
    }
    [frames_ addObject:stomp];
}

- (void) sendStomp: (GIStomp*) stomp{
    [stomp performSelector:@selector(hardSetStructure:) withObject:_structure];
    if (_logLevel>=GI_STOMP_LOG_TRACE) {
        NSLog(@"###... GIStompDispatcher:Send stomp: %@", stomp);
    }
    
    if ([stomp isKindOfClass:[GIRequestStompFrame class]]) {
        GIRequestStompFrame *r=(GIRequestStompFrame*)stomp;
        if (r.delegate && [r.delegate respondsToSelector:@selector(willFrameSend:)]) {
            [r.delegate willFrameSend:r];
        }
    }
    
    if ([stomp isKindOfClass:[GIStompMessage class]]) {
        GIStompMessage *r=(GIStompMessage*)stomp;
        if (r.delegate && [r.delegate respondsToSelector:@selector(willFrameSend:)]) {
            [r.delegate willFrameSend:r];
        }
    }
    
    [tunnel send:[stomp.request stompFrame]];
}

- (void) sendFrames{
    for (GIStomp *stomp in frames_) {
        [self sendStomp:stomp];
    }
}

- (void) sendRequests{
    for (NSNumber *requestID in requests_) {
        [self sendStomp:[requests_ objectForKey:requestID]];
    }
}

- (void) sendSubscriptions{
    for (id subscriptionID in subscriptions_) {
        [self sendStomp:[subscriptions_ objectForKey:subscriptionID]];
    }
}

- (NSArray*) frames{
    return frames_;
}

- (GIOrderedDictionary*) requests{
    return requests_;
}

- (GIOrderedDictionary*) subscriptions{
    return subscriptions_;
}

@end


@interface GIStompDispatcher()
@property(nonatomic,strong) GIDispatchedStomp    *messages;
@property(nonatomic,strong) id<GITunnelProtocol> tunnel;
@property(nonatomic,strong) NSMutableDictionary  *hungSubscriptions;
@property(atomic,strong)     dispatch_queue_t    connectionQueue;
@end

@implementation GIStompDispatcher
{
    
    //
    // main stomp task queue
    //
    dispatch_queue_t    stomponReadQueue;
    dispatch_queue_t    stomponCommandQueue;
    //dispatch_queue_t    connectionQueue;
    
    //
    // Common configuration
    //
    GIConfig             *config;
    
    //
    // Connection interfac structure
    //
    NSString  *interfaceStructure;
    
    //
    // Deffered frames
    //
    GIDispatchedStomp *stompConnection;
    BOOL               isStopConnectionOpened;
    
    
    NSTimeInterval startConnectionTime, endConnectionTime;
    NSTimer *hungSubscriptionFlushTimer;
}


@synthesize messages=_messages, tunnel=_tunnel;

- (GIDispatchedStomp*) messages{
    if (!_messages) {
        _messages = [[GIDispatchedStomp alloc] initWithTunnel:self.tunnel];
        _messages.logLevel = _loglevel;
        _messages.config = config;
        _messages.deferredTime = self.deferredTime;
    }
    
    return _messages;
}

- (NSMutableDictionary*) hungSubscriptions{
    if (_hungSubscriptions) {
        return _hungSubscriptions;
    }
    
    _hungSubscriptions = [[NSMutableDictionary alloc] init];
    
    return _hungSubscriptions;
}

- (id<GITunnelProtocol>) tunnel{
    if (!_tunnel) {
        
        if (config.tunnelType == GI_TUNNEL_CSP) {
            _tunnel = [[GICSPTunnel alloc] initWithGIConfig:config];
        }
        else{
            _tunnel = [[GIWSPTunnel alloc] initWithGIConfig:config];
        }
        
        _tunnel.connectionHandler = self;
        
        [stompConnection setTunnel:_tunnel];
        [self.messages setTunnel:_tunnel];
        
        [_tunnel open];
    }
    
    return _tunnel;
}

- (void) setTunnel:(id<GITunnelProtocol>)tunnel{
    
    if (_loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"###... GIStompDispatcher:setTunnel: %@", tunnel);
    }

    if (_tunnel) {
        id<GITunnelProtocol> tmp_tunnel = _tunnel;
        _tunnel = tunnel;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(config.connectionTimeout * NSEC_PER_SEC)), self.connectionQueue, ^{
            //
            // stop completely after connection timeout
            //
            tmp_tunnel.connectionHandler = nil;
            [tmp_tunnel close];
        });
    }
}

- (void) interruptStomp{
    
    @try {
        for (id messid in self.messages.subscriptions) {
            GIStompMessage *mess = self.messages.subscriptions[messid];
            mess.isDispatched = NO;
            if (mess.delegate && [mess.delegate respondsToSelector:@selector(didFrameInterrupt:withError:)]) {
                [mess.delegate didFrameInterrupt:mess withError:GI_STOMP_STRING_ERROR(GILocalizedString(@"Subscribtion has been interupted",@"Stomp subscription is incompleted"))];
            }
        }
        
        for (id reqid in self.messages.requests) {
            GIRequestStompFrame *request = self.messages.requests[reqid];
            request.isDispatched = NO;
            if (request.delegate && [request.delegate respondsToSelector:@selector(didFrameInterrupt:withError:)]) {
                [request.delegate didFrameInterrupt:request withError:GI_STOMP_STRING_ERROR(GILocalizedString(@"Request has been interrupted", @"Stomp request is incompleted"))];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
    }
    @finally {
        [self.messages.subscriptions removeAllObjects];
        [self.messages.requests removeAllObjects];
    }
    
}

- (id) initWithConfig:(GIConfig *)aConfig{
    return [self initWithConfig:aConfig andConnectImmediately:NO];
}

- (dispatch_queue_t) connectionQueue {
    if (!_connectionQueue){
        _connectionQueue = dispatch_queue_create("com.infocx.stomp.connection.queue", DISPATCH_QUEUE_SERIAL);
    }
    
    return _connectionQueue;
}

- (id) initWithConfig:(GIConfig *)aConfig andConnectImmediately:(BOOL)connectImmediately{
    self = [super init];
    
    if (self) {
        _loglevel = GI_STOMP_LOG_INFO;
        
        _hasPermanentConnection = NO;
        config = aConfig;
        stomponReadQueue = dispatch_queue_create("com.infocx.stomp.reader.queue", DISPATCH_QUEUE_SERIAL);
        stomponCommandQueue = dispatch_queue_create("com.infocx.stomp.command.queue", DISPATCH_QUEUE_SERIAL);
        //connectionQueue = dispatch_queue_create("com.infocx.stomp.connection.queue", DISPATCH_QUEUE_SERIAL);
        _deferredTime = config.readTimeout;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            hungSubscriptionFlushTimer = [NSTimer scheduledTimerWithTimeInterval:config.connectionTimeout target:self selector:@selector(flushHungSubscriptions:) userInfo:nil repeats:NO];
        });
        
    }
    
    return self;
}

- (void) connect{
    
    if (self.isRunning){
        return;
    }
    
    dispatch_async(self.connectionQueue, ^{
        
        if (_loglevel>=GI_STOMP_LOG_DEBUG) {
            NSLog(@"###... GIStompDispatcher:connect: tunnel=%@", _tunnel);
        }

        startConnectionTime = [NSDate timeIntervalSinceReferenceDate];
        
        GIStompConnect *connectionStomp = [GIStompConnect withLogin: config.login.user withPasscode: config.login.password andDomain:config.login.domain withLang:config.lang];
        stompConnection = [[GIDispatchedStomp alloc] initWithTunnel:self.tunnel];
        stompConnection.logLevel = _loglevel;
        stompConnection.config = config;
        stompConnection.deferredTime = self.deferredTime;
        
        [stompConnection addFrame: connectionStomp];
        if ([self.tunnel isOpened]) {
            [stompConnection sendFrames];
        }
    });
}

- (void) disconnect{
    
    if (_loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"###... GIStompDispatcher:disconnect: tunnel=%@", _tunnel);
    }

    [self interruptStomp];
    
    id<GITunnelProtocol> tunnel = _tunnel;
    if (stompConnection && isStopConnectionOpened) {
        stompConnection = nil;
        isStopConnectionOpened = NO;
        if (_tunnel) {
            dispatch_async(self.connectionQueue, ^{
                
                if(self.eventHandler && [self.eventHandler respondsToSelector:@selector(dispatcherWillStop)]){
                    [self.eventHandler dispatcherWillStop];
                }
                
                GIStompDisconnect *_disconnect = [GIStompDisconnect make];
                
                [tunnel send:[_disconnect.request stompFrame]];
                
                self.tunnel = nil;
                
                if(self.eventHandler && [self.eventHandler respondsToSelector:@selector(dispatcherDidStop)]){
                    [self.eventHandler dispatcherDidStop];
                }
                
            });
        }
    }
    else {
        self.tunnel = nil;
        stompConnection = nil;
        isStopConnectionOpened = NO;
    }
}

- (void) registerRequest:(GIRequestStompFrame*)request{
    [self.messages addRequest:request];
    if (isStopConnectionOpened) {
        [self.messages sendLastRequest];
    }
}

- (void) registerMessage:(GIStompMessage *)aMessage{
    GIStompMessage *message = [self.messages addSubscription:aMessage];
    if (isStopConnectionOpened && message) {
        [self.messages sendLastMessage: message];
    }
}

- (void) unregisterMessage:(GIStompMessage *)message{
    if (!self.isRunning) {
        [self.messages justRemoveSubscription:message];
        return;
    }
    [self.messages removeSubscription:message immediately:NO];
}

- (void) unregisterMessage:(GIStompMessage*)message immediately:(BOOL)immediately{
    if (!self.isRunning) {
        [self.messages justRemoveSubscription:message];
        return;
    }
    [self.messages removeSubscription:message immediately:immediately];
}


- (void) flushHungSubscriptions:(NSTimer*)timer{
    if (_loglevel>=GI_STOMP_LOG_DEBUG) {
        if (self.hungSubscriptions.count>0)
            NSLog(@"###... GIStompDispatcher:flushHungSubscriptions: tunnel=%@ hung subscriptions: %lu", _tunnel, (unsigned long)self.hungSubscriptions.count);
    }
    [self.hungSubscriptions removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        hungSubscriptionFlushTimer = [NSTimer scheduledTimerWithTimeInterval:config.connectionTimeout target:self selector:@selector(flushHungSubscriptions:) userInfo:nil repeats:NO];
    });
}


- (void) unregisterStomp:(GICommonStompFrame*)frame{
    if (self.isRunning) {
        id sid = frame.headers[@"subscription"];
        if ([self.hungSubscriptions objectForKey:sid] != nil) {
            if (_loglevel>=GI_STOMP_LOG_DEBUG) {
                NSLog(@"###... GIStompDispatcher:SUBSCRIPTION: %@ already is gone...: rp= %@:%@", sid, frame.headers[GI_FRAME_DESTINATION_STRING], frame.headers[GI_FRAME_SELECTOR_STRING]);
            }
            GIStompUnsubscriber *unsubscriber = [GIStompUnsubscriber makeForStomp:frame];
            [self.tunnel send: [unsubscriber.request stompFrame]];
            [self.hungSubscriptions setObject: unsubscriber forKey:sid];
        }
    }
}


- (BOOL) isRunning{
    return _tunnel !=nil && [_tunnel isOpened] && isStopConnectionOpened;
}

#pragma mark - protocol methodes

- (void) onRead:(NSData *)data{
    //
    // separate stomp blocks
    //
    // find \0 - end of stomp marker
    //
    
    __block NSInteger _block_marker_index = 0;
    for (NSInteger i=0; i<[data length]; i++) {
        
        //if (![self.tunnel isOpened]) {
        //    return;
        //}
        
        char _the_end_of_stopm;
        [data getBytes:&_the_end_of_stopm range:NSMakeRange(i, 1)];
        
        if (_the_end_of_stopm=='\0') {
            NSData *_block = [data subdataWithRange:NSMakeRange(_block_marker_index, i-_block_marker_index)];
            _block_marker_index = i+1;
            if (_block) {
                dispatch_sync(stomponReadQueue, ^(void){
                    [self postRead:_block];
                });
            }
        }
    }
}


- (void) loginFailed: (GICommonStompFrame*) stomp{
    if (self.loginHandler) {
        dispatch_async(stomponCommandQueue, ^(void){
            NSError *_error = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                                  code: GI_STOMP_LOGIN_FAILED
                                              userInfo: @{
                                                          NSLocalizedFailureReasonErrorKey: GILocalizedString(@"Authentication failed", @"Stomp error login faild reason key"),
                                                          NSLocalizedDescriptionKey: [NSString  stringWithFormat: GILocalizedString(@"Authentication failed: %@", @"Authentication error message"), stomp.headers[@"message"]]
                                                          }];
            
            GIError *_err = [[GIError alloc] initWithNSError:_error];
            
            [self.loginHandler didStompLoginError:_err withConnectionResponse:stomp];
        });
    }
}

- (void) processFrameError:(GICommonStompFrame*)stomp{
    NSString *receiptString = stomp.headers[@"receipt-id"];
    NSNumber *receiptID = [NSNumber numberWithInteger: [receiptString integerValue]];
    NSString *subscriptionString = stomp.headers[@"subscription"];
    NSNumber *subscriptionID = [NSNumber numberWithInteger: [subscriptionString integerValue]];
    
    if (subscriptionString==nil && receiptString == nil) {
        [self.errorHandler didStompError:GI_STOMP_FRAME_ERROR(stomp)];
        if (_loglevel>=GI_STOMP_LOG_DEBUG) {
            NSLog(@"###... GIStompDispatcher:processGatewayErrors: %s:%i\n\n", __FILE__, __LINE__);
        }
        return;
    }
    
    if (receiptString==nil)
        receiptID = subscriptionID;
    
    GIStompMessage *message = [self.messages.subscriptions objectForKey:receiptID];
    
    if (!message) {
        GIRequestStompFrame *request = [self.messages.requests objectForKey:receiptID];
        if (request && request.delegate && [request.delegate respondsToSelector:@selector(didFrameError:withError:)]) {
            NSError *_error = GI_STOMP_FRAME_ERROR(stomp);
            [request.delegate didFrameError:request withError:[[GIError alloc] initWithNSError:_error]];
        }
    }
    else{
        if (message && message.delegate && [message.delegate respondsToSelector:@selector(didFrameError:withError:)]) {
            NSError *_error = GI_STOMP_FRAME_ERROR(stomp);
            [message.delegate didFrameError:message withError:[[GIError alloc] initWithNSError:_error]];
        }
    }
}

- (void) processGatewayErrors: (GICommonStompFrame*)stomp withErrorDomain:(NSString*)errorDomain{
    
    if (_loglevel>=GI_STOMP_LOG_DEBUG)
        NSLog(@"###... GIStompDispatcher:processGatewayErrors: %@, %s:%i\n\n", stomp, __FILE__, __LINE__);
    
    if (
        [errorDomain hasSuffix:@"cannot-authenticate"] ||
        [errorDomain hasPrefix:@"invalid-user"]
        ) {
        [self disconnect];
        [self loginFailed:stomp];
    }
    else if ([errorDomain hasPrefix:@"access-denied"] || !isStopConnectionOpened){
        NSString *receiptString = stomp.headers[@"receipt-id"];
        NSNumber *receiptID = [NSNumber numberWithInteger: [receiptString integerValue]];
        if (receiptID && receiptString) {
            GIRequestStompFrame *request = [self.messages.requests objectForKey:receiptID];
            if (request && request.delegate && [request.delegate respondsToSelector:@selector(didAccessDeny:withError:)]) {
                [request.delegate didAccessDeny:request withError:GI_STOMP_FRAME_ERROR(stomp)];
            }
            else{
                GIStompMessage *message = [self.messages.subscriptions objectForKey:receiptID];
                if (message && message.delegate && [message.delegate respondsToSelector:@selector(didAccessDeny:withError:)]) {
                    [message.delegate didAccessDeny:message withError:GI_STOMP_FRAME_ERROR(stomp)];
                }
            }
        }
        else {
            if (self.loginHandler && [self.loginHandler respondsToSelector:@selector(didStompLoginError:withConnectionResponse:)]){
                NSString *error_text = [NSString stringWithFormat:GILocalizedString(@"Login failed. Server could not authenticate because: %@", @"login timed out"),stomp.headers[@"message"]];
                [self.loginHandler didStompLoginError:GI_STOMP_STRING_ERROR(error_text) withConnectionResponse:stomp];
            }
        }
    }
    else{
        [self processFrameError:stomp];
    }
}


- (void) performStompError: (NSString*) errorString{
    @try {
        if (self.errorHandler) {
            
            dispatch_async(stomponCommandQueue, ^{
                NSError *_error = [NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX]
                                                      code:GI_STOMP_ERROR
                                                  userInfo:@{
                                                             NSLocalizedFailureReasonErrorKey: GILocalizedString(@"Stomp error", @"NSLocalizedFailureReasonErrorKey"),
                                                             NSLocalizedDescriptionKey: [NSString  stringWithFormat: GILocalizedString(@"Stomp error has occured: [3:%@]", @"Stomp error"), errorString]
                                                             }];
                
                GIError *_err = [[GIError alloc] initWithNSError:_error];
                [self.errorHandler didStompError: _err];
            });
        }
    }
    @catch (NSException *exception) {
        NSLog(@"###... GIStompDispatcher:DISPATCHER Exception %@ %s:%i", exception, __FILE__, __LINE__);
    }
}

- (void) postRead:(NSData *)data{
    @try {
        
        //if (![self.tunnel isOpened])
        //    return;
        
        GICommonStompFrame *rp = [[GICommonStompFrame alloc] initWithData:data];
        
        if (self.loglevel>=GI_STOMP_LOG_TRACE) {
            NSLog(@"###... GIStompDispatcher:Frame: %@\n\n", rp);
        }
        
        if ([rp.name isEqualToString:@"ERROR"]) {
            
            NSString *_error_code = rp.headers[@"error-code"];
            NSArray  *errstruct = [_error_code componentsSeparatedByString:@"."];
            NSString *error_level, *error_who, *error_what;
            
            @try {
                error_level = errstruct[0];
                error_who = errstruct[1];
                error_what = errstruct[2];
                
                if (_loglevel>=GI_STOMP_LOG_DEBUG)
                    NSLog(@"###... GIStompDispatcher:STOMP Error: %@", rp);
                
                if (
                    [error_who hasPrefix:@"gateway"]
                    ) {
                    [self processGatewayErrors:rp withErrorDomain:error_what];
                }
                else if (
                         [error_who hasPrefix:@"authenticator"]
                         ){
                    [self processGatewayErrors:rp withErrorDomain:error_what];
                }
                else{
                    [self performStompError:rp.headers[@"message"]];
                    return;
                }
            }
            @catch (NSException *exception) {
                [self performStompError:rp.headers[@"message"]];
            }
            
        }
        else if ([rp.name isEqualToString:@"CONNECTED"]) {
            //
            // set connection atributes, such as session id channel id
            //
            GIStompConnect *connectionStomp = [[GIStompConnect alloc] init];
            [connectionStomp messageReceived: rp];
            
            endConnectionTime = [NSDate timeIntervalSinceReferenceDate];
            
            _structure = connectionStomp.lastResponse.body.structure;
            
            if (_loglevel>=GI_STOMP_LOG_DEBUG){
                if (_loglevel>=GI_STOMP_LOG_TRACE){
                    NSLog(@"###... GIStompDispatcher:STOMP Connection: \n%@", [_structure description]);
                }
                if (_loglevel>=GI_STOMP_LOG_DEBUG) {
                    NSLog(@"###... GIStompDispatcher:STOMP Connection time: %f", (endConnectionTime-startConnectionTime));
                }
            }
            
            self.messages.structure = _structure;
            
            isStopConnectionOpened = YES;
            
            if (self.eventHandler) {
                dispatch_async(stomponCommandQueue, ^(void){
                    [self.eventHandler channelOpened];
                });
            }
            
            if (self.loginHandler) {
                dispatch_async(stomponCommandQueue, ^(void){
                    [self.loginHandler didStompLogin:@{@"login": config.login}];
                });
            }
            
            //
            // Send dispatched requests
            //
            [self.messages sendRequests];
            
            //
            // Send dispatched subscriptions
            //
            [self.messages sendSubscriptions];
        }
        else if( [rp.name isEqualToString:@"REPLY"]){
            //
            // append to channle new tickers
            //
            
            NSNumber *_requestID = [NSNumber numberWithInteger: [rp.headers[@"request-id"] integerValue]];
            
            GIRequestStompFrame *req=[self.messages getRequest:_requestID];
            
            req.isDispatched = YES;
            
            [req performSelector:@selector(hardSetStructure:) withObject:_structure];
            
            if (req) {
                @synchronized (req){
                    [req messageReceived:rp];
                    if (req.delegate) {
                        
                        if (_loglevel>=GI_STOMP_LOG_TRACE)
                            NSLog(@"###... GIStompDispatcher:STOMP Reply: %@", req);
                        
                        dispatch_async(stomponCommandQueue, ^(void){
                            [req.delegate didFrameReceive:req];
                            [self.messages removeRequest:_requestID];
                        });
                    }
                }
            }
        }
        else if([rp.name isEqualToString:@"MESSAGE"]){
            //
            // get subscription id
            //
            NSNumber        *_subscriptionID = [NSNumber numberWithInteger:[rp.headers[@"subscription"] integerValue]];
            
            //
            // get message if it is dispatched
            //
            GIStompMessage  *_mess = [self.messages getSubscription:_subscriptionID];
            _mess.isDispatched = YES;
            
            if (_mess) {
                //
                // set new responsed data
                //
                //[_mess performSelector:@selector(hardSetStructure:) withObject:_structure];
                
                @synchronized (_mess){
                    [_mess messageReceived:rp];
                    if (_mess.delegate) {
                        
                        if (_loglevel>=GI_STOMP_LOG_TRACE)
                            NSLog(@"###... GIStompDispatcher:STOMP Message: %@", _mess);
                        
                        dispatch_async(stomponCommandQueue, ^(void){
                            [_mess.delegate didFrameReceive:_mess];
                        });
                        
                    }
                }
            }
            else{
                [self unregisterStomp:rp];
            }
        }
    }
    @catch (NSException *exception) {
        if (_loglevel>=GI_STOMP_LOG_DEBUG) {
            NSLog(@"###... GIStompDispatcher:DISPATCHER Exception %@ %s:%i", exception, __FILE__, __LINE__);
        }
        [self performStompError:exception.description];
    }
}

- (void) onError:(GIError *)error in:(GITunnelCommand)command withData:(NSData *)data{
    
    GICommonStompFrame *stomp = [[GICommonStompFrame alloc] initWithData:data];
    
    if ([stomp.name isEqualToString:@"CONNECT"]) {
        NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
        if (_loglevel>=GI_STOMP_LOG_DEBUG) {
            NSLog(@"1. ###... GIStompDispatcher:OnError:command:withData: %@: %@ %s:%i\n\n", error, _buffer, __FILE__, __LINE__);
        }
        [self reconnectWithError:error inCommand:command];
        return;
    }
    else if ([stomp.name isEqual:@"UNSUBSCRIBE"]){
        NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
        if (_loglevel>=GI_STOMP_LOG_DEBUG) {
            NSLog(@"2. ###... GIStompDispatcher:OnError:command:withData: %@: %@ %s:%i\n\n", error, _buffer, __FILE__, __LINE__);
        }
        return;
    }

    
    NSString *receiptString = stomp.headers[@"receipt"];
    NSNumber *receiptID = [NSNumber numberWithInteger: [receiptString integerValue]];
    NSString *subscriptionString = stomp.headers[@"subscription"];
    NSNumber *subscriptionID = [NSNumber numberWithInteger: [subscriptionString integerValue]];
    
    if (subscriptionString==nil && receiptString == nil) {
        [self.errorHandler didStompError:error];
        NSString *_buffer = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSStringEncodingConversionAllowLossy];
        if (_loglevel>=GI_STOMP_LOG_DEBUG) {
            NSLog(@"3. ###... GIStompDispatcher:OnError:command:withData: %@: %@ %s:%i\n\n", error, _buffer, __FILE__, __LINE__);
        }
        return;
    }
    
    if (receiptString==nil)
        receiptID = subscriptionID;
    
    GIStompMessage *message = [self.messages.subscriptions objectForKey:receiptID];
    
    if (!message) {
        GIRequestStompFrame *request = [self.messages.requests objectForKey:receiptID];
        if (request && !request.isDispatched){
            if(request.delegate && [request.delegate respondsToSelector:@selector(didFrameError:withError:)]) {
                [request.delegate didFrameInterrupt:request withError:[[GIError alloc] initWithNSError:error]];
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(config.reconnectTimeout * NSEC_PER_SEC)), stomponCommandQueue, ^{
                if (!request.isDispatched) {
                    [request updateRequestID];
                    [self registerRequest:request];
                }
            });
        }
    }
    else{
        if (message && !message.isDispatched){
            if (message.delegate && [message.delegate respondsToSelector:@selector(didFrameError:withError:)]) {
                [message.delegate didFrameInterrupt:message withError:[[GIError alloc] initWithNSError:error]];
            }
            [self unregisterMessage:message];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(config.reconnectTimeout * NSEC_PER_SEC)), stomponCommandQueue, ^{
                if (!message.isDispatched) {
                    [message updateSubscriptionID];
                    [self registerMessage:message];
                }
            });
        }
    }
}

- (void) reconnectWithError:(GIError*) error inCommand:(GITunnelCommand)command {
    if (self.hasPermanentConnection){
        if (self.eventHandler){
            dispatch_async(stomponCommandQueue, ^(void){
                [self.eventHandler channelReconnecting];
            });
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(config.reconnectTimeout * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self disconnect];
            sleep((unsigned int)config.reconnectTimeout);
            [self connect];
        });
    }else if (self.eventHandler) {
        self.tunnel = nil;
        isStopConnectionOpened = NO;
        dispatch_async(stomponCommandQueue, ^(void){
            [self.errorHandler didSystemError:error];
        });
    }
}

- (void) onError:(GIError *)error in:(GITunnelCommand)command{
    void (^reconnect)(void) = ^(void) {
        [self reconnectWithError:error inCommand:command];
    };
    
    if (error.code == GICSP_ERROR_CERT_INVALID) {
        [self.tunnel close];
        if (self.eventHandler) {
            dispatch_async(stomponCommandQueue, ^(void){
                [self.errorHandler didSystemError:error];
            });
        }
    }
    else if (error.code == GICSP_ERROR_CONNECTION_FAIL) {
        //
        // Connection failed
        //
        
        reconnect();
    }
    
    else if (error.code == GICSP_ERROR_RECEIVER_CLOSED && command == GITunnel_CONNECT ){
        reconnect();
    }
        
    else if (error.code == GISP_ERROR_HTTP_STATUS_SID_EXPIRED  && command == GITunnel_CONNECT ) {
        reconnect();
    }    
    else if (command == GITunnel_RECEIVE) {
        if (self.tunnel.isClosed) {
            //
            // reciever closed and could not relaunch
            //
            reconnect();
        }
        else if (self.eventHandler) {
            //
            // reciever closed and realunched
            //
            dispatch_async(stomponCommandQueue, ^(void){
                [self.eventHandler channelReconnecting];
            });
        }
    }
    else if (
             error.code == GICSP_ERROR_RETRIES_LIMIT ||
             error.code == GICSP_ERROR_RECEIVER_CLOSED
             ) {
        [self.tunnel close];
        if (self.eventHandler) {
            dispatch_async(stomponCommandQueue, ^(void){
                [self.errorHandler didSystemError:error];
            });
        }
    }
    else if (command == GITunnel_PING){
        if (self.eventHandler && [self.eventHandler respondsToSelector:@selector(channelPingRoundTrip:)]) {
            dispatch_async(stomponCommandQueue, ^(void){
                [self.eventHandler channelPingRoundTrip:-1.0];
            });
        }
    }
    else {
        isStopConnectionOpened = NO;
        if ([error.domain hasSuffix:@"http.status"] && error.code==410) {
            //
            // lost connection
            //
            if (_loglevel>=GI_STOMP_LOG_DEBUG) 
                NSLog(@"### Connection closed by GW: %@ %s:%i", error, __FILE__, __LINE__);
        }
        else{
            if (self.errorHandler){
                [self disconnect];
                dispatch_async(stomponCommandQueue, ^(void){
                    [self.errorHandler didSystemError:error];
                });
            }
        }
    }
}

- (void) onConnect:(NSData *)data{
    if (self.eventHandler) {
        [self.eventHandler channelWillActive];
    }
}

- (void) onClose:(NSData *)data{
    if (self.eventHandler) {
        [self.eventHandler channelClosed];
    }
}

- (void) onOpen:(NSData *)data{
    if (self.eventHandler && [self.eventHandler respondsToSelector:@selector(channelConnectionRestored)]) {
        [self.eventHandler channelConnectionRestored];
    }
    if (!isStopConnectionOpened && stompConnection) {
        [stompConnection sendFrames];
    }
}

- (void) onPing:(NSData *)data roundTrip:(NSTimeInterval)roundTrip{
    if (self.eventHandler && [self.eventHandler respondsToSelector:@selector(channelPingRoundTrip:)]) {
        dispatch_async(stomponCommandQueue, ^(void){
            [self.eventHandler channelPingRoundTrip:roundTrip];
        });
    }
}

- (void) onSend:(NSData *)data{
    if (self.loglevel>=GI_STOMP_LOG_DEBUG) {
        NSLog(@"###... GIStompDispatcher:onSend: %@", [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSUTF8StringEncoding]);
    }
}


#pragma - utilites
- (BOOL) isDataCompleted:(NSData *)chainedData{
    if(!chainedData) return YES;
    if([chainedData length]==0) return YES;
    char _the_end_of_stopm;
    [chainedData getBytes:&_the_end_of_stopm range:NSMakeRange([chainedData length]-1, 1)];
    return _the_end_of_stopm == '\0';
}


@end

