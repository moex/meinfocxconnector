//
//  GIStompBody.h
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIConnectionStructure.h"
#import "GIOrderedDictionary.h"

/**
 *  Common value formater. It uses to represent any types that received from server. There are simple data shuch as numbers, time, string.
 */
@interface GIValueFormatter : NSObject

/**
 *  Global number formater.
 */
@property(readonly)  NSNumberFormatter *numberFormatter;

/**
 *  Create app sinleton formater.
 *  
 *  @return Value formater.
 */
+ (id) sharedObject;

/**
 *  Represent as a string.
 *  
 *  @param value value 
 *  
 *  @return formated string.
 */
- (NSString*) string: (id) value;

/**
 *  Represent as a number.
 *  
 *  @param value value.
 *  
 *  @return number.
 */
- (NSNumber*) number: (id) value;
@end

/**
 *  STOMP value as a part of received data.
 */
@interface GIStompValue : NSObject <NSCopying>

/**
 *  Init from an abstract object.
 *  
 *  @param object an object.
 *  
 *  @return the object.
 */
- (id) initFromId: (id) object;

/**
 *  Get system object.
 *  
 *  @return get the cocoa object.
 */
- (id) valueOfObject;

/**
 *  Represent as a string.
 *  
 *  @return fromated string.
 */
- (NSString*) string;

/**
 *  Get as a nymber.
 *  
 *  @return nsnumber.
 */
- (NSNumber*) number;

/**
 *  Boolean test.
 *  
 *  @return YES if it is nill object.
 */
- (BOOL) isNull;

/**
 *  Try to get as a float.
 *  
 *  @return float number if can.
 */
- (float) floatValue;

/**
 *  Get as a integer number.
 *  
 *  @return integer number if can.
 */
- (NSInteger) integerValue;

/**
 *  Get the number precision or -1 if this object is not number.
 *
 *  @return precision.
 */
- (NSInteger) precision;

@end

typedef enum __gistomp_property_types {
    
    GISTOMP_UNKNOWN  = 0,
    GISTOMP_SNAPSHOT,
    GISTOMP_UPDATES,
    GISTOMP_REMOVAL,    
    
} GIStompPropertyTypes;


/**
 *  Any server messages received as STOMP object which contains: headers, properties, columns and data.
 */
@interface GIStompBodyProperties : NSObject

/**
 * Response message type.
 */
@property(readonly) GIStompPropertyTypes type;

/**
 * Consiquenses number of response.
 */
@property UInt64 seqNum;

/**
 *  Last server update for the stomp frame. Optional.
 */
@property(readonly, nonatomic) NSDate *timestamp;

/**
 *  Any content object
 *
 */
@property(readonly, nonatomic) NSString *comment;

/**
 *  Create instance from dictionary prepared by JSON.
 *  
 *  @param propDict JSON description.
 *  
 *  @return parsed body properties.
 */
- (id) initFromDictionary:(NSDictionary*)propDict;

/**
 * Return data as an JSON object.
 */
- (NSDictionary*) JSONObject;

@end


/**
 * Any server messages received as STOMP object which contains: headers, properties, columns and data. This is part contains data.
 */
@interface GIStompBody : NSObject <NSCopying>

/**
 *  Parse and create instance from STOMP buffered Frame.
 *  
 *  @param aData raw data contains stomp object.
 *  
 *  @return parsed body.
 */
- (id) initFromData:(NSData*)aData;

/**
 * STOMP body properties
 */
@property(readonly) GIStompBodyProperties *properties;

/**
 * Reference to connection structure if it has.
 */
@property(readonly) GIConnectionStructure *structure;

/** 
 * Any STOMP object which server response can contains columns which quiet match body data.
 */
@property(readonly) NSArray        *columns;

/**
 *  Body data.
 */
@property(readonly) NSArray        *data;

/**
 *  Set raw data/
 *  
 *  @param aData raw data.
 */
- (void) setData:(NSArray *)aData;

/**
 *  Convert JSON data to raw buffer.
 *  
 *  @return raw data.
 */
- (NSData*) dataWithJSONObject;

/**
 *  Return object from row by column name. Returnetd object is converted to right formating form.
 *  
 *  @param columnName column name where cell of data saved.
 *  @param key        a column name which should be compared with value.
 *  @param keyValue   a value which would match with value is in the cell of response data.
 *
 *  @return searched object or nil.
 *
 *   Example:
 *   We received the follow body:
 *   columns: [NAME, PRICE]
 *   data: [['ONE', 100],['TWO':1000]]
 *
 *   We want to find what is in row with PRICE == 100 for column NAME. In this case there is the first row return the value 'ONE' for column 'NAME'.
 *
 */
- (GIStompValue*) valueForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue;

/**
 *  Return formated string for column whete key is equal some value. It equal the -(GIStompValue*)valueForColumn:whereKey:isEqualToString:, but returns string.
 *  
 *  @param columnName column name where cell of data saved.
 *  @param key        a column name which should be compared with value.
 *  @param keyValue   a value which would match with value is in the cell of response data.
 *  @see -(GIStompValue*)valueForColumn:whereKey:isEqualToString:
 *  
 *  @return searched object or nil.
 */
- (NSString*) stringForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue;

/**
 *  Retrun number.
 *  
 *  @param columnName column name where cell of data saved.
 *  @param key        a column name which should be compared with value.
 *  @param keyValue   a value which would match with value is in the cell of response data.
 *  @see -(GIStompValue*)valueForColumn:whereKey:isEqualToString:
 *  
 *  @return searched object or nil.
 */
- (NSNumber*) numberForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue;

/**
 *  Return value at concrete index.
 *  
 *  @param row        row index,
 *  @param columnName column name where value is saved.
 *  
 *  @return searched object or nil.
 */
- (GIStompValue*) valueForIndex:(NSInteger)row andKey:(NSString*)columnName;

/**
 *  Return number for index.
 *  
 *  @param row        row index,
 *  @param columnName column name where value is saved.
 *  
 *  @return searched object or nil.
 */
- (NSNumber*) numberForIndex:(NSInteger)row andKey: (NSString*)columnName;

/**
 *  Get formated string.
 *  
 *  @param row        row index,
 *  @param columnName column name where value is saved.
 *  
 *  @return searched object or nil.
 */
- (NSString*) stringForIndex:(NSInteger)row andKey: (NSString*)columnName;

/**
 *  Fast object access.
 *  
 *  @param row        row index,
 *  @param columnName column name where value is saved.
 *  
 *  @return searched object or nil.
 */
- (id) objectForIndex:(NSInteger)row andKey: (NSString*)columnName;

/**
 *  Get a complete data row by index.
 *  
 *  @param indexRow row index.
 *  
 *  @return row dictionary.
 */
- (NSDictionary*) rowByIndex:(NSInteger)indexRow;

/**
 *  Get a complete data row where columnName value equal columnValue.
 *  
 *  @param columnValue value.
 *  @param columnName  column name where data equals value.
 *  
 *  @return row dictionary.
 */
- (NSDictionary*) rowByKeyValue:(NSString*)columnValue forColumn:(NSString*)columnName;

/**
 *  Size of data. Columns.
 *  
 *  @return columns number.
 */
- (NSUInteger) countOfColumns;

/**
 *  Size of data. Rows.
 *  
 *  @return rows number.
 */
- (NSUInteger) countOfRows;

/**
 *  Find max value for column.
 *  
 *  @param columnName column name.
 *  
 *  @return maximum value if column is number type.
 */
- (GIStompValue*) maxValueForColumn: (NSString *)columnName;

/**
 *  Find min value for column.
 *  
 *  @param columnName column name.
 *  
 *  @return minimum value if column is number type.
 */
- (GIStompValue*) minValueForColumn: (NSString *)columnName;

- (void) hardSetStructure:(GIConnectionStructure *)structure;
@end
