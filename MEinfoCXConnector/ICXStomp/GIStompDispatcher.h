//
//  GIStompDispatcher.h
//  GIConnector
//
//  Created by denn on 3/25/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIConfig.h"
#import "GIStompRequests.h"
#import "GIStompSubscriptions.h"
#import "GICSPTunnel.h"

typedef enum {
    GI_STOMP_LOG_INFO=1,
    GI_STOMP_LOG_WARN=100,
    GI_STOMP_LOG_DEBUG=1000,
    GI_STOMP_LOG_TRACE=10000
} GIStompDispatcherLogLevel;

typedef enum { 
    GI_STOMP_LOGIN_FAILED = 80000,
    GI_STOMP_ERROR
} GIStompDispatcherError;


#define GI_STOMP_FRAME_ERROR(stomp_frame) \
    [[GIError alloc] initWithNSError:[NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX] \
                               code: GI_STOMP_ERROR \
                           userInfo: @{ \
   NSLocalizedFailureReasonErrorKey: GILocalizedString(@"Stomp error", @"NSLocalizedFailureReasonErrorKey"), \
          NSLocalizedDescriptionKey: [NSString  stringWithFormat: GILocalizedString(@"%@: %@", @"Stomp error"), stomp_frame.headers[@"error-code"], stomp_frame.headers[@"message"]] \
    }]]


#define GI_STOMP_STRING_ERROR(error_text) \
    [[GIError alloc] initWithNSError:[NSError errorWithDomain: [NSString stringWithFormat:@"%@.stomp", GI_ERROR_DOMAIN_PREFIX] \
                                code: GI_STOMP_ERROR \
                            userInfo: @{ \
    NSLocalizedFailureReasonErrorKey: GILocalizedString(@"Stomp error", @"NSLocalizedFailureReasonErrorKey"), \
           NSLocalizedDescriptionKey: [NSString  stringWithFormat: GILocalizedString(@"%@", @"Stomp text error"), error_text] \
    }]]


/**
 *  Handle STOMP dispatcher errors protocol.
 */
@protocol GIStompErrorDispatchHandler <NSObject>

/**
 *  Stomp error occured.
 *  
 *  @param error error description.
 */
- (void) didStompError: (GIError*)error;

/**
 *  System error occured.
 *  
 *  @param error error description.
 */
- (void) didSystemError: (GIError*)error;

@end

/**
 *  Handle STOMP login enevnts protocol.
 */
@protocol GIStompLoginDispatchHandler <NSObject>

/**
 *  Stomp login has done correctely.
 *  
 *  @param connection connection description.
 */
- (void) didStompLogin: (NSDictionary*)connection;

/**
 *  Login failed.
 *  
 *  @param error    error description.
 *  @param response server response message 
 */
- (void) didStompLoginError: (GIError*)error withConnectionResponse: (GICommonStompFrame*)response;

@end

/**
 *  Handle STOMP dispatcher events
 *  
 */
@protocol GIStompEventDispatchHandler <NSObject>

/**
 *  Connection in progress, but not established
 */
- (void) channelWillActive;

/**
 *  Phisical connection established.
 */
- (void) channelOpened;

/**
 *  Phisical connection closed.
 */
- (void) channelClosed;

/**
 *  Connection is trying to make reconnection.
 */
- (void) channelReconnecting;

/**
 *  Connection established or reconected.
 */
- (void) channelConnectionRestored;

@optional

- (void) dispatcherWillStop;
- (void) dispatcherDidStop;

/**
 *  Ping trip responsed.
 *  
 *  @param roundtrip  ping response time.
 */
- (void) channelPingRoundTrip:(NSTimeInterval) roundtrip;

@end


/**
 * STOMP Dispatcher manages stomp channels and handle subscribed messages. This is the core STOMP frame processing point.
 * It creates connection, handles request and messages. 
 */
@interface GIStompDispatcher : NSObject <GITunnelHandler>

/**
 *  Log level flag uses while dubugging.
 */
@property GIStompDispatcherLogLevel loglevel;

/**
 *  Connection structure which explains streams and request permitions for the connected user.
 */
@property(readonly, nonatomic) GIConnectionStructure *structure;

/**
 *  Create dispatcher.
 *  
 *  @param config config options.
 *  
 *  @return dispatcher instance.
 */
- (id)   initWithConfig: (GIConfig*)config;

//
// Create instance
//
/**
 *  Create dispatcher and connect immediately if it needs.
 *  
 *  @param config             config options.
 *  @param connectImmediately YES if dispatcher should start immediately after creation.
 *  
 *  @return dispatcher instance.
 */
- (id)   initWithConfig: (GIConfig*)config andConnectImmediately: (BOOL) connectImmediately;

/**
 *  Connect to server.
 */
- (void) connect;

/**
 *  Discronnect.
 */
- (void) disconnect;

/**
 *  Register yet another request in the dispatcher.
 *  
 *  @param request STOMP Request.
 */
- (void) registerRequest:(GIRequestStompFrame*)request;

/**
 * Register message to dispatch. Every registered message response property will be updated when data will receive.
 * If connection did not established yet create it and send subscribtion.
 *  
 *  @param message STOMP Subscription.
 */
- (void) registerMessage:(GIStompMessage*)message;

/**
 *  Unregister (UNSUBSCRIBE) message.
 *  
 *  @param message early registered message.
 */
- (void) unregisterMessage:(GIStompMessage*)message;
- (void) unregisterMessage:(GIStompMessage*)message immediately:(BOOL)immediately;

/**
 *  Is the dispatcher runing? It means that the dispatcher has phisical tunnel to server and dispatch messages.
 *  
 *  @return YES if it is runing.
 */
@property (atomic, readonly) BOOL isRunning;

/**
 *  Delegate errors handler.
 */
@property id <GIStompErrorDispatchHandler> errorHandler;

/**
 *  Delegate events handler.
 */
@property id <GIStompEventDispatchHandler> eventHandler;

/**
 *  Delegate login handler.
 */
@property id <GIStompLoginDispatchHandler> loginHandler;


/**
 * Infinit phisical connection loop.
 * If property is YES - transport protocol try to reconnect always, else raise error to main process.
 */
@property BOOL hasPermanentConnection;

/**
 * Time to call deferred unsubscribtion.
 */
@property NSTimeInterval deferredTime;
@end
