//
//  GIStompBody.m
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIStompBody.h"
#import "GIUtils.h"

static GIValueFormatter *_shared_value_formater=nil;

@interface GIValueFormatter()
@property NSNumberFormatter *numberFormatter;
@end

@implementation GIValueFormatter

+ (id) sharedObject{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared_value_formater = [[GIValueFormatter alloc] init];
    });
    
    return _shared_value_formater;
}

- (id) init{
    @synchronized (self){
        if (_shared_value_formater) {
            self = _shared_value_formater;
            return self;
        }
        
        self = [super init];
        
        if (self) {
            if (!_numberFormatter) {
                
                _numberFormatter = [[NSNumberFormatter alloc] init];
                
                [_numberFormatter allowsFloats];
                [_numberFormatter setLocale:[NSLocale currentLocale]];
            }
            
            [_numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [_numberFormatter setAlwaysShowsDecimalSeparator:NO];
            //[_numberFormatter setMinimumSignificantDigits:0];
        }
        return self;
    }
}

- (NSString*) string: (id) aRet{
    @synchronized (self){
        if(!aRet) return @"-";
        
        id ret = aRet;
        
        if ([aRet isKindOfClass:[GIStompValue class]]) {
            ret = [aRet valueOfObject];
            if (IsEmpty(ret)) {
                return @"-";
            }
        }
        
        if ([ret isKindOfClass:[NSArray class]]) {
            long prec = 2;
            if ([ret count]==2) {
                prec = [(NSNumber*)[ret objectAtIndex:1] integerValue];
            }
            //[_numberFormatter setMaximumSignificantDigits:prec];
            [_numberFormatter setMaximumFractionDigits:prec];
            [_numberFormatter setMinimumFractionDigits:prec];
            
            return [_numberFormatter stringFromNumber:ret[0]];
        }
        
        else if ([ret isKindOfClass:[NSString class]])
            return [ret length]==0?@"-":ret;
        
        else if ( [ret isKindOfClass:[NSNumber class]]){
            NSNumber *_numret = (NSNumber*) ret;
            return  [_numret descriptionWithLocale:[NSLocale currentLocale]] ;
        }
        return [_numberFormatter stringForObjectValue:ret];
    }
}

- (NSNumber *) number:(id)aValue{
    @synchronized (self){
        
        if(!aValue) return nil;
        
        id value = aValue;
        if ([aValue isKindOfClass:[GIStompValue class]]) {
            value = [aValue valueOfObject];
        }
        
        if ([value isKindOfClass:[NSArray class]]) {
            
            id num = value[0];
            
            if ([num isKindOfClass:[NSString class]]) {
                num = [_numberFormatter numberFromString:num];
            }
            return num;
        }
        else if ([value isKindOfClass:[NSString class]]){
            return [_numberFormatter numberFromString:value];
        }
        else if ([value isKindOfClass:[NSNumber class]])
            return value;
        else
            return nil;
    }
}

@end


@implementation GIStompValue
{
    id valueOfObject;
}

- (BOOL) isNull{
    return IsEmpty(valueOfObject);
}

- (id) copyWithZone:(NSZone *)zone{
    GIStompValue *_ret = [[GIStompValue allocWithZone:zone] init];
    _ret->valueOfObject = [valueOfObject copy];
    return _ret;
}

- (id) initFromId: (id) object{
    self = [super init];
    valueOfObject=object;
    return self;
}

- (NSInteger) precision{
    if ([valueOfObject isKindOfClass:[NSArray class]]) {
        if ([valueOfObject count]==2) {
            return [(NSNumber*)[valueOfObject objectAtIndex:1] integerValue];
        }
    }else if ([valueOfObject isKindOfClass:[NSNumber class]]){
        return 6;
    }
    return -1;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@",valueOfObject];
}

- (id) valueOfObject{
    return valueOfObject;
}

- (NSString*) string{
    id v = [[GIValueFormatter sharedObject] string: valueOfObject];
    return IsEmpty(v)?@"-":v;
}

- (NSNumber*) number{
    id v = [[GIValueFormatter sharedObject] number: valueOfObject];
    return [v isKindOfClass:[NSNumber class]]?v:[NSNumber numberWithFloat:0.0];
}

- (float) floatValue {
    id v = [[GIValueFormatter sharedObject] number: valueOfObject];
    return [v isKindOfClass:[NSNumber class]]?[v floatValue]:0.0;
}

- (NSInteger)  integerValue{
    id v = [[GIValueFormatter sharedObject] number: valueOfObject];
    return [v isKindOfClass:[NSNumber class]]?[v integerValue]:0;
}

@end

@interface GIStompBodyProperties ()
@property GIStompPropertyTypes type;
@property(nonatomic,strong) NSDate *timestamp;
@property(nonatomic,strong) NSString *comment;
@end

static NSString *_types_string[] = { [GISTOMP_REMOVAL]=@"removal", [GISTOMP_SNAPSHOT]=@"snapshot", [GISTOMP_UPDATES]=@"updates"};

//
// PROPERTIES
//
@implementation GIStompBodyProperties

@synthesize type, seqNum;


- (id) initFromDictionary:(NSDictionary *)propDict{
    if(!(self=[super init]))
        return nil;
    
    self.seqNum = 0;
    
    NSString *_type = [propDict[@"type"] lowercaseString];
    
    if ([_type isEqualToString:@"snapshot"]) {
        self.type = GISTOMP_SNAPSHOT;
    }
    else if ([_type isEqualToString:@"updates"]){
        self.type = GISTOMP_UPDATES;
    }
    else if ([_type isEqualToString:@"removal"]){
        self.type = GISTOMP_REMOVAL;
    }
    else {
        self.type = GISTOMP_UNKNOWN;
    }
    
    self.comment = propDict[@"comment"];
    
    id sq = propDict[@"seqnum"];
    if (sq && ![sq isNull] && [sq isKindOfClass:[NSString class]]){
        self.seqNum = [sq integerValue];
    }
    else
        self.seqNum = 0;
    
    //    static NSDateFormatter* dateFormatter = nil;
    //    if (!dateFormatter) {
    //        dateFormatter = [[NSDateFormatter alloc] init] ;
    //        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS Z";
    //    }
    
    NSString *tms = propDict[@"timestamp"];
    NSTimeInterval timestamp = 0.0;
    if (tms) {
        timestamp =((NSTimeInterval)([tms longLongValue]))/1000.0f;
        if (timestamp)
            self.timestamp = [NSDate dateWithTimeIntervalSince1970:timestamp];
    }
    else
        self.timestamp = [NSDate date];
    
    return self;
}

- (NSDictionary*) JSONObject{
    return [[NSDictionary alloc]
            initWithObjectsAndKeys:
            _types_string[self.type], @"type",
            [NSNumber numberWithUnsignedLongLong: self.seqNum], @"seqnum",
            nil
            ];
}


- (NSString*) description{
    static NSDateFormatter* dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init] ;
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.SSS Z";
    }
    return [NSString stringWithFormat:@"{type: '%@', seqnum: %llu timestamp: %@ comment: %@}", _types_string[self.type], self.seqNum, [dateFormatter stringFromDate:self.timestamp], self.comment==nil?@"":self.comment];
}
@end


//
// BODY
//
@interface GIStompBody ()
@property NSArray        *columns;
@property NSArray *data;
@property GIStompBodyProperties *properties;
@property GIConnectionStructure *structure;
@end

@implementation GIStompBody
{
    //
    // We received data but it does not mean that we should process it immediately
    // Data well be processed on demand
    //
    NSData *_rawData;
    
    // indexed access to data position at columns
    NSMutableDictionary *columnsIndex;
    
    NSNumberFormatter    *numberFormatter;
    NSMutableDictionary  *maxTable;
    NSMutableDictionary  *minTable;
    
    NSMutableDictionary  *ratios;
}

@synthesize data=_data, columns=_columns, properties=_properties, structure=_structure;

- (id) copyWithZone:(NSZone *)zone{
    GIStompBody *_new_obj = [[[self class] allocWithZone:zone] init];
    
    if (_new_obj) {
        [self renderOnDemand];
        _new_obj->_columns = [self->_columns copy];
        _new_obj->_properties = self->_properties ;
        _new_obj->_rawData = nil;
        _new_obj->columnsIndex = [self->columnsIndex copy];
        _new_obj->maxTable = self->maxTable;
        _new_obj->maxTable = self->minTable;
        _new_obj->ratios = [self->ratios copy];
        _new_obj->numberFormatter = [self->numberFormatter copy];
        _new_obj->_data = [self->_data copy];
        _new_obj->_structure = [self->_structure copy];
    }
    
    return _new_obj;
}

- (id) initFromData:(NSData *)aData{
    
    if(!(self=[super self]))
        return self;
    
    //
    // parse on demand
    //
    _rawData = aData;
    
    // clear
    minTable = maxTable = nil;
    
    return self;
}

- (NSString*) description{
    NSMutableString *data = [[NSMutableString alloc] init];
    [data appendString:@" [\n"];
    if (!IsEmpty(self.data)){
        for (NSArray *d in self.data) {
            [data appendString:@"   [\n"];
            for (int i=0; i<self.columns.count; i++) {
                [data appendFormat:@"       %@ = %@,\n",self.columns[i], d[i]];
            }
            [data appendString:@"   ],\n"];
        }
    }
    NSMutableString *columns = [[NSMutableString alloc] init];
    [columns appendString:@" ["];
    if (!IsEmpty(self.columns)){
        for (int i=0; i<self.columns.count; i++) {
            [columns appendString:self.columns[i]];
            if (i<self.columns.count-1) {
                [columns appendString:@", "];
            }
        }
    }
    [columns appendString:@"]"];
    [data appendString:@" ]"];
    return [NSString stringWithFormat:@"{\n properties: %@,\n columns: %@,\n data:\n%@\n}", self.properties, columns, data];
}

- (NSUInteger) countOfColumns{
    if (IsEmpty(self.columns)){
        return 0;
    }
    return self.columns.count;
}

- (NSUInteger) countOfRows{
    if (IsEmpty(self.data)) {
        return 0;
    }
    return self.data.count;
}

- (NSData*) dataWithJSONObject{
    //@autoreleasepool {
    @try {
        if (self.data){
            NSError *_error;
            
            NSMutableDictionary *_dict = [[NSMutableDictionary alloc] init];
            
            [_dict setObject:[self.properties JSONObject] forKey:@"properties"];
            [_dict setObject:self.columns forKey:@"columns"];
            [_dict setObject:self.data forKey:@"data"];
            
            NSData  *_json = [NSJSONSerialization dataWithJSONObject: _dict options: 0 /*NSJSONWritingPrettyPrinted*/ error: &_error];
            
            return _json;
        }
        else
            return nil;
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        return nil;
    }
    //}
}

- (void) setColumns:(NSArray *)columns{
    _columns = columns;
}

- (void) setProperties:(GIStompBodyProperties *)properties{
    _properties = properties;
}

- (GIStompBodyProperties*) properties{
    @synchronized (self){
        [self renderOnDemand];
        return _properties;
    }
}

- (GIConnectionStructure*) structure{
    @synchronized (self){
        [self renderOnDemand];
        return _structure;
    }
}

- (void) setStructure:(GIConnectionStructure *)structure{
    _structure = structure;
}

- (void) hardSetStructure:(GIConnectionStructure *)structure{
    _structure = structure;
}

- (NSArray*) columns{
    @synchronized (self){
        [self renderOnDemand];
        return _columns;
    }
}


- (void) setData:(NSArray *)aData{
    @synchronized (self){
        _data = aData.mutableCopy;
        maxTable = minTable = nil;
    }
}

- (NSArray*) data {
    @synchronized (self){
        [self renderOnDemand];
        return _data;
    }
}

- (void) renderOnDemand{
    NSError *_error;
    
    if(_rawData){
        @autoreleasepool {
            
            NSMutableDictionary *_dict = [NSJSONSerialization JSONObjectWithData:_rawData options: NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments error:&_error];
            
            if (_dict == nil && _error) {
                NSLog(@"%@ %s:%i", _error, __FILE__, __LINE__);
                NSLog(@"Assembled: %@", [_rawData subdataWithRange:NSMakeRange(_rawData.length-4, 4)]);
                NSLog(@"Assembled: %@", [[NSString alloc] initWithBytes:[_rawData bytes] length: [_rawData length] encoding: NSUTF8StringEncoding]);
            }
            else{
                _data       = _dict[@"data"];
                _columns    = _dict[@"columns"];
                _properties = [[GIStompBodyProperties alloc] initFromDictionary:_dict[@"properties"]];
                
                id _json_structure = _dict[@"structure"];
                
                if (_json_structure) {
                    _structure = [[GIConnectionStructure alloc] initWithJson:_json_structure];
                }
                
                if(_columns){
                    
                    if (!columnsIndex)
                        columnsIndex = [NSMutableDictionary dictionaryWithCapacity:[_columns count]];
                    else
                        [columnsIndex removeAllObjects];
                    
                    NSUInteger index=0;
                    for (NSString *column in _columns) {
                        [columnsIndex setValue:[NSNumber numberWithUnsignedInteger:index] forKey:column]; ++index;
                    }
                }
                maxTable = minTable = nil;
            }
            _rawData = nil;
        }
    }
}

- (NSDictionary*) rowByIndex:(NSInteger)indexRow{
    @synchronized (self){
        @try {
            //[self renderOnDemand];
            
            if (IsEmpty(self.data)) {return nil;}
            
            if ([self.data count]==0) {
                return nil;
            }
            NSMutableDictionary * _rowd = [NSMutableDictionary dictionaryWithCapacity:[self.data count]];
            for (NSInteger i=0; i<[self.columns count];i++) {
                NSString *_cn = self.columns[i];
                [_rowd setValue:[self stringForIndex:indexRow andColumn:i] forKey:_cn];
            }
            return _rowd;
        }
        @catch (NSException *exception) {
            return nil;
        }
    }
}


- (NSInteger) indexWhereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    @synchronized (self){
        //[self renderOnDemand];
        
        if (IsEmpty(self.data)) {return 0;}
        
        NSNumber *_keyColumnIndex = columnsIndex[key] ;
        //
        // ! this version uses slow search,  in a future version we should use index to search key position
        //
        for (NSInteger i=0; i<[self.data count] ;i++) {
            NSString *keyObj = [self.data[i] objectAtIndex:_keyColumnIndex.integerValue];
            if ([keyObj isEqualToString:keyValue]) {
                return i;
            }
            i++;
        }
        return -1;
    }
}

- (GIStompValue*) valueForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    @synchronized (self){
        
        @try {
            [self renderOnDemand];
            NSNumber *_ci = columnsIndex[columnName];
            if (IsEmpty(_ci))
                return nil;
            return [self valueForIndex:[self indexWhereKey:key isEqualToString:keyValue]
                             andColumn:_ci.integerValue];
        }
        @catch (NSException *exception) {
            return nil;
        }
        return nil;
    }
}

- (NSString*) stringForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    @synchronized (self){
        
        @try {
            [self renderOnDemand];
            NSNumber *_ci = columnsIndex[columnName];
            
            if (IsEmpty(_ci))
                return nil;
            
            NSInteger _i=[self indexWhereKey:key isEqualToString:keyValue];
            NSString *_ret = [self stringForIndex: _i
                                        andColumn:_ci.integerValue];
            return _ret;
        }
        @catch (NSException *exception) {
            return nil;
        }
        return nil;
    }
}

- (NSNumber*) numberForColumn:(NSString *)columnName whereKey:(NSString*)key isEqualToString:(NSString*)keyValue{
    @synchronized (self){
        
        @try {
            [self renderOnDemand];
            NSNumber *_ci = columnsIndex[columnName];
            if (IsEmpty(_ci))
                return nil;
            return [self numberForIndex:[self indexWhereKey:key
                                            isEqualToString:keyValue]
                              andColumn:_ci.integerValue];
        }
        @catch (NSException *exception) {
            return nil;
        }
        return nil;
    }
}


- (NSDictionary*) rowByKeyValue:(NSString *)columnValue forColumn:(NSString *)columnName{
    @synchronized (self){
        
        @try {
            //[self renderOnDemand];
            
            if (IsEmpty(self.data)) {return nil;}
            
            NSNumber *_columnIndex = columnsIndex[columnName] ;
            
            if (IsEmpty(_columnIndex))
                return nil;
            
            //
            // ! this version uses slow search,  in a future version we should use index to search key position
            //
            [self renderOnDemand];
            for (NSInteger i=0; i<[self.data count] ;i++) {
                NSString *key = [ [self.data objectAtIndex:i]  objectAtIndex:[_columnIndex integerValue]];
                if ([key isEqualToString:columnValue]) {
                    return [self rowByIndex:i];
                }
                i++;
            }
        }
        @catch (NSException *exception) {
            return nil;
        }
        return nil;
    }
}

- (GIStompValue*) valueForIndex:(NSInteger)row andKey: (NSString*)columnName{
    @synchronized (self){
        
        [self renderOnDemand];
        NSNumber *_columnIndex = columnsIndex[columnName] ;
        if (IsEmpty(_columnIndex))
            return nil;
        return [self valueForIndex:row andColumn:_columnIndex.integerValue];
    }
}

- (id) valueForIndex:(NSInteger)row andColumn: (NSInteger)column{
    @try {
        if (IsEmpty(self.data)) {return nil;}
        
        return  [[GIStompValue alloc] initFromId: [[self.data objectAtIndex:row] objectAtIndex: column]];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (NSString*) stringForIndex:(NSInteger)row andColumn:(NSInteger)column{
    id ret = [[self valueForIndex:row andColumn:column] valueOfObject];
    GIValueFormatter *_formater = [GIValueFormatter sharedObject];
    return [_formater string:ret];
}

- (id) objectForIndex:(NSInteger)row andKey: (NSString*)columnName{
    @synchronized (self){
        //[self renderOnDemand];
        
        if (IsEmpty(self.data)) {return nil;}
        
        NSNumber *_columnIndex = columnsIndex[columnName];
        if (IsEmpty(_columnIndex))
            return nil;
        return [[self.data objectAtIndex:row] objectAtIndex: _columnIndex.intValue];
    }
}


- (NSString*) stringForIndex:(NSInteger)row andKey:(NSString *)columnName{
    @synchronized (self){
        
        [self renderOnDemand];
        NSNumber *_columnIndex = columnsIndex[columnName];
        if (IsEmpty(_columnIndex))
            return nil;
        return [self stringForIndex:row andColumn:_columnIndex.integerValue];
    }
}

- (NSNumber*) numberForIndex:(NSInteger)row andColumn:(NSInteger)columnIndex{
    @synchronized (self){
        
        id ret = [[self valueForIndex:row andColumn:columnIndex] valueOfObject];
        
        if(!ret) return nil;
        
        if ([ret isKindOfClass:[NSArray class]]) {
            id num = ret[0];
            if ([num isKindOfClass:[NSString class]]) {
                num = [numberFormatter numberFromString:num];
            }
            return num;
        }
        else if ([ret isKindOfClass:[NSString class]]){
            NSNumber *value = [numberFormatter numberFromString:ret];
            if (value == nil ){
                // not localized
                NSString *s = ret;
                double    v = [s doubleValue];
                value = [NSNumber numberWithDouble:v];
            }
            return value;
        }
        else if ([ret isKindOfClass:[NSNumber class]])
            return ret;
        else
            return nil;
    }
}

- (NSNumber*) numberForIndex:(NSInteger)row andKey:(NSString *)columnName{
    @synchronized (self){
        
        [self renderOnDemand];
        
        NSNumber *_columnIndex = columnsIndex[columnName];
        if (IsEmpty(_columnIndex))
            return nil;
        return [self numberForIndex:row andColumn:_columnIndex.integerValue];
    }
}


- (GIStompValue*) compareData: (NSString*)columnName withOrder:(NSComparisonResult)compartion forTable: (NSMutableDictionary* __strong *) table{
    
    // check table
    if (!*table)
        *table = [NSMutableDictionary dictionaryWithCapacity:0];
    
    // find value
    id _value = [*table objectForKey:columnName];
    
    if (_value)
        // ok
        return _value;
    
    // not found
    
    if (_rawData) [self renderOnDemand];
    
    for (NSUInteger i=0; i < [_data count]; i++){
        id _next_value = [self numberForIndex:i andKey:columnName];
        if (!_value)
            _value = _next_value;
        else if ([_next_value compare:_value] == compartion)
            _value = _next_value;
    }
    // new value
    
    [*table setValue:_value forKey:columnName];
    
    return [[GIStompValue alloc] initFromId: _value];
}

- (GIStompValue*) maxValueForColumn: (NSString *)columnName{
    @synchronized (self){
        return [self compareData:columnName withOrder:NSOrderedDescending forTable:&maxTable];
    }
}

- (GIStompValue*) minValueForColumn: (NSString *)columnName{
    @synchronized (self){
        return [self compareData:columnName withOrder:NSOrderedAscending forTable:&minTable];
    }
}


@end
