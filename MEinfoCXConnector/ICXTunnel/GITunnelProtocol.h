//
//  GITunnelProtocol.h
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 01/04/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GITunnelHandler.h"


/**
 * Abstract infoCX tunnel API.
 */

@protocol GITunnelProtocol <NSObject>

/** Connection delagate which handles data to next-level abstraction of an application uses Tunnel instance.
 */
@property(nonatomic,strong) id <GITunnelHandler> connectionHandler;

/** Session is opened, receiver is opened.
 * @return YES if channel is opened.
 */
@property (nonatomic,readonly) BOOL isOpened;

/**
 *  Session is closed, receiver is closed.
 *
 *  @return YES if channel is closed.
 */
@property (nonatomic,readonly) BOOL isClosed;

/**
 *   Start connection. Make backgrounded inifinit loop dispatch all communication process.
 */
- (void) open;

/**  Close receiver connection.
 */
- (void) close;

/**
 *  Send buffer to server.
 *
 *  @param data send buffer
 */
- (void) send: (NSData*)data;

/**
 *  Send string.
 *
 *  @param buffer buffered string.
 */
- (void) sendString: (NSString*)buffer;
@end
