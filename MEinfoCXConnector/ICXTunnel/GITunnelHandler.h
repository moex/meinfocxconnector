//
//  GITunnelHandler.h
//  GIConnector
//
//  Created by denn on 3/8/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIError.h"

/**
 *  Type if CSP command.
 */
typedef enum  {
    GITunnel_UNKNOWN    = -1,
    GITunnel_CONNECT    = 0,
    GITunnel_DISCONNECT = 1,
    GITunnel_PING       = 2,
    GITunnel_RECEIVE    = 3,
    GITunnel_SEND       = 4
} GITunnelCommand;


/** CSP interface to stream data connector.
    When some data, error, or event occurres we pass control to appropriated method.
 */
@protocol GITunnelHandler <NSObject>

/**
 *  Eval method when connection is opening.
 *  
 *  @param data Handle when data is received.
 */
- (void) onConnect: (NSData*)  data;

/**
 *  Eval method when receiver is opening.
 *  
 *  @param data what data received while channel is opened.
 */
- (void) onOpen: (NSData*)  data;

/**
 *  Eval when connection has been closed by timeout, closed by serevr or connection state has been changed.
 *  
 *  @param data what data receieved while channel is closed. 
 */
- (void) onClose: (NSData*)  data;

/**
 *  When data has been recieved we pass control to onRead method.
 *  
 *  @param data the main data which pushed by server to client.
 */
- (void) onRead: (NSData*)  data;

/**
 *  When we send data we can control what we replaied from server.
 *  
 *  @param data something returned from server.
 */
- (void) onSend: (NSData*)  data;

/**
 *  When we maked ping-pong and data has been recieved as result ping command we pass control to onPing method.
 *  
 *  @param data      some data which server can response on ping from client.
 *  @param roundTrip time to roundtrip interval in seconds.
 */
- (void) onPing: (NSData*)data roundTrip:(NSTimeInterval)roundTrip;

/**
 *   If some exception hapens error should catch the event.
 *  
 *  @param error   error description.
 *  @param command which command (connect/disconnect/send/read/...) lead to error. 
 *  @see GICSPCommand enum.
 */
- (void) onError: (GIError*)error in:(GITunnelCommand)command;
- (void) onError: (GIError*)error in:(GITunnelCommand)command withData:(NSData*)data;


/**
 *   We want to have a "gear" that can tell the stream processed task to finish reading
 *   and after that we can decide to pass completely loaded data to main process.
 *  It returns true when we decide data is compited and we expect call onRead callback
 *  
 *  @param chainedData part of completed data that can receive from server.
 *  
 *  @return YES if data is CSP-packet completed.
 */
- (BOOL) isDataCompleted: (NSData*)chainedData;

@end
