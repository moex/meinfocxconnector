//
//  GICSPCommandExecutor.m
//  GIConnector
//
//  Created by denn on 3/13/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIConfig.h"
#import "GICSPCommandExecutor.h"
#import "GICSPStream.h"

@interface GICSPCommandExecutor () <NSURLConnectionDataDelegate, GICSPStreamProtocol>

@property NSString *command;
@property NSData   *postData;

@end

@implementation GICSPCommandExecutor
{
    //
    // absolute URL
    //
    NSURL               *commandUrl;
    
    //
    // prepared http request
    //
    NSMutableURLRequest *request;
    
    //
    // http input stream reads data.
    // it uses when we request to execute csp command.
    // if the option of command is persistent connection we keep it and wait for data "inifinite time".
    // the "infinit time" is feature of GIConfig instance
    //
    GICSPStream       *stream;
    
    dispatch_queue_t  processingQueue;
    dispatch_queue_t  errorQueue;
    dispatch_queue_t  mainQueue;
    
    //
    // Timers
    //
    NSTimer *connectionTimeoutTimer;
    NSTimer *readTimeoutTimer;
    NSTimer *persistTimeoutTimer;
    
    BOOL   _executing;
    BOOL   _finished;
    
    
    //
    //
    //
    BOOL      streamingInProgress;
}

- (BOOL)isExecuting{    return _executing; }

- (BOOL)isFinished{    return _finished; }

- (BOOL)isConcurrent {return NO;}

//
// Operations messages
//
- (void)start{
    [self main];
}


- (void) __main__{
    if (stream) {
        [stream close]; stream = nil;
    }
    
    [self pollingData];
    
    //
    // Current context is waiting for when the stream connection will close
    //
    CFRunLoopRun();
}

- (void) main{
    @autoreleasepool {
        @try {
            [self __main__];
        }
        @catch (NSException *exception) {
            NSLog(@"%@: %s:%i", exception, __FILE__, __LINE__);
            [self __main__];
        }
    }
}


- (void) pollingData{
    [self makeNewRequest];
    
    stream = [[GICSPStream alloc] initWithURLRequest:request];
    
    //
    // update ssl setings if we need
    //
    
    [stream setDelegate: self];
    stream.validatesSecureCertificate = self.config.validatesSecureCertificate;
    
    // it needs to be KVO compliant
    [self willChangeValueForKey:@"isExecuting"];
    _executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    //
    // run handler loop
    //
    [stream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    //
    // create peer
    //
    [stream open];
}

- (void) cancel{
    
    if([self isCancelled])
        return;
    
    [self stopConnection];
    [super cancel];
}


- (void) close{
    
    // remove it from handle loop
    if (!stream)
        return
        
        // close stream
        // [stream close];
        
        [stream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    // stop current thread loop
    CFRunLoopStop(CFRunLoopGetCurrent());
    
    //stream = nil;
}

- (void) stopConnection{
    
    if ([self isFinished]) {
        return;
    }
    
    //
    // Alert anyone that we are finished
    //
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    _executing = NO;
    _finished  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
    
    if (connectionTimeoutTimer) [connectionTimeoutTimer invalidate]; connectionTimeoutTimer = nil;
    if (readTimeoutTimer) [readTimeoutTimer invalidate]; readTimeoutTimer = nil;
    if (persistTimeoutTimer) [persistTimeoutTimer invalidate]; persistTimeoutTimer = nil;
    
    [self close];
}

- (void) stopConnectionByTimer: (NSTimer*) timer
{
    NSArray      *_error = [timer userInfo];
    dispatch_async(errorQueue, ^(void){
        GIError *error=[GICSPError errorWithCode: (int)[[_error objectAtIndex:0 ] integerValue]
                                  andDescription:[_error objectAtIndex:1]];
        //[self.handler onError: error fromCommand:self.command];
        [self.handler cspExecutor:self onError:error];
    });
    [self stopConnection];
}

- (NSTimer*) startTimer:(time_t)timeout withErrorCode: (GICSP_ERRORS)code andDescription: (NSString*)description
{
    if(timeout){
        NSArray *_error_info = [NSArray arrayWithObjects:[NSNumber numberWithInt: code], description, nil];
        return [NSTimer scheduledTimerWithTimeInterval:timeout
                                                target: self
                                              selector: @selector(stopConnectionByTimer:)
                                              userInfo: _error_info
                                               repeats: NO];
    }
    return nil;
}

-(void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    
    @try {
        BOOL   should_it_close =NO;
        
        //@autoreleasepool {
            
            switch (eventCode) {
                    //
                    // Error handling
                    //
                case NSStreamEventOpenCompleted:
                {
                    
                    //
                    // should execute in the the same context
                    //
                    [self.handler cspExecutorOpened:self];
                    
                    //
                    // set connection timer for request
                    //
                    if (![self.options isPersistent])
                        
                        connectionTimeoutTimer = [self startTimer:self.config.connectionTimeout
                                                    withErrorCode:GICSP_ERROR_CONNECTION_TIMEOUT
                                                   andDescription:GILocalizedString(@"The CSP connection couldn’t be completed. Operation timed out.", @"CSP connection timed out") ];
                    
                    //
                    // reconnect receiver
                    //
                    if ([self.options isPersistent] && self.config.persistTimeout>0)
                        //
                        // Set peer connection time limit
                        //
                        persistTimeoutTimer = [self startTimer:self.config.persistTimeout
                                                 withErrorCode:GICSP_ERROR_PERSISTENT_TIMEOUT
                                                andDescription:GILocalizedString(@"The CSP receiver connection too long. Operation interrupted.", @"Opertaion interrupted")];
                    
                    streamingInProgress  = NO;
                    break;
                }
                    
                case NSStreamEventErrorOccurred:
                {
                    
                    void (^_error_block)(void) = ^(void){
                        
                        if ([[aStream streamError] code] <= errSSLProtocol) {
                            
                            GIError *error = [GICSPError errorWithCode:GICSP_ERROR_CERT_INVALID
                                                        andDescription:[NSString stringWithFormat:
                                                                        GILocalizedString(@"%@ certificate is not trusted for the receiver peer connection.", @"Bad certificate"), [self.config.serverUrl host]]];                            
                            [self.handler cspExecutor:self onError:error];
                        }
                        else{
                            GICSPError *_error ;
                            if ([aStream streamError])
                                _error = [[GICSPError alloc] initWithNSError:[aStream streamError]];
                            else
                                _error = [GICSPError errorWithCode:GICSP_ERROR_CONNECTION_FAIL
                                                    andDescription:[NSString stringWithFormat:
                                                                    GILocalizedString(@"Connection to %@ could not be created.", @"Connection could not be created"), commandUrl]
                                          ];
                            [self.handler cspExecutor:self onError:_error];
                        }};
                    
                    dispatch_async(errorQueue, _error_block);
                    
                    should_it_close = YES;
                    streamingInProgress = NO;
                    break;
                }
                    
                    //
                    // Connection peer has closed
                    //
                case NSStreamEventEndEncountered:
                {
                    if ([self.options isPersistent]) {
                        dispatch_async(processingQueue, ^(void){
                            GICSPError *_error =  [GICSPError errorWithCode:GICSP_ERROR_RECEIVER_CLOSED
                                                             andDescription:[NSString stringWithFormat:
                                                                             GILocalizedString(@"%@ connection closed or cancelled.", @""), commandUrl]];
                            [self.handler cspExecutor:self onError:_error];
                        });
                    }
                    should_it_close = YES;
                    streamingInProgress = NO;
                    break;
                }
                    
                    //
                    // Data processing
                    //
                case NSStreamEventHasBytesAvailable:
                {
                    
                    if (connectionTimeoutTimer) [connectionTimeoutTimer invalidate]; connectionTimeoutTimer = nil;
                    
                    if(![self.options isPersistent])
                        //
                        // request timeout. obviously peer connection has the infinit reading time
                        //
                        readTimeoutTimer = [self startTimer:self.config.readTimeout withErrorCode:GICSP_ERROR_READ_TIMEOUT
                                             andDescription:GILocalizedString(@"The CSP connection read date timeout. Operation interrupted.", @"Connection interrupted")];
                    
                    //
                    // Read body
                    //
                    
                    GICSPError *_error = nil;
                    NSData *_data = [stream readData:&_error];
                    
                    if (_error){
                        //
                        // pass to error hanlder
                        //
                        dispatch_async(errorQueue, ^(void){
                            [self.handler cspExecutor:self onError:_error];
                        });
                        should_it_close = YES;
                        return;
                    }
                    
                    if (!stream || !self.isExecuting) {
                        streamingInProgress = NO;
                        break;
                    }
                    
                    if(!_data) {
                        streamingInProgress = NO;
                        break;
                    }
                    
                    if (stream.statusCode/100>3) {
                        //
                        // wrong http header
                        //
                                            
                        should_it_close = YES;
                        streamingInProgress = NO;
                        dispatch_async(errorQueue, ^(void){
                            int errorcode = GISP_ERROR_HTTP_STATUS_UNAVAILABLE;
                            if (stream.statusCode==GISP_ERROR_HTTP_STATUS_SID_EXPIRED) {
                                errorcode=GISP_ERROR_HTTP_STATUS_SID_EXPIRED;
                            }
                            GICSPError *_error =  [GICSPError errorWithCode:errorcode
                                                             andDescription:[NSString stringWithFormat:
                                                                             GILocalizedString(@"%@ connection closed or cancelled.", @""), commandUrl]];
                            [self.handler cspExecutor:self onError:_error];
                        });
                        break;
                    };
                    
                    if (![self.options isPersistent]) {
                        //
                        // request and close connection
                        //
                        dispatch_async(processingQueue, ^(void){
                            [self.handler cspExecutor:self didDataReceive:_data withHeaders:stream.responseHeaders withStatus:stream.statusCode];
                        });
                    }
                    else if (_data) {
                        
                        if (!streamingInProgress || !self.completedData) {
                            //
                            // Start reading process
                            //
                            streamingInProgress = YES;
                            
                            //
                            // create new data store
                            //
                            
                            self.completedData = [[GICSPChunkedData alloc] init];
                        }
                        
                        //
                        // Win! pass data to connection handler
                        //
                        
                        GICSPError *_err;
                        
                        [self.completedData appendData:_data error:&_err];
                        
                        if (_err) {
                            NSLog(@"%@ %s:%i", _err, __FILE__, __LINE__);
                        }
                        
                        //if ([self.handler isDataCompleted:_data]) {
                        if ([self.handler cspExecutor:self isDataCompleted:_data]) {
                            //
                            // When all data has received and the end of data is marked -
                            // call handler
                            //
                            NSData *_d = [[self.completedData assembledData] copy];
                            dispatch_async(processingQueue, ^(void){
                                [self.handler cspExecutor:self didDataReceive:_d withHeaders:stream.responseHeaders withStatus:stream.statusCode];
                            });
                            
                            //
                            // keep to reconnect
                            //
                            if (_seqnum<self.completedData.currentSeqNum) {
                                _seqnum = (NSInteger) self.completedData.currentSeqNum;
                            }

                            streamingInProgress = NO;
                            self.completedData = nil;
                        }
                        
                        should_it_close = NO;
                        break;
                    }
                    else
                        streamingInProgress = NO;
                }
                    
                    //
                    // Just ignore
                    //
                default:
                    break;
            }
        //}
        
        if (readTimeoutTimer) [readTimeoutTimer invalidate]; readTimeoutTimer = nil;
        
        if (should_it_close) {
            [self stopConnection];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        streamingInProgress = NO;
        [self stopConnection];
        
        dispatch_async(errorQueue, ^{
            GICSPError *_error = [GICSPError errorWithCode:GICSP_ERROR_CONNECTION_FAIL
                                            andDescription:GILocalizedString(@"The CSP connection read fail. Operation interrupted.", @"Connection interrupted")];
            [self.handler cspExecutor:self onError:_error];
        });
        
    }
}


- (void) makeNewRequest{
    //
    // Keep request context
    //
    request = [NSMutableURLRequest requestWithURL: commandUrl
                                      cachePolicy: NSURLRequestReloadIgnoringCacheData
                                  timeoutInterval: [self.options isPersistent]? self.config.persistTimeout : self.config.connectionTimeout
               ];
    
    //
    // GI-CSP specific HTTP headers
    //
    [request setAllHTTPHeaderFields:[NSDictionary dictionaryWithObjectsAndKeys:
                                     [self.config.serverUrl host], @"Host",
                                     @"text/plain", @"Content-type",
                                     @"no-cache",   @"Pragma",
                                     @"no-cache",   @"Cache-Control",
                                     [NSString stringWithFormat:@"%@", @"gzip"], @"Accept-Encoding",
                                     nil]];
    
    if (self.seqnum > 0) {
        [request setValue: [NSString stringWithFormat:@"%ld", (long)self.seqnum] forHTTPHeaderField: @"X-CspHub-Seqnum"];
    }
    
    //
    // How to make request
    //
    if (self.postData){
        request.HTTPMethod = @"POST";
        request.HTTPBody   = self.postData;
        [request setValue: [NSString stringWithFormat:@"%lu", (unsigned long)[self.postData length]] forHTTPHeaderField: @"Content-Length"];
        
    }
    else
        request.HTTPMethod = @"GET";
}

//
// init instance
//
- (id) initCommand:(NSString*)aCommand withData:(NSData*)aData andHandler: (id<GICSPCommandHandler>)aHandler andConfig: (GIConfig*)aConfig{
    //@synchronized (self){
        if(!(self=[super init]))
            return nil;
        
        void (^clean_up_block)() = ^(void){
            [self _cleanUpAllInstances];
        };
        
        [super setCompletionBlock: clean_up_block];
        
        
        self.seqnum   = 0;
        self.postData = aData;
        self.config   = aConfig;
        self.command  = aCommand;
        self.handler  = aHandler;
        
        //
        // queues
        //
        processingQueue = dispatch_queue_create("com.infocx.processing.queue", DISPATCH_QUEUE_SERIAL);
        errorQueue = dispatch_queue_create("com.infocx.error.queue", DISPATCH_QUEUE_SERIAL);
        mainQueue  = dispatch_queue_create("com.infocx.persistent.queue", DISPATCH_QUEUE_SERIAL);
        
        commandUrl = [self.config.serverUrl URLByAppendingPathComponent: self.command isDirectory:NO];
        
        return self;
    //}
}

//
// Clean all
//
- (void) _cleanUpAllInstances{
    @synchronized (self){        
        //
        // Stop all operations
        //
        if (stream)
            [stream close];
        
        stream = nil;
        
        [self stopConnection];
    }
}

@end




