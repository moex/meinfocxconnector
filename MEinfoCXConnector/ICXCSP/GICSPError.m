//
//  GIError+GICSPTunnelError.m
//  GIConnector
//
//  Created by denn on 3/11/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GICSPError.h"

@implementation  GICSPError

+ (id)errorWithCode:(int)code andDescription:(NSString*)description{
    NSDictionary *userInfo = [
                              NSDictionary 
                              dictionaryWithObjects:[NSArray arrayWithObjects:  description, @"CSP Tunnel error", nil] 
                              forKeys:[NSArray arrayWithObjects: NSLocalizedDescriptionKey, NSLocalizedFailureReasonErrorKey, nil]
                              ];        
    
    return [NSError errorWithDomain:[NSString stringWithFormat:@"%@.http.csptunnel", GI_ERROR_DOMAIN_PREFIX] 
                                                 code:code
                                             userInfo:userInfo];
}


+ (id) errorWithHTTPStatus:(int)status andDescription:(NSString *)statusString{
    NSString *_status_str = [NSHTTPURLResponse localizedStringForStatusCode:status];
    
    NSDictionary *userInfo = [
                              NSDictionary 
                              dictionaryWithObjects:[NSArray arrayWithObjects: _status_str, statusString, nil] 
                              forKeys:[NSArray arrayWithObjects: NSLocalizedDescriptionKey, NSLocalizedFailureReasonErrorKey, nil]
                              ];
    
    return [NSError errorWithDomain:[NSString stringWithFormat:@"%@.http.status", GI_ERROR_DOMAIN_PREFIX] 
                                         code:status
                                     userInfo:userInfo];        
}


@end
