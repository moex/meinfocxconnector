//
//  GICSPCommand.m
//  GIConnector
//
//  Created by denn on 3/12/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//


#import <zlib.h>
#import <security/SecureTransport.h>

#import "GICSPCommandQueue.h"
#import "GICSPError.h"
#import "GICSPStreamHeader.h"


@interface GICSPCommandQueue ()

@property  GIConfig *config;
@property  NSString *name;

@end

@implementation GICSPCommandQueue
{
    NSOperationQueue     *selfQueue;
    GICSPCommandExecutor *exec;
}

@synthesize handler, name, config;

//
// Managing interface
//
- (void) execute:(NSString*)command{
    [self execute:command withData:nil];
}

- (void) executeAndWait:(NSString *)command{
    [self execute: command];
    [selfQueue waitUntilAllOperationsAreFinished];
}

- (void) execute:(NSString *)command withData:(NSData *)data{
    
    exec = [[GICSPCommandExecutor alloc] initCommand:command withData:data andHandler:self.handler andConfig:config];
    
    exec.options = self;
    
    @try {
        [selfQueue addOperation: exec];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
    }
}

- (void) cancel{
    [selfQueue cancelAllOperations];
}

- (NSInteger) seqnum{
    return exec.seqnum;
}

- (void) relaunch{
    @try {
        
        //
        // keep previous context
        //
        NSString *command = exec.command;
        NSData   *postData= exec.postData;
        NSInteger seqnum  = exec.seqnum;
        
        GICSPChunkedData   *completedData = exec.completedData;        
        
        NSLog(@" RESTORE CONNECTION SEQNUM:  dataseqnum %li:  last-seqnum = %li %@", (long)completedData.currentSeqNum, (long)seqnum, command);
        [exec cancel];
                
        //
        // make new exec instance
        //
        exec = [[GICSPCommandExecutor alloc] initCommand:command withData:postData andHandler:self.handler andConfig:config];
        
        //
        // restore conext
        //
        exec.options = self;
        exec.seqnum  = seqnum;
        exec.completedData = completedData;
        [exec.completedData flushIncompleted];
        
        [selfQueue addOperation: exec];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
    }
}

//
// Initialization methodes
//
- (id) initFromConfig:(GIConfig *)aConfig{
    
    if(!(self=[super init]))
        return nil;
    
    self.config = aConfig;
    
    selfQueue = [NSOperationQueue new];    
    
    return self;
}

@end
