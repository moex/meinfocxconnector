//
//  GICSStreamHeader.h
//  GIConnector
//
//  Created by denn on 3/11/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef char _tp_header_size[4];

/**
 *  Buffer CSP structure.
 */
typedef struct {
    _tp_header_size header_size;
    char seqnum[8];
    char length[8];
    
} CSPStreamPacketHeader;

#define GICSP_HEADER_SIZE (sizeof(CSPStreamPacketHeader)-sizeof(_tp_header_size))

/**
 *  Class describes the CSP header.
 */
@interface GICSPStreamHeader : NSObject
/**
 *  Header size.
 */
@property(readonly,nonatomic) long long int header_size;

/**
 *  Sequence number.
 */
@property(readonly,nonatomic) long long int seqnum;

/**
 *  Data after header length.
 */
@property(readonly,nonatomic) long long int length;

/**
 *  Parse CSP header packet.
 *  
 *  @param packet header structure.
 *  
 *  @return parsed header.
 */
- (id) initWithCSPPacket: (CSPStreamPacketHeader)packet;

/**
 *  Parse CSP header buffer.
 *  
 *  @param data header raw buffer
 *  
 *  @return parsed header.
 */
- (id) initWithData: (NSData*)data;
@end


@interface CSPStreamHeader : NSObject 
@end 



