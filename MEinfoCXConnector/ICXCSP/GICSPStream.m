//
//  GICSPStream.m
//  GIConnector
//
//  Created by denn on 6/26/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIUtils.h"
#import "GICSPStream.h"
#import "GIConfig.h"

@interface GICSPStream() <NSStreamDelegate>
@property z_stream      zStream;
@property BOOL isChuncked;
@property BOOL isGzipped;
@end

//
// I have found out ios7 has a problem assosiated with using CFReadStreamCreateForStreamedHTTPRequest().
// In case if we will try to work with asynchronous stream:handleEvent it does not call when event occurs realy.
// I can't wait for the bug will be fixed. So, i temporary make work-around code uses 
// CFStreamCreatePairWithSocketToHost() to process raw stream.
//
//
// YAHOOO! The problem was fixed in ios7 beta3!
//
//

#if 1 // (__IPHONE_OS_VERSION_MAX_REQUIRED<=60000)

@implementation GICSPStream
{
    NSURLRequest  *request;
    NSInputStream *istream;
    BOOL isHTTPS;
    NSDictionary *sslSettings;
    BOOL isStarted;
}

@synthesize delegate=_delegate, validatesSecureCertificate=_validatesSecureCertificate;

- (id) initWithURLRequest: (NSURLRequest*) aRequest{
    self = [super init];
    
    if (self) {
        isStarted = YES;
        _validatesSecureCertificate = YES;
        request = aRequest;
        istream = (__bridge_transfer NSInputStream *)(CFReadStreamCreateForHTTPRequest(CFAllocatorGetDefault(), assignNSURLRequestToCFHTTPMess(aRequest)));
        istream.delegate = self;
    }
    
    return self;
}

- (void) _initSSL{
    if ([[[request URL] scheme] isEqualToString:@"https"]) {
        isHTTPS=YES;
        if (!(_validatesSecureCertificate))
//            sslSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
//                           [NSNumber numberWithBool:YES], kCFStreamSSLAllowsExpiredCertificates,
//                           [NSNumber numberWithBool:YES], kCFStreamSSLAllowsAnyRoot,
//                           [NSNumber numberWithBool:NO],  kCFStreamSSLValidatesCertificateChain,
//                           kCFNull,kCFStreamSSLPeerName,
//                           nil];
            sslSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                       [NSNumber numberWithBool:NO], kCFStreamSSLValidatesCertificateChain,
                       kCFNull,kCFStreamSSLPeerName,
                       nil];
        else
            sslSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
//                           [NSNumber numberWithBool:NO], kCFStreamSSLAllowsExpiredCertificates,
//                           [NSNumber numberWithBool:NO], kCFStreamSSLAllowsAnyRoot,
                           [NSNumber numberWithBool:YES],  kCFStreamSSLValidatesCertificateChain,
                           nil];
    }
    else
        isHTTPS=NO;
}

- (void) stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    [self.delegate stream: istream handleEvent: eventCode];
}

- (void) setupStreamContext{
    
    CFHTTPMessageRef _inresponse = (CFHTTPMessageRef)CFReadStreamCopyProperty((__bridge CFReadStreamRef)(istream), kCFStreamPropertyHTTPResponseHeader);
    if (!_inresponse) {
        return;
    }
    
    _statusCode     = CFHTTPMessageGetResponseStatusCode(_inresponse);
    _statusLine = (NSString*) CFBridgingRelease(CFHTTPMessageCopyResponseStatusLine(_inresponse));
    
    _responseHeaders = (__bridge_transfer NSDictionary *)(CFHTTPMessageCopyAllHeaderFields(_inresponse));
    
    NSInteger contentLength   = [_responseHeaders[@"Content-Length"] integerValue];
    NSString *_encoding  = _responseHeaders[@"Content-Encoding"];
    
    if (_encoding && [_encoding rangeOfString:@"gzip"].location != NSNotFound){
        //
        // we need to decompress data
        //
        [self setupZstream];
    }
    
    if (contentLength==0 && [_responseHeaders[@"Transfer-Encoding"] hasSuffix:@"Identity"]) {
        _isChuncked = YES;
    }
    else
        _isChuncked = NO;
    
    if (_inresponse)
        CFRelease(_inresponse);
}


- (BOOL)hasBytesAvailable{
    return CFReadStreamHasBytesAvailable((CFReadStreamRef)istream);
}

- (NSInteger)read:(uint8_t *)buffer maxLength:(NSUInteger)len{
    NSInteger len_ = [istream read:buffer maxLength:len];
    
    if (isStarted) {
        isStarted = NO;
        [self setupStreamContext];
    }    
    return len_;
}

- (NSData*)readData: (GICSPError**)error{
    uint8_t _buffer[CSP_BUFFER_SIZE];
    
    memset(_buffer, 0, CSP_BUFFER_SIZE);
    
    NSInteger     _buffer_size = sizeof(_buffer);
    NSInteger     len  = 0;
    
    NSMutableData *_streamed_buffer = [NSMutableData dataWithCapacity:0];
    
    do {
        //
        // read all bytes
        //
        len = [self read:_buffer maxLength:_buffer_size];
        
        if(len<=0)
            break;
        @try {
            [_streamed_buffer appendBytes:_buffer length:len];
        }
        @catch (NSException *exception) {
            NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
            *error = [GICSPError errorWithCode:GICSP_ERROR_INCOMPLETED_CSP_PACKET
                                andDescription:[NSString stringWithFormat:
                                                GILocalizedString(@"Connection to %@ could be continued. Bad packet received.", @"Connection bad packet receieved."), [request URL]]
                      ];
            return nil;
        }
        
    } while ([istream hasBytesAvailable]);
    
    NSData *_data;
    if (_isGzipped) {
        
        //
        // We could not parse gzipped stream
        //
        
        _data = [self uncompressBytes:[_streamed_buffer bytes] length:[_streamed_buffer length] error:error];
        
        if (!_data){
            //
            // pass to error hanlder
            //
            NSLog(@"%@", *error);
            return nil;
        }
    }
    else
        _data = _streamed_buffer;
    
    //NSLog(@" streamed %@: ((((  %@  )))) ", _data, [[NSString alloc] initWithBytes:[_data bytes] length: [_data length] encoding:NSUTF8StringEncoding]);
    
    return _data;
}


- (void) open{
    [self _initSSL];
    if(isHTTPS)
        CFReadStreamSetProperty((__bridge CFReadStreamRef)(istream), kCFStreamPropertySSLSettings, (CFTypeRef)sslSettings);
    [istream open];
}

- (void) close{
    [istream close];
}

- (id) propertyForKey:(NSString *)key{
    return [istream propertyForKey:key];
}

- (BOOL) setProperty:(id)property forKey:(NSString *)key{
    return [istream setProperty:property forKey:key];
}

- (void) scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode{
    [istream scheduleInRunLoop:aRunLoop forMode:mode];
}

- (void) removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode{
    [istream removeFromRunLoop:aRunLoop forMode:mode];
}

- (NSStreamStatus) streamStatus{
    return [istream streamStatus];
}

- (NSError*) streamError{
    return [istream streamError];
}


- (NSData *)uncompressBytes:(const void*)bytes length:(NSUInteger)length error:(out GICSPError **)err{
    
    if (length == 0) return nil;
    
    NSUInteger halfLength = length/2;
    NSMutableData *outputData = [NSMutableData dataWithLength:length+halfLength];
    
    int status;
    
    _zStream.next_in = (Bytef*)bytes;
    _zStream.avail_in = (unsigned int)length;
    _zStream.avail_out = 0;
    
    GICSPError *theError = nil;
    
    NSInteger bytesProcessedAlready = _zStream.total_out;
    while (_zStream.avail_in != 0) {
        
        if (_zStream.total_out-bytesProcessedAlready >= [outputData length]) {
            [outputData increaseLengthBy:halfLength];
        }
        
        _zStream.next_out = [outputData mutableBytes] + _zStream.total_out-bytesProcessedAlready;
        _zStream.avail_out = (unsigned int)([outputData length] - (_zStream.total_out-bytesProcessedAlready));
        
        status = inflate(&_zStream, Z_NO_FLUSH);
        
        if (status == Z_STREAM_END) {
            break;
        } else if (status != Z_OK) {
            if (err) {
                *err = [[self class] inflateErrorWithCode:status];
            }
            return nil;
        }
    }
    
    if (theError) {
        if (err) {
            *err = theError;
        }
        return nil;
    }
    
    // Set real length
    [outputData setLength: _zStream.total_out-bytesProcessedAlready];
    return outputData;
}

- (GIError*) setupZstream{
    
    if (_isGzipped) {
        return nil;
    }
    
    // Setup the inflate stream
    _zStream.zalloc   = Z_NULL;
    _zStream.zfree    = Z_NULL;
    _zStream.opaque   = Z_NULL;
    _zStream.avail_in = 0;
    _zStream.next_in  = 0;
    int status = inflateInit2(&_zStream, (15+32));
    if (status != Z_OK) {
        return [[self class] inflateErrorWithCode:status];
    }
    _isGzipped = YES;
    return nil;
}

+ (GICSPError *)inflateErrorWithCode:(int)code
{
    NSDictionary *userInfo = [
                              NSDictionary
                              dictionaryWithObjects:[NSArray arrayWithObjects: @"Stream input error",
                                                     [NSString stringWithFormat:
                                                      GILocalizedString(@"Decompression of data failed with code %i", @"Decompretion error"), code ], nil]
                              forKeys:[NSArray arrayWithObjects: NSLocalizedDescriptionKey, NSLocalizedFailureReasonErrorKey, nil]
                              ];
    return [[GICSPError alloc]
            initWithNSError: [NSError errorWithDomain:[NSString stringWithFormat:@"%@.http.stream", GI_ERROR_DOMAIN_PREFIX]
                                                 code:code
                                             userInfo:userInfo]];
}


static CFHTTPMessageRef assignNSURLRequestToCFHTTPMess(NSURLRequest *request) {
    
    CFHTTPMessageRef message = CFHTTPMessageCreateRequest(kCFAllocatorDefault, (__bridge CFStringRef)[request HTTPMethod], (__bridge CFURLRef)[request URL], kCFHTTPVersion1_1);
    
    for (NSString *currentHeader in [request allHTTPHeaderFields]) {
        CFHTTPMessageSetHeaderFieldValue(message, (__bridge CFStringRef)currentHeader, (__bridge CFStringRef)[[request allHTTPHeaderFields] objectForKey:currentHeader]);
    }
    
    if ([request HTTPBody] != nil) {
        CFHTTPMessageSetBody(message, (__bridge CFDataRef)[request HTTPBody]);
    }
    
    return message;
}

@end

#else

@implementation GICSPStream
{
    NSInputStream   *istream;
    NSOutputStream  *ostream;
    NSURLRequest    *request;
    
    NSDictionary    *sslSettings;

    NSMutableData   *readBuffer;
    NSInteger       lastChunkSize;
    
    BOOL isStarted;
    BOOL isHTTPS;
    
    GICSPError *lastError;
}

@synthesize delegate=_delegate, validatesSecureCertificate=_validatesSecureCertificate;

- (id) initWithURLRequest: (NSURLRequest*) aRequest{
    self = [super init];
    
    if (self) {
        lastChunkSize = 0;
        _validatesSecureCertificate = YES;
        _isChuncked = NO;
        _isGzipped = NO;

        isStarted = YES;
        request = aRequest;
        
        if ([[[request URL] scheme] isEqualToString:@"https"]) {
            isHTTPS = YES;
        }
        else
            isHTTPS = NO;
        
        CFReadStreamRef  readStream;
        CFWriteStreamRef writeStream;
                
        NSURL *_url = [request URL];
        NSString *_host = _url.host;
        NSInteger _port = _url.port.intValue==0?isHTTPS?443:80:_url.port.intValue;
                
        CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)(_host), _port, &readStream, &writeStream);
                
        ostream = (__bridge_transfer NSOutputStream *)(writeStream);
        istream = (__bridge_transfer NSInputStream *)(readStream);
        
        istream.delegate = self;
        ostream.delegate = self;
    }
    
    return self;
}

-(void)sendRequest{
    NSMutableString *sendingString =   [NSMutableString stringWithFormat:@"%@ %@ HTTP/1.1\r\n", [request HTTPMethod], [[request URL] path]];
    for (NSString *headerName in request.allHTTPHeaderFields) {
        [sendingString appendFormat:@"%@: %@\r\n",headerName,request.allHTTPHeaderFields[headerName]];
    }
    [sendingString appendString:@"\r\n"];
    
    NSMutableData *sendingData = [NSMutableData dataWithData:[sendingString dataUsingEncoding:NSUTF8StringEncoding]];
    if (request.HTTPBody) {
        [sendingData appendData:request.HTTPBody];
    }
    
    [ostream write:[sendingData bytes] maxLength:[sendingData length]];
}

- (char*) nextChunk: (char*) buffer bufferLen:(int)bufferLen currentLen:(int*)current_len currentIndex:(int*)current_index{
    char *chunk_len=buffer;
    *current_len=0;
    for (int i=0; i<bufferLen; chunk_len++,i++) {
        if (strncmp(chunk_len, "\r\n", 2)==0) {
            *current_index = i+2;
            *current_len=Ascii2int(buffer, i);
            break;
        }
    }
    int next_chunk_index=*current_len+*current_index+2;
    if (*current_len==0 || bufferLen<=next_chunk_index) {
        return NULL;
    }
    return &buffer[next_chunk_index];
}

- (void) stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    if (aStream == ostream) {
        if (eventCode == NSStreamEventHasSpaceAvailable) {
            [self sendRequest];
            [ostream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        }
        else{
            readBuffer = nil;
            [self.delegate stream: ostream handleEvent: eventCode];
        }            
        return;
    }
    
    if (eventCode == NSStreamEventHasBytesAvailable) {

        readBuffer = [NSMutableData dataWithCapacity:0];
        
        do {
            
            uint8_t buffer[CSP_BUFFER_SIZE]; //memset(buffer, 0, sizeof(buffer));
            int     buffer_size = sizeof(buffer);
            
            //
            // read bytes
            //
            int len = [istream read:buffer maxLength:buffer_size], reslultlen=len;
                        
            //
            // stop read
            //
            if (len<=0 )
                break;
            
            if (isStarted) {
                //
                // read headers
                //
                isStarted = NO;
                
                char *next=(char *)buffer;
                NSData *headers;
                for (int i=0; i<len; i++) {
                    if (strncmp(next, "\r\n\r\n", 4)==0) {
                        //
                        // prepare to parse
                        //
                        headers = [NSData dataWithBytes:buffer length:i];
                        //
                        // body
                        //
                        next+=4; reslultlen-=4;
                        //
                        // set up body
                        //
                        memmove(buffer, next, reslultlen);
                        buffer[reslultlen]='\0';
                        break;
                    }
                    reslultlen--; next++;
                }
                //
                // setup headers
                //
                [self setupStreamContextFromResponse:headers];
            }
            
            //
            // append readbuffer
            //
                        
            char *result_buffer = (char*)buffer;
            
            if (_isChuncked && !isStarted && reslultlen>0){
                
                char *current_chunk;
                int current_chunk_len = 0;
                if (lastChunkSize>0) {
                    // last time we have incompleted chunk
                    [readBuffer appendBytes:result_buffer length:lastChunkSize];
                    if (reslultlen>lastChunkSize) {
                        lastChunkSize+=2; //\r\n
                        // cut buffer
                        reslultlen-=lastChunkSize;
                        memmove(result_buffer, &result_buffer[lastChunkSize], reslultlen);
                        result_buffer[reslultlen]='\0';
                        lastChunkSize=0;
                    }
                    else{
                        lastChunkSize-=reslultlen;
                        reslultlen=0;
                    }
                }
                //
                // find chunk size
                //
                current_chunk = (char*)result_buffer;
                for (int i=0, clen=0; i<reslultlen; i++, clen++) {
                    lastChunkSize = 0;
                    if (strncmp((char*)&result_buffer[i], "\r\n", 2)==0) {
                        // acb\r\n[data]\r\n....acb\r\n[\r\n]...
                        current_chunk_len=Ascii2int(current_chunk, clen);
                        
                        // current chunk index
                        i+=2; // \r\n
                        int current_index=i;
                        i+=current_chunk_len;
                        current_chunk = (char*)&result_buffer[current_index];
                        
                        if (reslultlen<i) {
                            // out of responsed
                            // rest size
                            lastChunkSize=current_chunk_len-(reslultlen-current_index);
                            [readBuffer appendBytes:current_chunk length:reslultlen-current_index];
                            break;
                        }
                        else{
                            [readBuffer appendBytes:current_chunk length:current_chunk_len];
                            // next chunk
                            i = current_index + current_chunk_len +2;
                            current_chunk += current_chunk_len+2;
                            clen = 0;
                        }
                    }
                }                
            }
            else
                if (reslultlen>0)
                    [readBuffer appendBytes:result_buffer length:reslultlen];
            
        } while ([istream hasBytesAvailable]);
        

        if ([readBuffer length]>0) {
            [self.delegate stream: istream handleEvent: eventCode];
        }
        
    }
    else{
        readBuffer = nil;
        [self.delegate stream: istream handleEvent: eventCode];
    }
}

- (NSData*)readData: (GICSPError**)error{

    NSData *_data;
    if (_isGzipped) {
        
        //
        // We could not parse gzipped stream
        //        
        _data = [self uncompressBytes:[readBuffer bytes] length:[readBuffer length] error:error];
        
        if (*error){
            //
            // pass to error hanlder
            //
            NSLog(@"%@", *error);
            return nil;
        }
    }
    else
        _data = readBuffer;
        
    return _data;
}

- (BOOL)hasBytesAvailable{
    return CFReadStreamHasBytesAvailable((CFReadStreamRef)istream);
}

- (void) open{
    [self _initSSL];
    if(isHTTPS){
        CFReadStreamSetProperty((__bridge CFReadStreamRef)(ostream), kCFStreamPropertySSLSettings, (CFTypeRef)sslSettings);
        CFReadStreamSetProperty((__bridge CFReadStreamRef)(istream), kCFStreamPropertySSLSettings, (CFTypeRef)sslSettings);
    }
    [ostream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [ostream open];
    [istream open];
}

- (void) close{
    if (ostream)         
        [ostream close];
    if (istream) 
        [istream close];
}

- (id) propertyForKey:(NSString *)key{
    return [istream propertyForKey:key];
}

- (BOOL) setProperty:(id)property forKey:(NSString *)key{
    return [istream setProperty:property forKey:key];
}

- (void) scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode{
    [istream scheduleInRunLoop:aRunLoop forMode:mode];
}

- (void) removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode{
    [istream removeFromRunLoop:aRunLoop forMode:mode];
}

- (NSStreamStatus) streamStatus{
    return [istream streamStatus];
}

- (NSError*) streamError{
    return [istream streamError];
}

- (void) setupStreamContextFromResponse: (NSData*)data{
    
    NSString *response = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSUTF8StringEncoding];
    
    NSArray *responsedLines = [response componentsSeparatedByString:@"\r\n"];
        
    NSMutableDictionary *_headers = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    for (NSString *line in responsedLines) {
        if ([line hasPrefix:@"HTTP/"]) {
            NSArray *status = [line componentsSeparatedByString:@" "];
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber * statusCode = [f numberFromString:status[1]];
            _statusCode = statusCode.intValue;
        }
        else{
            if ([line isEqualToString:@""]) {
                break;
            }
            else{
                NSArray *headerLine = [line componentsSeparatedByString:@":"];
                NSString *headerName = [headerLine[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString *headerValue = [headerLine[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                [_headers setValue:headerValue forKey:headerName];
            }
        }
    }
    
    _responseHeaders = _headers;
    
    NSInteger contentLength   = [_responseHeaders[@"Content-Length"] integerValue];
    NSString *_encoding  = _responseHeaders[@"Content-Encoding"];
    
    if (_encoding && [_encoding rangeOfString:@"gzip"].location != NSNotFound){
        //
        // we need to decompress data
        //
        [self setupZstream];
    }
    
    if (contentLength==0 && ( [_responseHeaders[@"Transfer-Encoding"] hasSuffix:@"Identity"] || [_responseHeaders[@"Transfer-Encoding"] hasSuffix:@"chunked"])) {
        _isChuncked = YES;
    }
    else
        _isChuncked = NO;    
}


- (void) _initSSL{
    if (isHTTPS) {
        if (!(_validatesSecureCertificate))
            sslSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [NSNumber numberWithBool:YES], kCFStreamSSLAllowsExpiredCertificates,
                           [NSNumber numberWithBool:YES], kCFStreamSSLAllowsAnyRoot,
                           [NSNumber numberWithBool:NO],  kCFStreamSSLValidatesCertificateChain,
                           kCFNull,kCFStreamSSLPeerName,
                           nil];
        else
            sslSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [NSNumber numberWithBool:NO], kCFStreamSSLAllowsExpiredCertificates,
                           [NSNumber numberWithBool:NO], kCFStreamSSLAllowsAnyRoot,
                           [NSNumber numberWithBool:YES],  kCFStreamSSLValidatesCertificateChain,
                           nil];
    }
}

- (NSData *)uncompressBytes:(const void*)bytes length:(NSUInteger)length error:(GICSPError **)err{
    
    if (length == 0) return nil;
    
    NSUInteger halfLength = length/2;
    NSMutableData *outputData = [NSMutableData dataWithLength:length+halfLength];
    
    int status;
    
    _zStream.next_in = (Bytef*)bytes;
    _zStream.avail_in = (unsigned int)length;
    _zStream.avail_out = 0;
    
    GICSPError *theError = nil;
    
    NSInteger bytesProcessedAlready = _zStream.total_out;
    while (_zStream.avail_in != 0) {
        
        if (_zStream.total_out-bytesProcessedAlready >= [outputData length]) {
            [outputData increaseLengthBy:halfLength];
        }
        
        _zStream.next_out = [outputData mutableBytes] + _zStream.total_out-bytesProcessedAlready;
        _zStream.avail_out = (unsigned int)([outputData length] - (_zStream.total_out-bytesProcessedAlready));
        
        status = inflate(&_zStream, Z_NO_FLUSH);
        
        if (status == Z_STREAM_END) {
            break;
        } else if (status != Z_OK) {
            if (err) {
                *err = [[self class] inflateErrorWithCode:status];
            }
            return nil;
        }
    }
    
    if (theError) {
        if (err) {
            *err = theError;
        }
        return nil;
    }
    
    // Set real length
    [outputData setLength: _zStream.total_out-bytesProcessedAlready];
    return outputData;
}

- (GIError*) setupZstream{
    
    if (_isGzipped) {
        return nil;
    }
    
    // Setup the inflate stream
    _zStream.zalloc   = Z_NULL;
    _zStream.zfree    = Z_NULL;
    _zStream.opaque   = Z_NULL;
    _zStream.avail_in = 0;
    _zStream.next_in  = 0;
    int status = inflateInit2(&_zStream, (15+32));
    if (status != Z_OK) {
        return [[self class] inflateErrorWithCode:status];
    }
    _isGzipped = YES;
    return nil;
}

+ (GICSPError *)inflateErrorWithCode:(int)code
{
    NSDictionary *userInfo = [
                              NSDictionary
                              dictionaryWithObjects:[NSArray arrayWithObjects: @"Stream input error",
                                                     [NSString stringWithFormat:
                                                      GILocalizedString(@"Decompression of data failed with code %i", @"Decompretion error"), code ], nil]
                              forKeys:[NSArray arrayWithObjects: NSLocalizedDescriptionKey, NSLocalizedFailureReasonErrorKey, nil]
                              ];
    return [[GICSPError alloc]
            initWithNSError: [NSError errorWithDomain:[NSString stringWithFormat:@"%@.http.stream", GI_ERROR_DOMAIN_PREFIX]
                                                 code:code
                                             userInfo:userInfo]];
}

@end

#endif