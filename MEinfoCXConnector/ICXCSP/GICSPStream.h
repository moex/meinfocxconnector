//
//  GICSPStream.h
//  GIConnector
//
//  Created by denn on 6/26/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <zlib.h>
#import "GICSPError.h"

#define CSP_BUFFER_SIZE (128*1024)


/**
 *  Handle stream events.
 */
@protocol GICSPStreamProtocol <NSObject>

/**
 *  The delegate receives this message when a given event has occurred on a given stream.
 *  
 *  @param aStream   The stream on which streamEvent occurred.
 *  @param eventCode The stream event that occurred.
 */
-(void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode;

@end

/**
 *  GICSPStream is a CSP-transport level input stream implimentation. It hides specific ICX-internet protocol impliments over HTTP/HTTPS.
 */
@interface GICSPStream : NSObject

/**
 *  Stream protocol delegate.
 *  @see GICSPStreamProtocol protocol.
 */
@property id<GICSPStreamProtocol> delegate;

/**
 *  Whether we trust invalide certificates or not.
 *  @warning *Warning:* Use it just for DEBUG mode only.
 */
@property(nonatomic) BOOL validatesSecureCertificate;

/**
 *  Responsed HTTP status code.
 */
@property(nonatomic,readonly) NSInteger statusCode;

/**
 *  Responsed HTTP status string.
 */
@property(nonatomic,readonly) NSString* statusLine;

/**
 *  Responsed HTTP headers.
 */
@property(nonatomic,readonly) NSDictionary *responseHeaders;

/**
 *  Init Stream with NSURLRequest.
 *  
 *  @param aRequest NSURLRequest.
 *  
 *  @return GICSPStream instance.
 */
- (id) initWithURLRequest: (NSURLRequest*) aRequest;

/**
 *  Returns has any bytes available in stream. 
 *  
 *  @return YES if more then one byte is.
 */
- (BOOL)hasBytesAvailable;

/**
 *  Reads received data.
 *  
 *  @param error Error container wich is nill if ok, and some fail happend.
 *  
 *  @return data from server, or nill if nothing received.
 */
- (NSData*)readData: (GICSPError**)error;

/**
 *  Open stream.
 */
- (void) open;

/**
 *  Close stream.
 */
- (void) close;

/**
 *  Start thread to listen server connection.
 *  
 *  @param aRunLoop application RunLoop object.
 *  @param mode RunLoop mode.
 *  @see NSStream.
 */
- (void) scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode;

/**
 *  Stop thread to listen server connection.
 *  
 *  @param aRunLoop RunLoop object.
 *  @param mode  RunLoop mode.
 */
- (void) removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode;

/**
 *  Returns the receiver’s property for a given key.
 *  
 *  @param key The key for one of the receiver's properties.
 *  @see NSStream.
 *  @see setProperty:forKey:
 *
 *  @return The receiver’s property for the key key.
 */
- (id) propertyForKey:(NSString *)key;

/**
 *  Attempts to set the value of a given property of the receiver and returns a Boolean value that indicates whether the value is accepted by the receiver.
 *  
 *  @param property The value for key.
 *  @param key      The key for one of the receiver's properties.
 *  @see NSStream.
 *  @see propertyforKey:
 *  
 *  @return YES if the value is accepted by the receiver, otherwise NO
 */
- (BOOL) setProperty:(id)property forKey:(NSString *)key;

/**
 *  Returns the receiver’s status.
 *  
 *  @return The receiver’s status.
 */
- (NSStreamStatus) streamStatus;

/**
 *  Returns an NSError object representing the stream error.
 *  
 *  @return An NSError object representing the stream error, or nil if no error has been encountered.
 */
- (NSError*) streamError;

@end
