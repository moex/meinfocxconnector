//
//  GICSPChunkedData.m
//  GIConnector
//
//  Created by denn on 3/20/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GICSPChunkedData.h"
#import "GICSPStreamHeader.h"


//
// Chunked Raw data
//
@interface _GINSMutableData : NSObject
//
//data ssequential number
//
@property long long seqNum, chunkDefinedLength;

//
// if the chunk is downloaded completely
//
@property BOOL      isCompleted;
@end

@implementation _GINSMutableData
{
    NSMutableData *data;
}

@synthesize seqNum, chunkDefinedLength, isCompleted;

- (id) initWithData:(NSData *)aData{
    
    if (!(self=[super init])) {
        return nil;
    }
    
    data = [NSMutableData dataWithData:aData];
    
    isCompleted = YES;
    seqNum = -1;
    chunkDefinedLength = 0;
    
    return self;
}

- (void) appendData: (NSData*)aData{
    [data appendData:aData];
}

- (NSInteger) length{
    return data.length;
}

- (NSData*) getData{
    return data;
}

- (NSString*) description{
    NSString *_s = [[NSString alloc] initWithBytes:[data bytes] length: [data length] encoding: NSUTF8StringEncoding];
    return [NSString stringWithFormat:@"Is Completed: %i\nSeqNUM: %li\nDefined length:%li\nReal length: %lu\n----\n%@\n----\n", isCompleted, (long)seqNum, (long)chunkDefinedLength, (unsigned long)[data length], _s];
}

@end


//
// Data
//
@interface GICSPChunkedData ()
@property long long   currentSeqNum;
@end

@implementation GICSPChunkedData
{
    NSInteger        capacity;
    NSMutableArray  *dataChain;
}


- (NSString *) description{
    NSMutableString *_s = [[NSMutableString alloc] init];
    for (_GINSMutableData *d in dataChain) {
        [_s appendFormat:@"%@\n", d];
    }
    return _s;
}

- (NSInteger) length{
    NSInteger l=0;
    for (_GINSMutableData *d in dataChain) {
        l+=d.length;
    }
    return l;
}


- (NSInteger) count{
    return [dataChain count];
}

- (id) init{
    
    if(!(self=[super init]))
        return nil;
    
    self.currentSeqNum = -1;
    capacity = 0;
    dataChain = nil;
    
    return self;
}

- (void) flushIncompleted{
    _GINSMutableData *_last_chunk= [dataChain lastObject];
    if (!_last_chunk.isCompleted) {
        [dataChain removeLastObject];
    }
}

- (void) appendNewChunk:(NSData *)aData from:(NSInteger)position withLength:(long long) length seqnum:(long long)seqnum  andCompletedFlag:(BOOL)isComleted andDefinedLength: (long long) definedLength{
    _GINSMutableData *_chunk = [[_GINSMutableData alloc] initWithData:[aData subdataWithRange:NSMakeRange(position, (NSInteger)length)]];
    
    _chunk.seqNum = seqnum;
    _chunk.isCompleted = isComleted;
    _chunk.chunkDefinedLength = definedLength;
    
    if(isComleted)
        //
        // current completed seqnum
        //
        self.currentSeqNum = seqnum;
    
    [dataChain addObject:_chunk];
}

//
// Append data to chain
//
- (void)     appendData: (NSData*)aData error:(out GICSPError **)error{
    //@autoreleasepool {
        if (!aData || [aData length]==0) {
            return;
        }
        _GINSMutableData *_last_incomleted_chunk= [dataChain lastObject];
        
        if ([aData length]<=sizeof(CSPStreamPacketHeader)) {
            if ([aData length]<sizeof(CSPStreamPacketHeader)) {
                NSLog(@"Connection problem has occurred. Too short packet received.");
            }
            if (_last_incomleted_chunk.isCompleted)
                [self appendNewChunk:aData from:0  withLength:[aData length] seqnum:0 andCompletedFlag: NO andDefinedLength:0];
            return;
        }
        
        GICSPStreamHeader *_sheader = [[GICSPStreamHeader alloc] initWithData:aData];
        
        if(!dataChain)
            dataChain = [NSMutableArray arrayWithCapacity:0];
        
        if(error)
            *error = nil;
        
        if(_sheader.header_size!=GICSP_HEADER_SIZE){
            //
            // data is not completed
            // add to previous chunk and make new
            //
            
            if([dataChain count]==0){
                if (error) {
                    *error =[GICSPError errorWithCode: GICSP_ERROR_INCOMPLETED_CSP_PACKET
                                       andDescription:GILocalizedString(@"Check your network connection. The incompleted packet has occurred.", @"")];
                }
                return;
            }
            
            //
            // get the last chunk
            //
            
            NSUInteger _we_need_bytes   = (NSUInteger)_last_incomleted_chunk.chunkDefinedLength - (NSUInteger)[_last_incomleted_chunk length];
            NSUInteger _chunk_diff_size = [aData length] -  (NSUInteger)_we_need_bytes;
#if DEBUG
            NSLog(@"GICSPChunkedData NEXT chunck ---> %li <> %li <-- %lu ", (long)_we_need_bytes, (long)_chunk_diff_size, (unsigned long)[aData length]);
#endif
            
            if (_last_incomleted_chunk.isCompleted || _we_need_bytes==0){
                _last_incomleted_chunk.isCompleted = YES;
                return;
            }
            else if(_chunk_diff_size<=0){
                // just add
                
                [_last_incomleted_chunk appendData:aData];
                
                _last_incomleted_chunk.isCompleted = YES;
                self.currentSeqNum = _last_incomleted_chunk.seqNum;
                
                return;
            }
            else if (_chunk_diff_size>0){
                //
                // length of data > then we need, and we need (defined) - (what we have)
                //
                
                // just add the previous part to chunk
                
                [_last_incomleted_chunk appendData:[aData subdataWithRange:NSMakeRange(0, _we_need_bytes)]];
                _last_incomleted_chunk.isCompleted=YES;
                self.currentSeqNum = _last_incomleted_chunk.seqNum;
                
                //
                // next
                //
                NSMutableData *_nextData = [NSMutableData dataWithData:[aData subdataWithRange:NSMakeRange(_we_need_bytes, _chunk_diff_size)]];
                [self appendData:_nextData error:error];
                return;
            }
        }
        else{
            
            if(self.currentSeqNum>0 && self.currentSeqNum>=_sheader.seqnum)
                // we have those chunks
                return;
            
            NSInteger _chunk_diff_size = ([aData length] - (NSInteger)(sizeof(CSPStreamPacketHeader) + _sheader.length));
            
            if (_chunk_diff_size == 0) {
                
                // completed chunk
                [self appendNewChunk:aData from:(NSInteger)sizeof(CSPStreamPacketHeader)  withLength:_sheader.length seqnum:_sheader.seqnum andCompletedFlag: YES andDefinedLength:_sheader.length];
                
                return;
            }
            
            //
            // now we should process the follow obviously moments:
            // 1. arrived data can be less then we have in the header defined
            // 2. data can be longer then it defined in header
            //
            else if (_chunk_diff_size<0){
                //
                // in this case we just append incompeted chunk to chain
                //
                NSInteger _len = [aData length]-sizeof(CSPStreamPacketHeader);
                
                [self appendNewChunk:aData from:(NSInteger)sizeof(CSPStreamPacketHeader)  withLength:_len seqnum:_sheader.seqnum andCompletedFlag: NO andDefinedLength:_sheader.length];
                
                return;
            }
            else if (_chunk_diff_size>0){
                //
                // in this case we should append all chunks to chain
                //
                
                //
                // add competed data
                //
                //NSLog(@"3. new [%li:%li]", sizeof(CSPStreamPacketHeader), (long)_sheader.length) ;
                
                [self appendNewChunk:aData from:(NSInteger)sizeof(CSPStreamPacketHeader)  withLength:_sheader.length seqnum:_sheader.seqnum andCompletedFlag: YES andDefinedLength:_sheader.length];
                
                //
                // scan rest of buffer
                //
                NSInteger _from = (NSInteger)(_sheader.length+sizeof(CSPStreamPacketHeader));
                NSInteger _len = [aData length] - _from;
                
                NSData *_currentData = [NSData dataWithData:[aData subdataWithRange:NSMakeRange(_from, _len)]];
                //
                // use recursion patern (in this version) ##### i'm not like to use it, but i need a work solution #####
                //
                [self appendData:_currentData error:error];
                
                return;
            }
        }
    //}
}


//
// Get assembed data
//
- (NSData*) assembledData{
    if (!dataChain) {
        return nil;
    }
    NSMutableData *ret = [NSMutableData dataWithCapacity:capacity];
    for (_GINSMutableData *chunk in dataChain) {
        if ([chunk isCompleted]) {
            [ret appendData: [chunk getData]];
        }
    }
    return ret;
}

@end
