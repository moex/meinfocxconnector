//
//  GICSStreamHeader.m
//  GIConnector
//
//  Created by denn on 3/11/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GICSPStreamHeader.h"
#import "GIUtils.h"

@interface GICSPStreamHeader ()
@property(assign)     long long int header_size, seqnum, length;
@end

@implementation GICSPStreamHeader

@synthesize header_size, seqnum, length;

- (id) initWithCSPPacket: (CSPStreamPacketHeader)packet{
    
    if(!(self = [super init]))
        return nil;
    
    self.header_size = Ascii2int(packet.header_size, sizeof(packet.header_size));
    self.seqnum = Ascii2int(packet.seqnum, sizeof(packet.seqnum));
    self.length = Ascii2int(packet.length, sizeof(packet.length));
    
    return self;
}


- (id) initWithData:(NSData *)data{
    
    CSPStreamPacketHeader packet; 
    memcpy(&packet, [data bytes], sizeof(packet));
    
    return [self initWithCSPPacket:packet];
}

- (NSString*) description{
    return [NSString stringWithFormat:@"header_size=%lli: seqnum=%lli: length=%lli", self.header_size, self.seqnum, self.length];
}

@end
