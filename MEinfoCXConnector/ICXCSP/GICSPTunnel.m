
//
//  GICSPTunnel.m
//  GIConnector
//
//  Created by denn on 3/8/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GICSPTunnel.h"
#import "GICSPCommandQueue.h"


//
// CSP command (uri paths) should start with base server URL.
//

// Path to connection initialization url
# define CSP_CONNECT_PATH    @"connect"
//
// Receiver. Make connection to the path and wait for data which sends by server to the pipe asynchronously.
//
#define CSP_RECIEVE_PATH     @"%@/receive"
//
// Disconnect receiver.
// 
#define CSP_DISCONNECT_PATH  @"%@/disconnect"
//
// Just send packet to server.
//
#define CSP_SEND_PATH        @"%@/send"
//
// Send http header and wait for a replay.
// Use it for compute roundtrip delay.
//
#define CSP_PING_PATH        @"%@/ping"

//
// Connection states.
//
enum _csp_state
{
    cspREADYSTATE_INITIAL = 0, // initialize a connection 
    cspREADYSTATE_OPENING = 1, // try to recieved a session id
    cspREADYSTATE_OPEN    = 2, // the connection was opened successfully
    cspREADYSTATE_CLOSING = 3, // the connection is in the begining of closing process 
    cspREADYSTATE_CLOSED  = 4  // the connection has been closed
};
typedef enum _csp_state CSPSate;

enum _csp_receiver_state
{
    cspRECEIVER_INITIAL   = 0, // initialize a receiver but it is not ready to open yet
    cspRECEIVER_OPEN      = 1, // the receiver connection established
    cspRECEIVER_READY     = 2, // the receiver can be established
    cspRECEIVER_CLOSING   = 3, // the connection is closing
    cspRECEIVER_CLOSED    = 4  // the connection closed
};
typedef enum _csp_receiver_state CSPReceiverSate;

enum _csp_ping_state
{
    cspPING_SENT       = 1, // ping sent
    cspPING_REPLIED    = 2, // replied
};
typedef enum _csp_ping_state CSPPingSate;


//
// Close connection after data receives
//
@interface GICSPSender : GICSPCommandQueue
@end
@implementation GICSPSender
- (BOOL) isPersistent{return NO;}
@end

//
// Don't close connection after data receives
//
@interface GICSPReceiver : GICSPCommandQueue
@end
@implementation GICSPReceiver
- (BOOL) isPersistent{return YES;}
@end


@interface GICSPTunnel () <GICSPCommandHandler>

@property(atomic,assign)    NSTimeInterval  

//
// See description in .h file.
//
pingRoundTripTime, 

//
// Last time when ping-pong command has been sent.
//
pingLastTime;

@property NSString  *pingCommand, *connectCommand, *sendCommand, *receiveCommand, *disconnectCommand;
@property  GIConfig *config;



/** In our case it means RTD: round-trip-delay.
 It is the length of time it takes for a signal to be sent plus the length of time it takes for an acknowledgment of that signal to be received.
 This time delay therefore consists of the transmission times between the two points of a signal.
 We pursue two aims to run ping-pong and compute round-trip-time:
 1. server keeps our session ID if it knows the client which makes the connection is alive.
 2. client app can use this value to indicate a user, who uses the app, about network quality.
 */
@property(readonly)    NSUInteger      pingRefreshTime;

@end


@implementation GICSPTunnel
{
    //
    // Current connection state.
    // It uses to control stage which already has passed.
    //
    CSPSate    state;
    
    //
    // Receiver state.
    //
    CSPReceiverSate state_receiver;
    
    //
    // States of ping-pong signals.
    //
    CSPPingSate     state_ping;
    
    //
    // Session ID. 
    //
    NSString   *sid;
    
    //
    // Allowed headers response from remote server when connection makes.
    //
    NSString   *allowed_headers;
    
    //
    // Connection should send ping to server every:
    //
    NSTimeInterval   ping_refresh_period;
    
    //
    // Command connections object.
    // Connection object uses to send command and data.
    // After sending it closes connection immediately. Sending and receiving are async processes.
    // All data pending to the class delegate.
    //
    // Command object is a NSOperationQueue container. We put commands to queue and then they execute coherently,
    // but in the background context.
    //
    GICSPSender             *command;
    
    //
    // Receiver.
    // Long polling, longtime waiting connection to server.
    // When connection closed, it makes automated reconnect with the same sid but the last updated seqNum.
    //            
    GICSPReceiver           *receiver;
    
    
    //
    // Queue is used by tunnel instance to control operation consequecies.
    //
    NSOperationQueue        *self_queue;   
    
    //
    // Task queue.
    // We eval every user defined handler in background thread use GCD.
    //
    dispatch_queue_t  command_queue;
    dispatch_queue_t  receiver_queue;
    
    //
    // Connection retries before exiting.
    //
    int  retries;
        
    //
    // Self-control NSOperation protocol.
    //
    BOOL   _executing;
    BOOL   _finished;
}

@synthesize connectionHandler, pingRoundTripTime, pingLastTime, config;


- (BOOL)isExecuting{   return _executing; }

- (BOOL)isFinished{  return _finished; }


- (id) initWithGIConfig:(GIConfig *)userConfig{
    
    if (!(self = [super init]))	
		return nil;
    
    self.config = userConfig;
    
    state         = cspREADYSTATE_INITIAL;
    state_receiver = -1;
    
    //
    // set ping-pong period by default
    //
    _pingRefreshTime = ping_refresh_period  = self.config.readTimeout;
    
    // 
    // statistic
    //
    self.pingRoundTripTime = 0;
    
    self_queue = [NSOperationQueue new];
    
    command   = [[GICSPSender alloc] initFromConfig:self.config];
    command.handler = self;
    
    receiver  = [[GICSPReceiver alloc] initFromConfig:self.config];
    receiver.handler = self;
    
    self.connectCommand = [NSString stringWithFormat:@"%@", CSP_CONNECT_PATH];
    
    command_queue  = dispatch_queue_create("com.infocx.cspcommand.queue", DISPATCH_QUEUE_SERIAL);
    receiver_queue = dispatch_queue_create("com.infocx.cspreceiver.queue", DISPATCH_QUEUE_SERIAL);
    
    retries = 0;
    
    return self;
}

- (void) open{
    
    @synchronized (self){
        @try {
            if ([self isExecuting] || [self isConnected]) {
                NSLog(@"Tunnel already openned. %s:%i", __FILE__, __LINE__);
                return;
            }
            
            [self _connect];
            
            //
            // Start main loop async
            //
            
            [self_queue addOperation:self];    
        }
        @catch (NSException *exception) {
            NSLog(@"%@ %s:%i", exception, __FILE__, __LINE__);
        }
    }
}

- (BOOL) isOpened{
    return state_receiver == cspRECEIVER_OPEN;
}

- (BOOL) isClosed{
    return state == cspREADYSTATE_CLOSED;
}

- (BOOL) isConnected{
    return state == cspREADYSTATE_OPEN;
}

- (void) close{
    [receiver cancel];
}

- (void) restoreReceiver{
    [receiver relaunch];
}


- (void) send:(NSData *)data{
    [self _sendData:data];
}

- (void) sendString:(NSString *)buffer{
    [self _sendData: [buffer dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
}


- (void) cancel{
        
    if([self isCancelled] || [self isFinished]){
        return;
    }
    
    state = cspREADYSTATE_CLOSING;
    state_receiver = cspRECEIVER_CLOSING;
    
    [self _disConnect];
    
    //
    // Alert anyone that we are finished
    //
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    _executing = NO;
    _finished  = YES;        
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];   
    
    [super cancel];
    [self stopTunnel];
}

- (void) stopTunnel
{
    CFRunLoopStop(CFRunLoopGetCurrent());
}

- (void) ping:(NSTimer*)timer{
    
    if (!config.doPing) return;
    
    if (state == cspREADYSTATE_OPEN) {
        //
        // Execute the new ping-pong command when connection is created and last ping replay received
        //        
        state_ping = cspPING_SENT;        
        self.pingLastTime = [NSDate timeIntervalSinceReferenceDate];

        if (self.pingCommand == nil) {
            dispatch_async(command_queue, ^(void){
                [command execute: self.pingCommand];
            });
        }
    }
    
    double delayInSeconds = ping_refresh_period;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, receiver_queue, ^(void){
        [self ping:nil];
    });
}

- (void) main {
    
    [self willChangeValueForKey:@"isFinished"];
    _finished = NO;
    [self didChangeValueForKey:@"isFinished"];
    
    // needs to be KVO compliant
    [self willChangeValueForKey:@"isExecuting"];
    _executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    //
    // Run operation in background
    //
    CFRunLoopRun();
}

- (BOOL) isConcurrent { return NO;}

#pragma mark - GICSPunnel private messages


//
//
// Connection is opened
//
//
- (BOOL) cspExecutorOpened:(GICSPCommandExecutor *)csp{
    if ([csp.command isEqual:self.receiveCommand]) {
        state_receiver = cspRECEIVER_OPEN;
        // peer connection opened reset retries
        retries = 0 ;
        
        dispatch_async(command_queue, ^(void){
            [self.connectionHandler onOpen:nil];
        });
    }
    return NO;
}

- (BOOL) cspExecutor:(GICSPCommandExecutor *)csp isDataCompleted:(NSData *)chainedData{
    return [self.connectionHandler isDataCompleted:chainedData];
}


//
//
// DATA HANDLER - process command executions
//
//
- (BOOL) cspExecutor:(GICSPCommandExecutor *)csp didDataReceive:(NSData *)data withHeaders:(NSDictionary *)headers withStatus:(NSInteger)code{
    
    if (state == cspREADYSTATE_CLOSED) {
        return NO;
    }
    
    if ([csp.command isEqualToString:self.connectCommand]) {
        return [self _onConnectDone:headers withData:data];
    }
    else if ([csp.command isEqualToString:self.pingCommand]){
        
        state_ping = cspPING_REPLIED;
        self.pingRoundTripTime = [NSDate timeIntervalSinceReferenceDate] - self.pingLastTime;
        
        dispatch_async(command_queue, ^(void){   
            [self.connectionHandler onPing:data roundTrip:self.pingRoundTripTime];
        });
        
    }
    else if ([csp.command isEqual:self.disconnectCommand]){
        return [self _onDisConnectDone:headers withData:data];
    }
    else if ([csp.command isEqual:self.sendCommand]){
        return [self _onSendDone:headers withData:data];
    }
    else if ([csp.command isEqual:self.receiveCommand]){
        return [self _onReceivedDataAvailable:headers withData:data];
    }
    
    return NO;
}

//
// ERROR HANDLER
//

- (GITunnelCommand) whichCommand:(NSString*)commandName{
    
    //
    // пока тупо
    //
    
    if ([commandName isEqualToString:self.connectCommand]) 
        return GITunnel_CONNECT;
    
    else if ([commandName isEqualToString:self.pingCommand])
        return GITunnel_PING;
    
    else if ([commandName isEqualToString:self.disconnectCommand])
        return GITunnel_DISCONNECT;
    
    else if ([commandName isEqualToString:self.receiveCommand])
        return GITunnel_RECEIVE;
    
    return GITunnel_UNKNOWN;
}


- (void) stopOnHTTPGoneError:(GIError*)error withcommandName:(NSString*)commandName{
    state = cspREADYSTATE_CLOSED;
    state_receiver = cspRECEIVER_CLOSED;
    [self stopTunnel];
    dispatch_async(command_queue, ^(void){   
        [self.connectionHandler onError: error in:[self whichCommand:commandName]];
    });                       
}

- (BOOL) cspExecutor:(GICSPCommandExecutor *)csp onError:(GIError *)error{

    NSString *commandName = csp.command;
    NSData   *commandData = csp.postData;
    
    if (state == cspREADYSTATE_CLOSING) {
        if (
            [error code]==GISP_ERROR_HTTP_STATUS_SID_EXPIRED // if sid expired we should reconnect
            ) {
            [self stopOnHTTPGoneError:error withcommandName:commandName];
        }
        return NO;
    }
    
    if (state == cspREADYSTATE_CLOSED) {
        [self stopTunnel];
        return NO;
    }
    
    if (
        [error code]==GISP_ERROR_HTTP_STATUS_SID_EXPIRED // if sid expired we should reconnect
        ) {
        [self stopOnHTTPGoneError:error withcommandName:commandName];
        return NO;
    }
    else if ([commandName isEqualToString:self.pingCommand]){
        dispatch_async(command_queue, ^(void){
            [self.connectionHandler onError: error in:[self whichCommand:commandName]];
        });
        return NO;
    }
    else if ([commandName isEqualToString:self.sendCommand]){
        dispatch_async(command_queue, ^(void){
            if (commandData!=nil) {
                [self.connectionHandler onError: error in:[self whichCommand:commandName] withData:commandData];
            }
            else
                [self.connectionHandler onError: error in:[self whichCommand:commandName]];
        });
        return NO;
    }
    else if ([commandName isEqualToString:self.connectCommand] ||
        [commandName isEqual:self.sendCommand]
        ) {
        
        state = cspREADYSTATE_CLOSED;
        //
        // Reset ping 
        //
        self.pingCommand = nil;
        
        //
        // We try config.reconnectCount times new connection 
        // after config.reconnectTimeout
        //
        [command cancel];
        
        //
        // Increment try 
        //
        retries++;
        
        if (
            [error code] == GICSP_ERROR_CERT_INVALID       || 
            (
             [[error domain] hasSuffix:@".http.status"] &&         // if we response some http status, that means some wrong happanes with server
             [error code]!=GISP_ERROR_HTTP_STATUS_UNAVAILABLE      // if server temporary uanavailable we should reconnect
             // in the other cases pass error to next level
             ) ||
            [[error domain] hasPrefix: (__bridge NSString*) kCFErrorDomainCFNetwork]
            )
        {
            
            dispatch_async(command_queue, ^(void){   
                [self.connectionHandler onError: error in:[self whichCommand:commandName]];
            });           
            
            [self cancel];
            return NO;
        }
        else if (retries>self.config.reconnectCount) {
            
            dispatch_async(command_queue, ^(void){   
                [self.connectionHandler onError: [GICSPError errorWithCode: GICSP_ERROR_RETRIES_LIMIT 
                                                            andDescription: GILocalizedString(@"Reconnection retries has exhausted.", @"Reconections limit")]
                                             in:[self whichCommand:commandName]
                 ];
            });           
            
            //
            // Stop sel operation
            //
            [self cancel];
            return NO;
        }
        
        //
        // Recreate connection command chanel after waiting
        //
        sleep((unsigned int)self.config.reconnectTimeout);
        command   = [[GICSPSender alloc] initFromConfig:self.config];
        command.handler = self;
        
        [self _connect];        
    }
    else if ([commandName isEqual:self.receiveCommand]){
        
        if ([error code] == GICSP_ERROR_RECEIVER_CLOSED) {
            state_receiver = cspRECEIVER_CLOSED;
            
            retries++;
            
            [receiver cancel];
            
            if(retries>self.config.reconnectCount){
                [self cancel];
                dispatch_async(command_queue, ^(void){
                    [self.connectionHandler onError: [GICSPError errorWithCode: GICSP_ERROR_RETRIES_LIMIT
                                                                andDescription: GILocalizedString(@"Reconnection retries has exhausted.", @"Reconections limit")]
                                                 in:[self whichCommand:commandName]
                     ];
                });
                return NO;
            }
            
            sleep((unsigned int)self.config.reconnectTimeout);
            
            dispatch_async(receiver_queue, ^(void){
                [receiver relaunch];
            });
            
        }
        else if ([[error domain] hasSuffix:@"http.status"]){
            //
            // Session has been closed
            //
            state = cspREADYSTATE_CLOSED;
            state_receiver = cspRECEIVER_CLOSED;
            
            [self _connect];
        }
        else{
            //
            // unknown error
            //
            state = cspREADYSTATE_CLOSED;
            state_receiver = cspRECEIVER_CLOSED;
            [receiver cancel];
            [self cancel];
            
            dispatch_async(command_queue, ^(void){
                [self.connectionHandler onError: error in:[self whichCommand:commandName]];
            });
            return NO;
        }
    }
    dispatch_async(command_queue, ^(void){
        [self.connectionHandler onError: error in:[self whichCommand:commandName]];
    });
    return NO;
}



//
//
// CONNECTION
//
//
- (void) _connect{
    state = cspREADYSTATE_OPENING;    
    dispatch_async(command_queue, ^(void){
        [command execute: self.connectCommand];
    });
}


-(BOOL) _onConnectDone: (NSDictionary *)headers withData: (NSData *) data{    
    //
    // set session parapmeters such as session id and ping refresh time
    //
    
#if DEBUG
    NSLog(@"onConnect: %@", headers);
#endif
    
    allowed_headers      = [headers[@"Access-Control-Expose-Headers"] description];
    sid                  = [headers[@"X-CspHub-Session"] description];
    _pingRefreshTime = ping_refresh_period  = [headers[@"X-CspHub-Ping"] integerValue];
    
    if (!sid){
        //
        // Connection could not establishe
        //
        [self.connectionHandler onError: [GICSPError errorWithCode:GICSP_ERROR_SID_IS_NULL andDescription: 
                                          GILocalizedString(@"Session id has not received.", @"NULL session id")]
                                     in: GITunnel_DISCONNECT
         ];
        state = cspREADYSTATE_CLOSED;
        return NO;
    }
    
    //
    // Pass processing to onOpen asynchronously
    //
    dispatch_async(command_queue, ^(void){   
        [self.connectionHandler onConnect:data];
    });
    
    self.pingCommand    = [NSString stringWithFormat:CSP_PING_PATH,sid];
    self.sendCommand    = [NSString stringWithFormat:CSP_SEND_PATH,sid];
    self.receiveCommand = [NSString stringWithFormat:CSP_RECIEVE_PATH,sid];
    self.disconnectCommand = [NSString stringWithFormat:CSP_DISCONNECT_PATH,sid];
    
    //
    // Reset retries
    //
    retries = 0;
    
    //
    // try connect to receiver
    //
    state = cspREADYSTATE_OPEN;
    [self _receiverOpen];
    
    [self ping:nil];

    return NO;
}


//
//
// DISCONNECT
//
//
- (void) _disConnect{
    
    if (! self.disconnectCommand /*|| state == cspREADYSTATE_CLOSED */) {
        //
        // Nothing to disconnect
        //
        return;
    }
    
    state = cspREADYSTATE_CLOSING;    
    
    //
    // stop ping pong
    //
    self.pingCommand = nil;
    
    [command executeAndWait: self.disconnectCommand];
}

-(BOOL) _onDisConnectDone: (NSDictionary *)headers withData: (NSData *) data{
    
    if (state == cspREADYSTATE_CLOSED)
        return NO;
    
    //
    // Pass processing to onClose asynchronously
    //
    dispatch_async(command_queue, ^(void){   
        [self.connectionHandler onClose:data];
    });
    
    state = cspREADYSTATE_CLOSED;
    state_receiver = cspRECEIVER_CLOSED;
    
    [command cancel];
    [receiver cancel];
    
    return NO;
}


//
//
// SEND
//
//
- (void) _sendData: (NSData *) data{
    
    if (state_receiver!=cspRECEIVER_OPEN || !self.sendCommand) {
        //
        // Nothing to send
        //
        dispatch_async(command_queue, ^(void){
            [self.connectionHandler onError: [GICSPError errorWithCode:GICSP_ERROR_NOT_OPENED andDescription:
                                              GILocalizedString(@"Receiver is not opened yet, or it has been closed by server.", @"Receiver not opened/closed") ]
                                         in: GITunnel_SEND
             ];
        });        
        return;
    }
    
    [command execute: self.sendCommand withData:data];
}

-(BOOL) _onSendDone: (NSDictionary *)headers withData: (NSData *) data{
    //
    // Pass processing to onClose asynchronously
    //
    dispatch_async(command_queue, ^(void){   
        [self.connectionHandler onSend:data];
    });
    
    return NO;
}


//
//
//  RECIVEIR
//
//
- (void) _receiverOpen{
    
    if (state!=cspREADYSTATE_OPEN || !self.receiveCommand) {
        //
        // Nothing to create
        //
        return;
    }
    
    state_receiver = cspRECEIVER_INITIAL;
    dispatch_async(receiver_queue, ^(void){
        [receiver execute: self.receiveCommand];
        state_receiver = cspRECEIVER_READY;
    });    
}

-(BOOL) _onReceivedDataAvailable: (NSDictionary *)headers withData: (NSData *) data{
    //
    // Pass processing to onRead data asynchronously
    //
    dispatch_async(receiver_queue, ^(void){   
        [self.connectionHandler onRead:data];
    });
    
    return YES;
}

@end
