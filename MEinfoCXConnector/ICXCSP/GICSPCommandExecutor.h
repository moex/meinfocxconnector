//
//  GICSPCommandExecutor.h
//  GIConnector
//
//  Created by denn on 3/13/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <zlib.h>
#import <security/SecureTransport.h>

#import "GIConfig.h"

#import "GICSPError.h"
#import "GICSPStreamHeader.h"
#import "GICSPChunkedData.h"

@class GICSPCommandExecutor;

//
// Default thread check period, we use it as a watch dog timer.
// Mesured in usec.
//
#define CSP_MAIN_LOOP_TIMEOUT   1000
//
// "Inifint" timeout is used to process data streaming.
//
#define CSP_INIFINT_STREAM_TIMEOUT (3600.0*24.0)

//
// Command options protocol tell concrete instance of GICSPCommand.
// It keeps connection context or close connection immediately when command has ben sent.
// It does not mean that instance is destroyed when connection closes, this means
// that command has just sent, connection closed and we are wait for receiving data.
// So, if isPersistent returns YES, GICSPCommand instance keeps connection if NO connection 
// closes after any data receives.
//
@protocol GICSPCommandOptions <NSObject>
@optional

//
// It returns YES or NO to indicate which type of connection we uses.
//
- (BOOL) isPersistent;

@end

//
// Command handler is a methods delagator which handle command responses.
// Behavior depends on CommandOptions. Every time when data recived or error occurre.d
//
@protocol GICSPCommandHandler <NSObject>

//
// Callback if error has occured.
// Return YES if we want to continue connection or revoke command.
//
- (BOOL) cspExecutor:(GICSPCommandExecutor*)csp onError:(GIError*)error;

//
// It calls when connection opened successfully.
//
- (BOOL)  cspExecutorOpened:(GICSPCommandExecutor*)csp ;

//
// Callback after data received and they are completed. 
// Return YES if we want to continue connection.
//
- (BOOL) cspExecutor:(GICSPCommandExecutor*)csp didDataReceive:(NSData*)data withHeaders:(NSDictionary*)headers withStatus:(NSInteger)code;

//
// is data completed
//
- (BOOL) cspExecutor:(GICSPCommandExecutor*)csp isDataCompleted: (NSData*)chainedData;
@end


//
// Execute queued command.
//
@interface GICSPCommandExecutor : NSOperation

@property(nonatomic) GIConfig                *config;

//
// Calback handlers.
//
@property(nonatomic) id<GICSPCommandHandler> handler;

//
// Command options is set to tell executor how make connection.
// There are two options the process have: request and polling (persistent connection).
//
@property(nonatomic) id<GICSPCommandOptions> options;

//
// Chunked data which we use to keep track of packets
// when all packet received we join them and pass to top level
//
@property(nonatomic,assign)     NSInteger             seqnum;
@property(nonatomic,strong)     GICSPChunkedData     *completedData;

//
// Current CSP command.
//
@property(readonly)     NSString*   command;

//
// Reference to data that we tried send to server.
//
@property(readonly)     NSData*     postData;

- (id) initCommand:(NSString*)aCommand withData:(NSData*)data andHandler: (id<GICSPCommandHandler>)aHandler andConfig: (GIConfig*)aConfig;

@end

