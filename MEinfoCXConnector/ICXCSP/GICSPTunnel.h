//
//  GICSPTunnel.h
//  GIConnector
//
//  Created by denn on 3/8/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GITunnelHandler.h"
#import "GITunnelProtocol.h"
#import "GICSPError.h"
#import "GIConfig.h"


/**
 Create the CSP tunnel, establish a connection and start async reader connection.
 Send a message if you want.
 Receive some messages and eval handler method if some event happens.


 The base class controls low-level behavior of connection. The first version based on CSP - Comet Session Protocol.
 Next release should support websockets. 

 At the level of abstraction we just handle connection commands, events and receive some data which server replies asynchronously when one receives 
 client command. According to our GI-CSP extention specs we use 4 types of commands:
 1. connect - get SESSION ID
 2. ping    - check connection and force the server keep our session id
 3. send    - send POST data, at the level it undefines what we send to server
 4. receive - persistent connection, in technical terms, if we use CSP approach, it means long-polling connection we sniff this connection and waitnig for any data which can receive from server. Received data has special structer: [header:rawdata]. If data header or the data are corrupted, receiver makes a new reconnection with last sequence number of data and try to reread currupted data. After success we pass these data to the application businnes-level.

*/
@interface GICSPTunnel : NSOperation <GITunnelProtocol>

/**
 *  Create tunnel instance.
 *  
 *  @param userConfig user configuration.
 *  
 *  @return tunnel instance.
 */
- (id)   initWithGIConfig: (GIConfig *) userConfig;

@end
