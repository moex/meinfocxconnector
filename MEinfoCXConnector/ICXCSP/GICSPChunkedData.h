//
//  GICSPChunkedData.h
//  GIConnector
//
//  Created by denn on 3/20/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GICSPError.h"

@interface GICSPChunkedData : NSObject

@property(readonly) long long currentSeqNum;

- (id) init;

//
// Append data to chain
//
- (void)     appendData: (NSData*)aData error:(out GICSPError **) error;

//
// Get assembed data
//
- (NSData*) assembledData;

//
//
//
- (NSInteger) length;

//
//
//
- (NSInteger) count;

//
// remove incompeted
//
- (void) flushIncompleted;

@end
