//
//  GICSPCommand.h
//  GIConnector
//
//  Created by denn on 3/12/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GICSPCommandExecutor.h"
#import "GIError.h"
#import "GIConfig.h"

//
// Command (operation) QUEUE container. Put command to operation queue, execute and manage.
//
// Send command to HTTP connection, wait replay and pass data to the next level of process.
// Command can be disposable without saving the connection context and the persistent.
// Persistent serves long polling stream from server.
//
// In both options instance object pass received data to handler object asynchronously.
// In both options command lives as a backgroud event-driven operation object.
// We can cancel command operation at all, suspend or resume. 
// 
// Cancelled command operation destroy all connections and should be recreated again.
//
@interface GICSPCommandQueue : NSObject <GICSPCommandOptions>

//
// Init instance of command
//
- (id) initFromConfig: (GIConfig*)config;

//
// Execute command
//
- (void) execute:(NSString*)name;

//
// Execute command with POST data
//
- (void) execute:(NSString*)command withData:(NSData*)data;

//
// Execute command and wait 
//
- (void) executeAndWait:(NSString*)command;

//
// Relaunch command execution
//
- (void) relaunch;

//
// Cancel all executing operation
//
- (void) cancel;

//
// Command handler
//
@property id<GICSPCommandHandler> handler;

@property (nonatomic) NSInteger seqnum;

@end
