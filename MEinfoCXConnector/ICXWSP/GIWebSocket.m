//
//  GIWebSocket.m
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 01/04/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "GIWebSocket.h"

@interface SRWebSocket (GIWebSocket)
- (void) handlePong;
- (void) handlePing:(NSData *)pingData;
@end

@interface GIWebSocket ()
@end

@implementation GIWebSocket

- (void) handlePing:(NSData *)pingData{
    [super handlePing:pingData];
    if (self.handler) {
        [self.handler websocket:self onPing:pingData];
    }
}

- (void) handlePong{
    [super handlePong];
}

@end
