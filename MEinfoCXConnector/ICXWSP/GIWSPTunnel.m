//
//  GIWSPTunnel.m
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 31.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "GIWSPTunnel.h"
#import "GIWebSocket.h"
#import "GICSPChunkedData.h"

@interface GIWSPTunnel () <SRWebSocketDelegate,GITunnelProtocol, GIWebSocketHandler>
@property (nonatomic,strong) GIWebSocket *webSocket;
@property (nonatomic,readonly) GIConfig *config;
@property (nonatomic,strong) GICSPChunkedData *completedData;
@property (nonatomic,assign) dispatch_queue_t webSocketQueue;
@property (nonatomic,assign) dispatch_queue_t streamQueue;
@property (nonatomic,assign) BOOL isConnected;
@property (nonatomic,readonly) NSString *connectionSid;
@property (nonatomic,readonly) NSURL *connectionUrl;
@end

@implementation GIWSPTunnel
{
    NSInteger   seqnum;
    NSTimer     *connectionTimeoutTimer;
    NSUInteger  reconnections;
}

- (id) initWithGIConfig:(GIConfig *)userConfig{
    self = [super init];
    
    if (self) {
        _config = userConfig;
    }
    
    return self;
}

@synthesize webSocket=_webSocket, config=_config, isOpened=_isOpened, isClosed=_isClosed, connectionHandler=_connectionHandler;

- (dispatch_queue_t) webSocketQueue{
    if (_webSocketQueue) {
        return _webSocketQueue;
    }
    _webSocketQueue =  dispatch_queue_create("com.infocx.wsptunnel.websocket", DISPATCH_QUEUE_SERIAL);
    return _webSocketQueue;
}


-(dispatch_queue_t) streamQueue{
    if (_streamQueue) {
        return _streamQueue;
    }
    
    _streamQueue = dispatch_queue_create("com.infocx.wsptunnel.stream", DISPATCH_QUEUE_SERIAL);
    
    return _streamQueue;
}

- (NSURL*) connectionUrl{
    if (self.connectionSid) {
        return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%li",[self.config.url absoluteString],self.connectionSid,(long)seqnum]];
    }
    return self.config.url;
}

- (GIWebSocket*) webSocket{
    
    if (_webSocket) {
        return _webSocket;
    }
    
    _webSocket = [[GIWebSocket alloc] initWithURL:self.connectionUrl];
    _webSocket.delegate = self;
    _webSocket.handler = self;
    
    return _webSocket;
}

- (void) stopConnectionByTimer: (NSTimer*) timer
{
    [self invalidateConnectionTimer];
    [self.webSocket close];
    
    _webSocket = nil;
    
    GICSPError *error;
    if (reconnections>=self.config.reconnectCount) {
        error = [GICSPError errorWithCode: GICSP_ERROR_RETRIES_LIMIT
                           andDescription: GILocalizedString(@"Reconnection retries has exhausted.", @"Reconections limit")];
    }
    else{
        error=[GICSPError errorWithCode:GICSP_ERROR_CONNECTION_TIMEOUT
                         andDescription:GILocalizedString(@"The CSP connection couldn’t be completed. Operation timed out.", @"CSP connection timed out")];
        
        [self reopen];
    }
    
    [self.connectionHandler onError:error in:GITunnel_CONNECT];
}

- (void) startCheckConnectionTimer:(time_t)timeout
{
    if(timeout){

        [self invalidateConnectionTimer];
        
        connectionTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: (NSTimeInterval)timeout
                                                                  target: self
                                                                selector: @selector(stopConnectionByTimer:)
                                                                userInfo: nil
                                                                 repeats: NO];
    }
}

- (void) invalidateConnectionTimer{
    if (connectionTimeoutTimer) {
        [connectionTimeoutTimer invalidate];
        connectionTimeoutTimer = nil;
    }
}

- (void) reopen{
    self.isConnected = NO;
    
    [self invalidateConnectionTimer];
    
    if (reconnections>=self.config.reconnectCount) {
        return;
    }
    
    [self startCheckConnectionTimer:self.config.connectionTimeout];
    
    reconnections ++;
    [self.webSocket open];
}

#pragma mark - Tunnel

- (void) open{
    seqnum = 0;
    [self reopen];
    [self.connectionHandler onConnect:nil];
}

- (void) close{
    
    self.isConnected = NO;
    seqnum = 0;
    
    [connectionTimeoutTimer invalidate];
    
    [self.webSocket close];
    _webSocket.delegate = nil;
    _webSocket = nil;
    
    if (self.webSocket.readyState==SR_OPEN && reconnections<self.config.reconnectCount) {
        [self.connectionHandler onClose:nil];
    }
    
}

- (void) send:(NSData *)data{
    if (self.isConnected) {
        [self.webSocket send:data];
    }
    else if (self.connectionHandler){
        [self.connectionHandler onError:[GICSPError errorWithCode:GICSP_ERROR_NOT_OPENED andDescription:
                                         GILocalizedString(@"Receiver is not opened yet, or it has been closed by server.", @"Receiver not opened/closed") ]
                                     in:GITunnel_SEND
                               withData:data];
        [self.connectionHandler onSend:[@"OK" dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) sendString:(NSString *)buffer{
    [self send:[buffer dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
}

- (BOOL) isClosed{
    return self.webSocket.readyState==SR_CLOSED;
}

- (BOOL) isOpened{
    return self.webSocket.readyState==SR_OPEN;
}

#pragma mark - GIWebSocket

- (void) websocket:(GIWebSocket *)webSocket onPing:(NSData *)data{
    
    if (webSocket!=_webSocket || _webSocket==nil) {
        return;
    }
    
    NSError *error;
    NSDictionary *session_dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                                   error:&error];
    if (session_dict != nil) {
        NSNumber *roundtrip = [session_dict objectForKey:@"X-CspHub-Roundtrip"];
        if (roundtrip) {
            [self.connectionHandler onPing:data roundTrip:[roundtrip floatValue]/1000.];
            return;
        }
    }
    
    GICSPError *_error=[GICSPError errorWithCode: GICSP_ERROR_INCOMPLETED_PING
                                  andDescription: error.localizedDescription];
    [self.connectionHandler onError:_error in:GITunnel_PING];
}

#pragma mark - SRWebSocket

- (void) webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
    
    if (webSocket!=_webSocket || _webSocket==nil) {
        return;
    }
    
    if (code == 0) {
        //
        // tcp connection termiated
        //
        if (!wasClean && _webSocket) {
            [_webSocket close];
        }
        else
            [self reopen];
    }
    else{
        GICSPError *error =  [GICSPError errorWithCode:GICSP_ERROR_RECEIVER_CLOSED
                                        andDescription:[NSString stringWithFormat:
                                                        reason, self.config.url]];
        [self.connectionHandler onError:error in:GITunnel_RECEIVE];
    }
    
}

- (void) webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    
    if (webSocket!=_webSocket || _webSocket==nil) {
        return;
    }
    
    void (^_error_block)(void) = ^(void){
        
        if ([error code] <= errSSLProtocol) {
            
            GIError *_error = [GICSPError errorWithCode:GICSP_ERROR_CERT_INVALID
                                         andDescription:[NSString stringWithFormat:
                                                         GILocalizedString(@"%@ certificate is not trusted for the receiver peer connection.", @"Bad certificate"), [self.config.serverUrl host]]];
            [self.connectionHandler onError:_error in:GITunnel_RECEIVE];
        }
        else{
            GICSPError *_error = [GICSPError errorWithCode:GICSP_ERROR_CONNECTION_FAIL
                                            andDescription:error.localizedDescription];
            [self.connectionHandler onError:_error in:GITunnel_RECEIVE];
        }};
    _error_block();
}

- (void) webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    @autoreleasepool {
        if (webSocket!=_webSocket || _webSocket==nil) {
            return;
        }
        
        if (!self.isConnected) {
            
            self.isConnected = YES;
            
            if ([message isKindOfClass:[NSString class]]) {
                NSError *error;
                NSDictionary *session_dict = [NSJSONSerialization JSONObjectWithData:[message dataUsingEncoding: NSUTF8StringEncoding allowLossyConversion:YES]
                                                                             options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                                               error:&error];
                if (session_dict != nil) {
                    _connectionSid = [session_dict objectForKey:@"X-CspHub-Session"];
                }
            }
            
            if (self.connectionSid==nil) {
                GIError *error=[GICSPError errorWithCode:GICSP_ERROR_SID_IS_NULL andDescription:
                                GILocalizedString(@"Session id has not received.", @"NULL session id")];
                [self.connectionHandler onError:error in:GITunnel_CONNECT];
            }
            else  {
                
                reconnections = 0;
                
                [self.connectionHandler onOpen:nil];
            }
        }
        else{
            if ([message isKindOfClass:[NSData class]]) {
                if (!self.completedData) {
                    self.completedData = [[GICSPChunkedData alloc] init];
                }
                
                GICSPError *error;
                [self.completedData appendData:message error:&error];
                
                seqnum = (NSInteger)self.completedData.currentSeqNum;
                
                
                if (error) {
                    NSLog(@"%@ %s:%i", error, __FILE__, __LINE__);
                }
                
                if ([self.connectionHandler isDataCompleted:message]) {
                    //
                    // When all data has received and the end of data is marked -
                    // call handler
                    //
                    
                    NSData *_d =[[self.completedData assembledData] copy];
                    [self.connectionHandler onRead:_d];
                    
                    //
                    // keep to reconnect
                    //
                    if (seqnum<self.completedData.currentSeqNum) {
                        seqnum = (NSInteger)self.completedData.currentSeqNum;
                    }
                    
                    self.completedData = nil;
                }
            }
        }
    }
}

- (void) webSocketDidOpen:(SRWebSocket *)webSocket{
    [self invalidateConnectionTimer];
}

@end
