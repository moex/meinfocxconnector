//
//  GIWebSocket.h
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 01/04/14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#include <SocketRocket.h>

@class GIWebSocket;

@protocol GIWebSocketHandler <NSObject>
- (void) websocket:(GIWebSocket*)webSocket onPing:(NSData*)data;
@end

@interface GIWebSocket : SRWebSocket
@property (nonatomic,strong) id<GIWebSocketHandler> handler;
@end
