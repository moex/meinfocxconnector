//
//  GIWSPTunnel.h
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 31.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <SocketRocket.h>

#import "GITunnelProtocol.h"
#import "GIConfig.h"

/**
 Create the WSP tunnel, establish a connection and start async reader connection.
 Send a message if you want.
 Receive some messages and eval handler method if some event happens.
 
 The base class controls low-level behavior of connection. The first version based on WSP - websocket protocol.
 
 At the level of abstraction we just handle connection commands, events and receive some data which server replies asynchronously when one receives
 client command. According to our GI-CSP extention specs we use 4 types of commands:
 1. <root>/websocket - create connection and get session id (sid).
 2. ping-pong with server
 3. send command
 4. recovering closed connection with <root>/websocket/<sid>/<seqnum>
 
 See SocketRocket which i use to implement infoCX tunnel layer.
 
 */
@interface GIWSPTunnel : NSObject <GITunnelProtocol>
/**
 *  Create tunnel instance.
 *
 *  @param userConfig user configuration.
 *
 *  @return tunnel instance.
 */
- (id)   initWithGIConfig: (GIConfig *) userConfig;
@end
