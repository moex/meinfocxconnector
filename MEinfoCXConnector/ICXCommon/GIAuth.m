//
//  GIAuth.m
//  GIConnector
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Security/Security.h>
#import "GIAuth.h"

@interface GIAuth ()

@property time_t lastTime;

@end

@implementation GIAuth

@synthesize user=_user, password=_password;
@synthesize lastTime;

- (id) copyWithZone:(NSZone *)zone{
    GIAuth *_new_obj = [[[self class] allocWithZone:zone] init];
    if (_new_obj) {
        _new_obj->_password = [self->_password copyWithZone:zone];
        _new_obj->_user = [self->_user copyWithZone:zone];
        _new_obj->lastTime = self->lastTime;
        _new_obj->_domain = [self->_domain copyWithZone:zone];
    }
    return _new_obj;
}

- (id) initWithUser:(NSString *)usr andPassword:(NSString *)pas{
    return [self initWithUser:usr withPassword:pas andDomain:GIC_DEFAULT_DOMAIN];
}

- (id) initWithUser:(NSString *)userid withPassword:(NSString *)password andDomain:(NSString *)domain{
    
    if(!(self = [super init]))
        return nil;
    
    _user = userid;
    _password = password;
    self.lastTime  = time(nil);
    _domain = domain;
    
    return self;
}

- (id) init{
    
    if(!(self = [super init]))
        return nil;
    
    self.user = GIC_GUEST_USER;
    self.password = GIC_GUEST_PASSWD;
    self.domain = GIC_GUEST_DOMAIN;
    
    return self;    
}

- (void) setUser:(NSString *)user{
    _user = user;
    self.lastTime  = time(nil); 
}

- (void)setPassword:(NSString *)password{
    _password = password;
    self.lastTime  = time(nil); 
}

- (NSString*) description{
    return [NSString stringWithFormat:@"GIAuth login: %@, Password: %@, Last Access time: %ld, Domain: %@", _user, _password, lastTime, _domain];
}

- (NSDictionary*) searchOptions:(NSString *) domain{
    return [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword),  kSecClass, domain, kSecAttrServer, kCFBooleanTrue, kSecReturnAttributes, kCFBooleanTrue, kSecReturnData, nil];
}

- (void) saveInKeyChain:(NSString*) URL{
        
    NSDictionary *search = [self searchOptions:URL];
    
    // Remove any old values from the keychain
    SecItemDelete((__bridge CFDictionaryRef) search);
    
    // Create dictionary of parameters to add
    NSData* passwordData = [self.password dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)(kSecClassInternetPassword), kSecClass, URL, kSecAttrServer, passwordData, kSecValueData, self.user, kSecAttrAccount, self.domain, kSecAttrSecurityDomain, nil];
    
    // Try to save to keychain
    SecItemAdd((__bridge CFDictionaryRef) dict, NULL);
}

- (OSStatus) restoreFromKeyChain:(NSString*) URL{
    
    // Create dictionary of search parameters
    NSDictionary* search = [self searchOptions:URL];
    
    // Look up server in the keychain
    CFTypeRef result = nil;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef) search, &result);

    NSDictionary *found = (__bridge NSDictionary *)(result);

    if (!result) return err;
    
    // Found
    self.user     = (NSString*) [found objectForKey:(__bridge id)(kSecAttrAccount)];
    self.domain     = (NSString*) [found objectForKey:(__bridge id)(kSecAttrSecurityDomain)];
    self.password = [[NSString alloc] initWithData:[found objectForKey:(__bridge id)(kSecValueData)] encoding:NSUTF8StringEncoding];
    
    return err;
}

@end
