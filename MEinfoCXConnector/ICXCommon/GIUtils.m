//
//  GIUtils.m
//  GIConnector
//
//  Created by denn on 5/3/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIUtils.h"

BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || (thing == [NSNull null])
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}


int  Ascii2int(char* buffer, int len) {
    int result = 0;
    
    for (int i = 0; i < len; i++)
    {
        char ch = toupper(buffer[i]) - '0';
        if (ch > 9)
            ch += '0' - 'A' + 10;
        
        result = result * 16 + ch;
    }
    
    return result;
}
