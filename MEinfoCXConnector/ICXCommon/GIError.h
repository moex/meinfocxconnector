//
//  GIError.h
//  GIConnector
//
//  Created by denn on 3/4/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIBundle.h"

//
// goInvest error domain.
//

#define GI_ERROR_DOMAIN_PREFIX @"com.micex.infocx"

//
//

/**
 
 Error container.
 We just place an error code and the error description to inform something who will handle a replied data.

 */
@interface GIError : NSError

/**
 *  Init from NSError.
 *  
 *  @param error NSError object.
 *  
 *  @return GIError instance.
 */
- (id) initWithNSError: (NSError*)error;

@end

