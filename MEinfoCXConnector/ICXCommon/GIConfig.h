//
//  GIConfig.h
//  GIConnector
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIAuth.h"

typedef enum{
    GI_TUNNEL_CSP, // CSP (commet session protocol)
    GI_TUNNEL_WSP  // web sockets
} GITunelType;

//
// 
//

/** Default connection settings.
 *  
 */
#define GI_CONNECTION_TIMEOUT 10  /** stop a connection if it established but not completed */
#define GI_RECONNECT_COUNT    3   // reconnection retries if connection could not be established, refused for example
#define GI_RECONNECT_TIMEOUT  3   // the time out between connection retries if it could not be established, refused for example
#define GI_READ_TIMEOUT       10  // a connection estableshed but data could not be received after the timeout
#define GI_PERSIST_TIMEOUT    0   // a connection established, but data had received after the timeout
                                  // 0 - means infinit waitnig
#define GI_WRITE_TIMEOUT      10  // a connection established, data has been sent but the operation could not be completed

#define GI_MP_MARKETID @"id"
#define GI_MP_CHANNELID @"id"



/** GIConfig contains connection instance preferences, such as:
 
 - goInvest server url
 - login
 - password
 - timeouts options
 - etc...
 
 */
@interface GIConfig : NSObject 

/** Authentication container.
 *
 * @see GIAuth class.
 *
 */
@property(nonatomic,strong)   GIAuth  *login;

/**
 *  Reconnection count means how many times Connector would be try to establish new connection if 
 *  a attempt is failed.
 */
@property(nonatomic, assign) int       reconnectCount;

/**
 *  Time to stop hanged connection.
 */
@property(nonatomic, assign) time_t    connectionTimeout;

/**
 *  Wait for the time to try next connection.
 */
@property(nonatomic, assign) time_t    reconnectTimeout;

/**
 *  Time to stop reading from hanged connection.
 */
@property(nonatomic, assign) time_t     readTimeout;

/**
 *  Time to close persistent connection after time point when connection has been established.
 */
@property(nonatomic, assign) time_t     persistTimeout;

/**
 *  Time to stop writing from hanged connection.
 */
@property(nonatomic, assign) time_t     writeTimeout;

/**
 *  Lounch periodicaly ping action if it is YES.
 */
@property(nonatomic, assign) BOOL      doPing;

/**
 *  Whether we trust invalide certificates or not.
 *  @warning *Warning:* Use it just for DEBUG mode only.
 */
@property (nonatomic, assign) BOOL    validatesSecureCertificate;

/**
 *  Current application language.
 */
@property(nonatomic,strong) NSString *lang;

@property(nonatomic,readonly) GITunelType tunnelType;

/**
 *  Init with an url.
 *  
 *  @param url URL string.
 *  
 *  @return GIConfig instance.
 */
- (id) initWithUrl: (NSString *)url;

/**
 *  Change server URL.
 *  
 *  @param url URL string
 */
- (void)   setServerUrl:(NSString *)url;

/**
 *  Get the current server URL.
 *  
 *  @return NSURL object.
 */
- (NSURL*) serverUrl;

@property (nonatomic,readonly) NSURL* url;

@end
