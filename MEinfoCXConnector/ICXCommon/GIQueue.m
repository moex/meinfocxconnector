//
//  GIQueue.m
//  MEinfoCXConnector
//
//  Created by denis svinarchuk on 21.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import "GIQueue.h"

@implementation GIQueue
{
    NSMutableArray *queue;
    NSUInteger      depth;
}

- (id) init{
    self = [super init];
    if(self){
        queue = [NSMutableArray arrayWithCapacity:0];
        depth = 0;
    }
    return self;
}

- (NSUInteger) maxSize{
    return depth;
}

- (void) setMaxSize:(NSUInteger)size{
    @synchronized (self){
        depth=size;
        if ([queue count]>depth) {
            [queue removeObjectsInRange:NSMakeRange(depth, [queue count]-depth)];
        }
    }
}

- (NSUInteger) size{
    return [queue count];
}

- (id) pop{
    @synchronized (self){
        if ([queue count]==0){
            return nil;
        }
        @try {
            id r_ = [queue objectAtIndex:0];                        
            [queue removeObjectAtIndex:0];
            return r_;
        }
        @catch (NSException *exception) {
            NSLog(@"%@ at %s:%i", exception, __FILE__, __LINE__);
            return nil;
        }
        return nil;
    }
}

- (void) push:(id )frame{
    @synchronized (self){
        if (!frame)
            return;
        if (depth>1 && queue.count>depth) {
            [queue removeObjectsInRange:NSMakeRange(0, queue.count-depth)];
        }
        [queue addObject:frame];
    }
}

@end
