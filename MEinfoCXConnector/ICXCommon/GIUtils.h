//
//  GIUtils.h
//  GIConnector
//
//  Created by denn on 5/3/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Test object.
 *  
 *  @param thing object.
 *  
 *  @return is the object nill or not.
 */
BOOL IsEmpty(id thing);

/**
 *  Convert ascii HEX buffer to integer.
 *  
 *  @param buffer char-buffer.
 *  @param len    buffer length.
 *  
 *  @return return integer number.
 */
int  Ascii2int(char* buffer, int len);
