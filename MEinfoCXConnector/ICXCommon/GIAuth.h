//
//  GIAuth.h
//  GIConnector
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>


#define GIC_GUEST_USER    @"Guest"
#define GIC_GUEST_PASSWD  @""
#define GIC_GUEST_DOMAIN  @"DEMO"
#define GIC_DEFAULT_DOMAIN  @"passport"

/** GIAuth is a base infoCX Connector user authentication atributes container.
 */
@interface GIAuth : NSObject <NSCopying>

/** User id.
 */
@property(nonatomic) NSString *user;

/**
 * User password.
 */
@property(nonatomic) NSString *password;

/**
 * Client application domain.
 */
@property(nonatomic) NSString *domain;

/** Last time authentication.
 */
@property(readonly) time_t lastTime;

/** Create auth container.
 *  
 *  @param userid unique user identifier (login name, nikname, email, etc).
 *  @param password password string.
 *  
 *  @return GIAuth object instance.
 */
- (id)   initWithUser:(NSString*)userid andPassword:(NSString*)password;
- (id)   initWithUser:(NSString*)userid withPassword:(NSString*)password andDomain:(NSString*)domain;

/** Save user authenticate connection preferences in KeyChains.
 *  
 *  @param domain where it saves.
 */

- (void) saveInKeyChain:(NSString*) URL;

/** Restore saved user preferences from system KeyChains store.
 *  
 *  @param URL where from restore.
 *  
 *  @return restoring code status.
 */
- (OSStatus) restoreFromKeyChain:(NSString*) URL;

@end

