//
//  GIConfig.m
//  GIConnector
//
//  Created by denn on 3/2/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIConfig.h"

@interface GIConfig ()
//
// shared dictionaries, set once when app is launched
// or configure is changed
//
@property NSMutableArray *marketPlaces;
@property NSMutableArray *channels;
@end

@implementation GIConfig
{
    NSURL *_serverUrl;
    NSString *_normalized_url;
}

@synthesize readTimeout, reconnectCount, connectionTimeout, writeTimeout, persistTimeout, validatesSecureCertificate, reconnectTimeout, doPing, login=_login;


- (void) setServerUrl:(NSString *)url{
    _url=[[NSURL alloc] initWithString:url];

    if ([[_url scheme] isEqualToString:@"wss"] || [[_url scheme] isEqualToString:@"ws"]) {
        _tunnelType = GI_TUNNEL_WSP;
    }
    else
        _tunnelType = GI_TUNNEL_CSP;
    
    _normalized_url = [NSString stringWithFormat:@"%@://%@%@/",
                                  [_url scheme],
                                  [_url host],
                                  [_url port]?[NSString stringWithFormat:@":%@",[_url port]]:@""
                                  ];
    _serverUrl = [NSURL URLWithString:_normalized_url];
        
    GIAuth *_auth = [[GIAuth alloc] init];
    OSStatus err = [_auth restoreFromKeyChain:_normalized_url];
    if(err==0){
        _login = _auth;
    }
}

- (void) setLogin:(GIAuth *)login{
    _login=login;
    if (_normalized_url) {
        [_login saveInKeyChain:_normalized_url];        
    }
}

- (NSURL*) serverUrl { 
    return _serverUrl ;
};

- (void) _init_
{
    reconnectCount    = GI_RECONNECT_COUNT;
    reconnectTimeout  = GI_RECONNECT_TIMEOUT;
    connectionTimeout = GI_CONNECTION_TIMEOUT;
    readTimeout       = GI_READ_TIMEOUT;
    writeTimeout      = GI_WRITE_TIMEOUT;
    persistTimeout    = GI_PERSIST_TIMEOUT;
    
    validatesSecureCertificate = YES;
    
    doPing = YES;
    
    _login = [[GIAuth alloc] init];
    
    _lang = @"en";
    
    NSString *languageCode = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([languageCode hasPrefix:@"ru"])
        _lang = @"ru";        
}

- (id) initWithUrl:(NSString *)url{
    if (!(self = [super init]))	
		return nil;
    
    [self _init_];
    
    [self setServerUrl:url];
    
    return self;
}

- (id) init{
    
    if (!(self = [super init]))	
		return nil;

    [self _init_];
        
    return self;
}


- (NSString*) description{
    return [NSString stringWithFormat:@"\n\
            Configuration is:   \n\
                         url:   %@\n\
                        user:   %@\n\
                  reconnects:   %d\n\
           reconnect timeout:   %ld\n\
             connect timeout:   %ld\n\
                read timeout:   %ld\n\
               write timeout:   %ld\n\
             persist timeout:    %ld\n\
  validatesSecureCertificate:    %i\n\
                      doPing:    %i\n\
          ", self.serverUrl, 
          self.login, reconnectCount,
          reconnectTimeout, connectionTimeout, readTimeout, writeTimeout, persistTimeout,
          validatesSecureCertificate, doPing];
}

@end
