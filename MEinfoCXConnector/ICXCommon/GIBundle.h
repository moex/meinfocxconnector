//
//  NSBundle+GIBundle.h
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>


//
// Customize localizations. 
//

#define GIBUNDLE_NAME @"ICXConnector"
#define GILOCALIZABLE @"Localizable"
#define GILocalizedString(key,comment) (([GIBundle GIBundle])?(NSLocalizedStringFromTableInBundle(key, GILOCALIZABLE, [GIBundle GIBundle], comment)):(NSLocalizedString(key, comment)))

/**
 *  ICXConnector bundle preferences.
 */
@interface GIBundle: NSBundle

/**
 *  Get the shared bundle instance.
 *  
 *  @return Cocoa bundle instance.
 */
+(NSBundle*) GIBundle;

@end
