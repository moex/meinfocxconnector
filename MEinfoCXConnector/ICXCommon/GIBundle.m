//
//  NSBundle+GIBundle.m
//  GIConnector
//
//  Created by denn on 3/17/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIBundle.h"

@implementation GIBundle

+(NSBundle*) GIBundle
{
    static NSBundle*       bundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        
        NSString *libraryBundlePath = [[NSBundle bundleForClass:[self class]] pathForResource: GIBUNDLE_NAME
                                                                      ofType:@"bundle"];
        NSBundle *libraryBundle = [NSBundle bundleWithPath:libraryBundlePath];
        NSString *langID        = [[NSLocale preferredLanguages] objectAtIndex:0];
        NSString *path          = [libraryBundle pathForResource:langID ofType:@"lproj"];
        
        bundle           = [NSBundle bundleWithPath:path];
    });
    return bundle;
}

@end
