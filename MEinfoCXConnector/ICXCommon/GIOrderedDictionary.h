//
//  GIOrderedDictionary.h
//  GIConnector
//
//  Created by denn on 6/13/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Ordered dictionary extantion.
 */
@interface GIOrderedDictionary : NSMutableDictionary

/**
 *  Insert new object.
 *  
 *  @param anObject object 
 *  @param aKey     key
 *  @param anIndex  where place
 */
- (void)insertObject:(id)anObject forKey:(id)aKey atIndex:(NSUInteger)anIndex;

/**
 *  Get key object at the index.
 *  
 *  @param anIndex index.
 *  
 *  @return value.
 */
- (id)keyAtIndex:(NSUInteger)anIndex;

/**
 *  Get object at the index.
 *
 *  @param anIndex index.
 *
 *  @return object.
 */
- (id) objectAtIndex:(NSUInteger)anIndex;

/**
 *  Key enumerator to represent in forin.
 *  
 *  @return NSEnumerator object.
 */
- (NSEnumerator *)reverseKeyEnumerator;


/**
 *  Exchanges the objects in the array at given indices.	
 *
 *  @param indexOne The index of the object with which to replace the object at index indexTwo.
 *  @param indexTwo The index of the object with which to replace the object at index indexOne.
 *  
 */
- (void)  exchangeObjectAtIndex:(NSInteger)indexOne withObjectAtIndex:(NSInteger)indexTwo;

- (id) lastObject;
- (id) firstObject;

- (void) removeObjectsInRange:(NSRange)range;

@end
