//
//  GIOrderedDictionary.m
//  GIConnector
//
//  Created by denn on 6/13/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIOrderedDictionary.h"

//
// Ordered DICTIONARY
//
@implementation GIOrderedDictionary
{
	NSMutableDictionary *dictionary;
	NSMutableArray *array;
}

- (void) removeObjectsInRange:(NSRange)range{
    for (NSInteger i=range.location; i<range.length; i++) {
        id key = [array objectAtIndex:i];
        [dictionary removeObjectForKey:key];
    }
    [array removeObjectsInRange:range];
}

- (id) lastObject{
    if (self.count == 0)        return nil;
    id key = [self keyAtIndex:self.count-1];
    return [self objectForKey:key];
}

- (id) firstObject{
    if (self.count == 0)        return nil;
    id key = [self keyAtIndex:0];
    return [self objectForKey:key];
}

- (void) configureWithCapacity:(NSUInteger)capacity{
    if (self != nil)
    {
        dictionary = [[NSMutableDictionary alloc] initWithCapacity:capacity];
        array = [[NSMutableArray alloc] initWithCapacity:capacity];
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        [self configureWithCapacity:0];
    }
	return self;
}

- (id)initWithCapacity:(NSUInteger)capacity
{
	self = [super init];
    if (self) {
        [self configureWithCapacity:capacity];
    }
	return self;
}

- (id)copy
{
	return [self mutableCopy];
}

- (void)setObject:(id)anObject forKey:(id)aKey
{
	if (![dictionary objectForKey:aKey])
	{
		[array addObject:aKey];
	}
	[dictionary setObject:anObject forKey:aKey];
}

- (void)removeObjectForKey:(id)aKey
{
	[dictionary removeObjectForKey:aKey];
	[array removeObject:aKey];
}

- (NSUInteger)count
{
	return [dictionary count];
}

- (id)objectForKey:(id)aKey
{
	return [dictionary objectForKey:aKey];
}

- (NSEnumerator *)keyEnumerator
{
	return [array objectEnumerator];
}

- (NSEnumerator *)reverseKeyEnumerator
{
	return [array reverseObjectEnumerator];
}

- (void)  exchangeObjectAtIndex:(NSInteger)indexOne withObjectAtIndex:(NSInteger)indexTwo{
    [array exchangeObjectAtIndex:indexOne withObjectAtIndex:indexTwo];
}

- (void)insertObject:(id)anObject forKey:(id)aKey atIndex:(NSUInteger)anIndex
{    
	if ([dictionary objectForKey:aKey])
	{
		[self removeObjectForKey:aKey];
	}
	[array insertObject:aKey atIndex:anIndex];
	[dictionary setObject:anObject forKey:aKey];
}

- (id)keyAtIndex:(NSUInteger)anIndex
{
	return [array objectAtIndex:anIndex];
}

- (id) objectAtIndex:(NSUInteger)anIndex{
    return [self objectForKey: [array objectAtIndex:anIndex]];
}


NSString *DescriptionForObject(NSObject *object, id locale, NSUInteger indent)
{
	NSString *objectString;
	if ([object isKindOfClass:[NSString class]])
	{
		objectString = (NSString *)object;
	}
	else if ([object respondsToSelector:@selector(descriptionWithLocale:indent:)])
	{
		objectString = [(NSDictionary *)object descriptionWithLocale:locale indent:indent];
	}
	else if ([object respondsToSelector:@selector(descriptionWithLocale:)])
	{
		objectString = [(NSSet *)object descriptionWithLocale:locale];
	}
	else
	{
		objectString = [object description];
	}
	return objectString;
}

- (NSString *)descriptionWithLocale:(id)locale indent:(NSUInteger)level
{
	NSMutableString *indentString = [NSMutableString string];
	NSUInteger i, count = level;
	for (i = 0; i < count; i++)
	{
		[indentString appendFormat:@" "];
	}
	
	NSMutableString *description = [NSMutableString string];
	[description appendFormat:@"{"];
    if ([self count]>0)
        [description appendFormat:@"\n"];

    i = 0;
    for (NSObject *key in self)
	{        
		[description appendFormat:@"%@%@ = %@",
         indentString,
         DescriptionForObject(key, locale, level),
         DescriptionForObject([self objectForKey:key], locale, level)];
        if (i++<[self count])
            [description appendFormat:@",\n"];
	}
    if ([self count]>0)
        [description appendFormat:@"%@",indentString];
	[description appendFormat:@"}"];
	return description;
}


- (NSString*) description{
    return [self descriptionWithLocale:[NSLocale currentLocale] indent:0];
}
@end
