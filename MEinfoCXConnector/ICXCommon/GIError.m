//
//  GIError.m
//  GIConnector
//
//  Created by denn on 3/4/13.
//  Copyright (c) 2013 Denis Svinarchuk. All rights reserved.
//

#import "GIError.h"

@implementation GIError

- (id) initWithNSError:(NSError *)error{
    self = [error copy];    
    return self;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"Error: %li: %@ {%@}", (long)self.code, self.localizedDescription, self.localizedRecoverySuggestion];
}

@end
