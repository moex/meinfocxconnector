//
//  MEinfoCXDispatcherTest.m
//  MEinfoCXDispatcherTest
//
//  Created by denn on 10.11.13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GIStomp.h"
#import "GIStompDispatcher.h"

@interface MEinfoCXDispatcherTest : XCTestCase <GIStompDispatcherProtocol, GIStompErrorDispatchHandler, GIStompEventDispatchHandler, GIStompLoginDispatchHandler>

@end

@implementation MEinfoCXDispatcherTest
{
    GIConfig          *config;    
    GIStompDispatcher *_dispatcher;
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    
    config = [[GIConfig alloc] initWithUrl:@"wss://moex.com/infocx/v1/websocket"];
    config.login = [[GIAuth alloc] initWithUser:@"guest" andPassword:@""];
    
    config.validatesSecureCertificate = NO;
    
    _dispatcher = [[GIStompDispatcher alloc] initWithConfig:config];
    
    _dispatcher.loglevel = GI_STOMP_LOG_DEBUG;
    _dispatcher.errorHandler = self;
    _dispatcher.eventHandler = self;
    
    
    _dispatcher.loginHandler = self;
    
    [_dispatcher connect];

    GIStompSecurity *ot = [GIStompSecurity select:@"MXSE.TQBS.GAZP"];
    ot.delegate = self;
    [_dispatcher registerMessage:ot];

    GIStompSearchTickers *_preset = [GIStompSearchTickers selectPreset:@"default"];
    [_dispatcher registerRequest:_preset];
    _preset.delegate = self;

    GIStompSearchTickers *_tickers1 = [GIStompSearchTickers select:@"Сбер"];
    [_dispatcher registerRequest:_tickers1];
    _tickers1.delegate = self;
    
    
    GIStompSearchTickers *_tickers2 = [GIStompSearchTickers select:@"lko"];
    [_dispatcher registerRequest:_tickers2];
    _tickers2.delegate = self;

    
    if (0) {        
        XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
    }
    
    
    sleep(10);
    
    [_dispatcher disconnect];
}


- (void) didStompLoginError:(GIError *)error withConnectionResponse:(GICommonStompFrame *)response{
    NSLog(@"Login error: %@: %@", error, response);
}

- (void) didStompLogin:(NSDictionary*)connection {
    NSLog(@"Login connection: %@", connection);
}

- (void) didFrameReceive:(id)frame{
    
    GICommonStompFrame *message=frame;
    
    NSLog(@"\nYAHOOOO!\n%@\n", [frame class]);
    
    
    if ([frame isKindOfClass:[GIStompTickers class]]){
        NSLog(@" TICKERS: %@", message);
        return;
    }
    
    if ([frame isKindOfClass:[GIStompSearchTickers class]]){
        NSLog(@" SEARCH: %@", message);
        return;
    }
    
    if ([frame isKindOfClass:[GIStompCandles class]]) {
        NSLog(@" CANDLES: %@", message);
        return;
    }
    
    GIStompMessage *m = frame;
    
    for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c=[m.responseQueue pop]) {
        NSLog(@" Queue length = %lu %@ %@ : %@", (unsigned long)[m.responseQueue size], [m.lastResponse.body stringForIndex:0 andKey:@"TICKER"], m.lastResponse.body.properties, [c description]);
        //NSLog(@" Structure", m.structure);
    }    
}

- (void) channelConnectionRestored{
    NSLog(@"Channel restored");
}

- (void) channelReconnecting{
    NSLog(@"Channel reconnection");
}


- (void) didSystemError:(GIError *)error{
    NSLog(@"%@", error);
}

- (void) didStompError:(GIError *)error{
    NSLog(@"%@", error);
}

- (void) channelWillActive{
    NSLog(@"Start connecting...");
}

- (void) channelOpened{
    NSLog(@"Opened...");
    NSLog(@"Opened: %@",  _dispatcher.structure);
}

- (void) channelClosed{
    NSLog(@"Closed");
}

@end
