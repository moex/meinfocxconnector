Pod::Spec.new do |s|
  s.name         = 'MEinfoCXConnector'
  s.version      = '1.2.4'
  s.license      = { :type => 'GPL Version 3', :file => 'LICENSE' }
  s.author       = { "denn nevera (book pro)" => "denn.nevera@gmail.com" }
  s.homepage     = 'http://moex.com'
  s.summary      = 'InfoCXConnector is Cocoa framework for OS X/iOS which provides high level access to InfoCXConnector remote objects'
  s.description  = 'InfoCXConnector is Cocoa framework for OS X/iOS which provides high level access to InfoCXConnector remote objects. Connector object offers event-driven model to control states and datasets of the follow types: securities, trades, orderbooks, orders, positions.'

# Source Info
  s.ios.deployment_target     =  '8.0'
  s.osx.deployment_target     =  '10.8'
  s.source       =  {:git => 'https://denn_nevera@bitbucket.org/denn_nevera/meinfocxconnector.git', :tag => s.version}
  
  s.source_files = 'MEinfoCXConnector/ICXCSP/*.{h,m}', 'MEinfoCXConnector/ICXWSP/*.{h,m}', 'MEinfoCXConnector/ICXCommon/*.{h,m}', 'MEinfoCXConnector/ICXStomp/*.{h,m}', 'MEinfoCXConnector/ICXTunnel/*.{h,m}'
  s.public_header_files = 'MEinfoCXConnector/ICXCSP/*.h', 'MEinfoCXConnector/ICXWSP/*.h', 'MEinfoCXConnector/ICXCommon/*.{h}', 'MEinfoCXConnector/ICXStomp/*.{h}', 'MEinfoCXConnector/ICXTunnel/*.{h}'
  s.resources    = 'MEinfoCXConnector/ICXConnector.bundle'
  
  s.framework    = 'Foundation'
  s.libraries    = 'z'
  s.requires_arc = true
  
# Pod Dependencies

  s.dependency 'SocketRocket'
  
end
