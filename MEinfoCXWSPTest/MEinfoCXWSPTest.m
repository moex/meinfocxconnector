//
//  MEinfoCXWSPTest.m
//  MEinfoCXWSPTest
//
//  Created by denis svinarchuk on 31.03.14.
//  Copyright (c) 2014 Moscow Exchange. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GIWSPTunnel.h"
#import "GIStompRequests.h"
#import "GIStompSubscriptions.h"

@interface MEinfoCXWSPTest : XCTestCase <GITunnelHandler>

@property (nonatomic,strong) GIWSPTunnel *tunnel;
@property (nonatomic,strong) GIConfig *config;

@end

@implementation MEinfoCXWSPTest


- (GIConfig*) config{
    if (_config) {
        return _config;
    }

    _config = [[GIConfig alloc] initWithUrl:@"wss://moex.com/infocx/v1/websocket"];
    _config.login = [[GIAuth alloc] initWithUser:@"guest" andPassword:@""];
    _config.validatesSecureCertificate = NO;

    return _config;
}

- (GIWSPTunnel*) tunnel{
    if (_tunnel) {
        return _tunnel;
    }

    _tunnel = [[GIWSPTunnel alloc] initWithGIConfig:self.config];
    _tunnel.connectionHandler = self;
    
    return _tunnel;
}

- (void) dealloc{
    [self.tunnel close];
}


- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    //XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);

    GIWSPTunnel *tunnel = self.tunnel;
    
    [tunnel open];
    
    while (!tunnel.isOpened) {
        NSLog(@"Wait opening....");
        sleep(1);
    }
    
    sleep(100);
    
    [tunnel close];
}


- (void) onError:(GIError *)error in:(GITunnelCommand)command withData:(NSData *)data{
    NSLog(@"onError: %@ in:%i withData:%@", error, command, data);
}

- (void) onError:(GIError *)error in:(GITunnelCommand)command{
    NSLog(@"onError: %@ in:%i", error, command);
}

- (void) onSend:(NSData *)data{
    NSLog(@"onSend: %@", data);
}

- (void) onPing:(NSData *)data roundTrip:(NSTimeInterval)roundTrip{
    NSLog(@"onPing: roundtrip: %f", roundTrip);
}

- (void) onRead:(NSData *)data{
    GICommonStompFrame *rp = [[GICommonStompFrame alloc] initWithData:data];

    if ([rp.name isEqualToString:@"CONNECTED"]){
        
        GIStompConnect *connectionStomp = [[GIStompConnect alloc] init];
        [connectionStomp messageReceived: rp];
        
        GIConnectionStructure *structure = connectionStomp.lastResponse.body.structure;
        NSLog(@"Structure: %@", structure);
        
        GIStompSecurity *ot = [GIStompSecurity select:@"MXSE.TQBS.GAZP"];
        [self.tunnel send:[ot.request stompFrame]];
    }
    else{
        NSLog(@"onRead: %@", rp);
    }
}

- (void) onOpen:(NSData *)data{
    NSLog(@"onOpen: %@", data);
    
    GIStompConnect *connect = [GIStompConnect withLogin:self.config.login.user andPasscode:self.config.login.password];
    
    [self.tunnel send:[connect.request stompFrame]];
    
}

- (void) onConnect:(NSData *)data{
    NSLog(@"onConnect: %@", data);
}

- (void) onClose:(NSData *)data{
    NSLog(@"onClose: %@", data);
}


- (BOOL) isDataCompleted:(NSData *)chainedData{
    if(!chainedData) return YES;
    if([chainedData length]==0) return YES;
    char _the_end_of_stopm;
    [chainedData getBytes:&_the_end_of_stopm range:NSMakeRange([chainedData length]-1, 1)];
    return _the_end_of_stopm == '\0';
}

@end
