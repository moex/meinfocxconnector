# MEInfoCXConnector

Created by **denn.nevera**

InfoCXConnector is a Cocoa framework for OSX/iOS which provides high level access to InfoCX (former goInvest) remote objects.
The connector offers event-driven model to control states and datasets of the follow types: securities, trades, orderbooks, orders, positions 
and abstruct STOMP-objects that can be extended to any type comprises structured data.

InfoCXConnector is a AMQP-based server-side aplication offers deifferent transport level protocols for client apps: AMQP/STOMP/TSMR/CSP.
InfoCX client connector for OSX/iOS represents CSP-like push-async messaging connection through HTTP/HTTPS/Websocket.

Mobile applications can use the package to recieve Moscow Exchange public trading information.

# How to use

* Install cocoapods: http://cocoapods.org/
* Edit the Podfile in your XCode project directory YourApp:

```
#!bash
    $ edit Podfile
    platform :ios, '7.0'
    pod 'MEinfoCXConnector', :git => 'https://denn_nevera@bitbucket.org/denn_nevera/meinfocxconnector.git', :tag => '1.0.8'
```

* Install dependences in your project

```
#!bash
    $ pod install
```

* Open the XCode workspace instead of the project file

```
#!bash
    $ open YourApp.xcworkspace
```

* Import API

```
#!objective-c

    #import "GIStomp.h"
    #import "GIStompDispatcher.h"

```

# Example

```
#!objective-c

#import <Foundation/Foundation.h>
#import "GIStompChannelDispatcher.h"

@interface GIStompTest : NSObject <GIStompDispatcherProtocol, GIStompErrorDispatchHandler, GIStompEventDispatchHandler, GIStompLoginDispatchHandler>

@end


@implementation GIStompTest
{
    GIConfig                 *config;
    GIStompChannelDispatcher *dispatcher;
}

//
//  We should define didFrameReceive
//
#pragma mark GIStompDispatcherProtocol

- (void) didFrameReceive:(id)frame{

    NSLog(@"\nYAHOOOO!\n%@\n", [frame class]);
    
    
    if ([frame isKindOfClass:[GIStompTickers class]]){
        // Request 
        NSLog(@" TICKERS: %@", frame);
        return;
    }
    
    
    GIStompMessage *m = frame;
    //
    // Messages 
    // 
    for (GICommonStompFrame *c = [m.responseQueue pop]; c ; c=[m.responseQueue pop]) {
       //
       // All messages in response queue can be poped
       //
        NSLog(@" Queue length = %i %@ %@ : %@", [m.responseQueue size], [m.lastResponse.body stringForIndex:0 andKey:@"TICKER"], m.lastResponse.body.properties, [c description]);

       //
       // We can read the stomp structure wich have received. The structure comprises fields information,
       // their types and the fields (in case field is an enum - a description of the enum)
       //
       NSLog(@" Stomp structure: %@", c.structure);
    }   
}


//
// Optional protocols
//

//
// Connection events
//
#pragma mark GIStompEventDispatchHandler
- (void) channelConnectionRestored{
    NSLog(@"Channel restored");
}

- (void) channelReconnecting{
    NSLog(@"Channel reconnection");
}

- (void) channelWillActive{
    NSLog(@"Start connecting...");
}

- (void) channelOpened{
    NSLog(@"Opened");
}

- (void) channelClosed{
    NSLog(@"Closed");
}


//
// Error events
//
#pragma mark GIStompErrorDispatchHandler
- (void) didSystemError:(GIError *)error{
    NSLog(@"%@", error);
}

- (void) didStompError:(GIError *)error{
    NSLog(@"%@", error);
}

//
// Authentication events
//
#pragma mark GIStompLoginDispatchHandler
- (void) didStompLoginError:(GIError *)error withConnectionResponse:(GICommonStompFrame *)response{
    NSLog(@"Login error: %@: %@", error, response);
}

- (void) didStompLogin:(NSDictionary*)connection {
    NSLog(@"Login connection: %@", connection);
}

@end


int main(int argc, char* argv[])
{
	
  @autoreleasepool{
	
       // 
       // Create configuration
       //
       self.config = [[GIConfig alloc] initWithUrl:@"https://goinvest2.beta.micex.ru"];
       self.config.login = [[GIAuth alloc] initWithUser:@"username" andPassword:@"password"];

       // 
       // Open new dispatcher instance
       // 
       self.dispatcher = [[GIStompDispatcher alloc] initWithConfig:config];

       //
       // Set delegates (handlers)
       //
       self.dispatcher.errorHandler = self;
       self.dispatcher.eventHandler = self;
    
       //
       // Authenticator handler
       //
       self.dispatcher.loginHandler = self;
    
       //
       // Open persistent connection to dispatch connection
       //
       [dispatcher connect];

       //
       // Create new subscirption definition
       //           
       GIStompCandles   *candles    = [GIStompCandles select:@"MXSE.TQBR.SBER" andInterval:@"M10"];
       GIStompOrderbook *orderbooks = [GIStompOrderbook select:@"MXSE.TQBR.SBER"];
       GIStompSecurity  *security   = [GIStompSecurity select:@"MXSE.TQBR.SBER"];

       //
       // Subscription definition should be regestered in dispatcher
       //
       [self.dispatcher registerMessage:candles];
       //
       // Set subsciption dispatcher delegate (handler)
       //
       candles.delegate = self;

       [self.dispatcher registerMessage:orderbooks];
       orderbooks.delegate = self;

       [self.dispatcher registerMessage:security];
       security.delegate = self;

       //
       // Just simulate application main loop thread
       // 
       int _in_progress = 1;
       while(_in_progress){
		sleep(1);
      }
  }
}

```

